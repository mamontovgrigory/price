import * as React from 'react';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { IResources } from '@models/localization';
import { IColorItem, IColors } from '@models/colors';
import {
  getColors,
  setColorItem,
  saveColorItem,
  deleteColorItems,
  uploadColorsItems,
  checkColorsItems
} from '@data/colors';
import { Crud, Icon } from '@ui';
import { Loc } from '@components';
import { ColorForm } from './components';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  colors: IColors;
  setColorItem: (item: IColorItem) => void;
  saveColorItem: (data: { item: IColorItem }) => Promise<null>;
  deleteColorItems: (data: { ids: number[] }) => Promise<null>;
  uploadColorsItems: (files: File[]) => Promise<null>;
  checkColorsItems: () => Promise<null>;
}

interface IState {
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => {
    return dispatch(getColors());
  }
}])
@connect(
  (state) => ({
    currentLanguage: state.localization.currentLanguage,
    resources: state.localization.resources,
    colors: state.colors
  }),
  (dispatch) => ({
    setColorItem: (item: IColorItem) => dispatch(setColorItem(item)),
    saveColorItem: (data: { item: IColorItem }) => dispatch(saveColorItem(data)),
    deleteColorItems: (data: { ids: number[] }) => dispatch(deleteColorItems(data)),
    uploadColorsItems: (files: File[]) => dispatch(uploadColorsItems(files)),
    checkColorsItems: () => dispatch(checkColorsItems())
  })
)
export class ColorsPage extends React.Component<IProps, IState> {
  private getLabel = (locKey: string) => {
    const { currentLanguage, resources } = this.props;
    return Loc.getString({
      currentLanguage,
      resources,
      locKey
    });
  }

  public render() {
    const {
      colors: {
        list,
        item
      },
      setColorItem,
      saveColorItem,
      deleteColorItems,
      uploadColorsItems,
      checkColorsItems
    } = this.props;
    const colModel = [
      {
        name: 'id',
        label: this.getLabel('id'),
        width: 100
      },
      {
        name: 'code',
        label: this.getLabel('colorCode')
      },
      {
        name: 'mark',
        label: this.getLabel('mark'),
        formatter: 'select'
      },
      {
        name: 'name',
        label: this.getLabel('name'),
        formatter: 'select'
      },
      {
        name: 'fullName',
        label: this.getLabel('fullName')
      }
    ];
    const actions = [
      {
        title: <Icon type="search"/>,
        onClick: checkColorsItems
      }
    ];
    return (
      <Crud
        title={<Loc locKey="colors"/>}
        colModel={colModel}
        list={list}
        item={item}
        setItem={setColorItem}
        saveItem={saveColorItem}
        deleteItems={deleteColorItems}
        upload={uploadColorsItems}
        actions={actions}
        form={<ColorForm/>}
      />
    );
  }
}

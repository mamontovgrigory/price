import * as React from 'react';
const {connect} = require('react-redux');

import { IColors, IColorItem } from '@models/colors';
import { setColorItem } from '@data/colors';
import { Row, Input } from '@ui';
import { Loc } from '@components';

interface IProps {
  colors?: IColors;
  setColorItem?: (item: IColorItem) => void;
}

interface IState {
}

@connect(
  (state) => ({
    colors: state.colors
  }),
  (dispatch) => ({
    setColorItem: (item: IColorItem) => dispatch(setColorItem(item))
  })
)
export class ColorForm extends React.Component<IProps, IState> {
  private changeItem = (key, value) => {
    const {setColorItem, colors: {item}} = this.props;
    if (setColorItem) {
      setColorItem(Object.assign({}, item, {
        [key]: value
      }));
    }
  }

  private handleChangeCode = (value: string) => {
    this.changeItem('code', value);
  }

  private handleChangeMark = (value: string) => {
    this.changeItem('mark', value);
  }

  private handleChangeName = (value: string) => {
    this.changeItem('name', value);
  }

  private handleChangeFullName = (value: string) => {
    this.changeItem('fullName', value);
  }

  public render() {
    const {
      colors: {
        item: {
          code, mark, name, fullName
        }
      }
    } = this.props;
    return (
      <div>
        <Row>
          <Input
            sm={4}
            label={<Loc locKey="code"/>}
            validate={true}
            value={code}
            onChange={this.handleChangeCode}
          />
          <Input
            sm={4}
            label={<Loc locKey="mark"/>}
            validate={true}
            value={mark}
            onChange={this.handleChangeMark}
          />
          <Input
            sm={4}
            label={<Loc locKey="name"/>}
            validate={true}
            value={name}
            onChange={this.handleChangeName}
          />
        </Row>
        <Row>
          <Input
            sm={12}
            label={<Loc locKey="fullName"/>}
            value={fullName}
            onChange={this.handleChangeFullName}
          />
        </Row>
      </div>
    );
  }
}

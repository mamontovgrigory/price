import * as React from 'react';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { IResources } from '@models/localization';
import { IOptionItem, IOptions } from '@models/options';
import {
  getOptions,
  setOptionItem,
  saveOptionItem,
  deleteOptionItems,
  uploadOptionsItems,
  checkOptionsItems
} from '@data/options';
import { Crud, Icon } from '@ui';
import { Loc } from '@components';
import { OptionForm } from './components';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  options: IOptions;
  setOptionItem: (item: IOptionItem) => void;
  saveOptionItem: (data: { item: IOptionItem }) => Promise<null>;
  deleteOptionItems: (data: { ids: number[] }) => Promise<null>;
  uploadOptionsItems: (files: File[]) => Promise<null>;
  checkOptionsItems: () => Promise<null>;
}

interface IState {
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => {
    return dispatch(getOptions());
  }
}])
@connect(
  (state) => ({
    currentLanguage: state.localization.currentLanguage,
    resources: state.localization.resources,
    options: state.options
  }),
  (dispatch) => ({
    setOptionItem: (item: IOptionItem) => dispatch(setOptionItem(item)),
    saveOptionItem: (data: { item: IOptionItem }) => dispatch(saveOptionItem(data)),
    deleteOptionItems: (data: { ids: number[] }) => dispatch(deleteOptionItems(data)),
    uploadOptionsItems: (files: File[]) => dispatch(uploadOptionsItems(files)),
    checkOptionsItems: () => dispatch(checkOptionsItems())
  })
)
export class OptionsPage extends React.Component<IProps, IState> {
  private getLabel = (locKey: string) => {
    const { currentLanguage, resources } = this.props;
    return Loc.getString({
      currentLanguage,
      resources,
      locKey
    });
  }

  public render() {
    const {
      options: {
        list,
        item
      },
      setOptionItem,
      saveOptionItem,
      deleteOptionItems,
      uploadOptionsItems,
      checkOptionsItems
    } = this.props;
    const colModel = [
      {
        name: 'id',
        label: this.getLabel('id'),
        width: 100
      },
      {
        name: 'code',
        label: this.getLabel('optionCode')
      },
      {
        name: 'mark',
        label: this.getLabel('mark'),
        formatter: 'select'
      },
      {
        name: 'model',
        label: this.getLabel('model')
      },
      {
        name: 'explanation',
        label: this.getLabel('explanation')
      },
      {
        name: 'doNotUse',
        label: this.getLabel('doNotUse'),
        formatter: 'select'
      }
    ];
    const actions = [
      {
        title: <Icon type="search"/>,
        onClick: checkOptionsItems
      }
    ];
    return (
      <Crud
        title={<Loc locKey="options"/>}
        colModel={colModel}
        list={list}
        item={item}
        setItem={setOptionItem}
        saveItem={saveOptionItem}
        deleteItems={deleteOptionItems}
        upload={uploadOptionsItems}
        actions={actions}
        form={<OptionForm/>}
      />
    );
  }
}

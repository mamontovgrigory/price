import * as React from 'react';
const {connect} = require('react-redux');

import { IOptions, IOptionItem } from '@models/options';
import { setOptionItem } from '@data/options';
import { Row, Input, Checkbox, Textarea } from '@ui';
import { Loc } from '@components';

interface IProps {
  options?: IOptions;
  setOptionItem?: (item: IOptionItem) => void;
}

interface IState {
}

@connect(
  (state) => ({
    options: state.options
  }),
  (dispatch) => ({
    setOptionItem: (item: IOptionItem) => dispatch(setOptionItem(item))
  })
)
export class OptionForm extends React.Component<IProps, IState> {
  private changeItem = (key, value) => {
    const {setOptionItem, options: {item}} = this.props;
    if (setOptionItem) {
      setOptionItem(Object.assign({}, item, {
        [key]: value
      }));
    }
  }

  private handleChangeCode = (value: string) => {
    this.changeItem('code', value);
  }

  private handleChangeMark = (value: string) => {
    this.changeItem('mark', value);
  }

  private handleChangeModel = (value: string) => {
    this.changeItem('model', value);
  }

  private handleChangeDisabled = (value: boolean) => {
    this.changeItem('disabled', value);
  }

  public render() {
    const {
      options: {
        item: {
          code, mark, model, explanation, disabled
        }
      }
    } = this.props;
    return (
      <div>
        <Row>
          <Input
            sm={4}
            label={<Loc locKey="code"/>}
            validate={true}
            value={code}
            onChange={this.handleChangeCode}
          />
          <Input
            sm={4}
            label={<Loc locKey="mark"/>}
            validate={true}
            value={mark}
            onChange={this.handleChangeMark}
          />
          <Input
            sm={4}
            label={<Loc locKey="model"/>}
            validate={true}
            value={model}
            onChange={this.handleChangeModel}
          />
        </Row>
        <Row>
          <Textarea
            label={<Loc locKey="explanation"/>}
            value={explanation}
            onChange={this.handleChangeExplanation}
          />
        </Row>
        <Row>
          <Checkbox
            label={<Loc locKey="doNotUse"/>}
            checked={disabled}
            onChange={this.handleChangeDisabled}
          />
        </Row>
      </div>
    );
  }

  private handleChangeExplanation = (value: string) => {
    this.changeItem('explanation', value);
  }
}

import * as React from 'react';
const {connect} = require('react-redux');

import { IStock, IStockItem } from '@models/stock';
import { setStockItem } from '@data/stock';
import { Row, Col, Input, Autocomplete, Textarea, Collapsible, Carousel } from '@ui';
import { Loc } from '@components';

interface IProps {
  stock?: IStock;
  clients: string[];
  setStockItem?: (item: IStockItem) => void;
}

interface IState {
}

@connect(
  (state) => ({
    stock: state.stock
  }),
  (dispatch) => ({
    setStockItem: (item: IStockItem) => dispatch(setStockItem(item))
  })
)
export class StockForm extends React.Component<IProps, IState> {
  private changeItem = (key, value) => {
    const {setStockItem, stock: {item}} = this.props;
    setStockItem(Object.assign({}, item, {
      [key]: value
    }));
  }

  private handleChangeClient = (value: string) => {
    this.changeItem('client', value);
  }

  private handleChangeMark = (value: string) => {
    this.changeItem('mark', value);
  }

  private handleChangeModel = (value: string) => {
    this.changeItem('model', value);
  }

  private handleChangeComplectation = (value: string) => {
    this.changeItem('complectation', value);
  }

  private handleChangeColor = (value: string) => {
    this.changeItem('color', value);
  }

  private handleChangeBody = (value: string) => {
    this.changeItem('body', value);
  }

  private handleChangeCondition = (value: string) => {
    this.changeItem('condition', value);
  }

  private handleChangePlace = (value: string) => {
    this.changeItem('place', value);
  }

  private handleChangeFront = (value: string) => {
    this.changeItem('front', value);
  }

  private handleChangeBack = (value: string) => {
    this.changeItem('back', value);
  }

  private handleChangeDescription = (value: string) => {
    this.changeItem('description', value);
  }

  private handleChangeVin = (value: string) => {
    this.changeItem('vin', value);
  }

  private handleChangeVideos = (value: string) => {
    this.changeItem('videos', value);
  }

  private handleChangePanoramas = (value: string) => {
    this.changeItem('panoramas', value);
  }

  private handleChangeInteriorsByVin = (value: string) => {
    this.changeItem('interiorsByVin', value);
  }

  private getPhotosList = (str?: string) => {
    return str ? str.replace('', '').split(',') : [];
  }

  public render() {
    const {stock: {item}, clients} = this.props;
    const {
      client,
      mark,
      model,
      complectation,
      color,
      body,
      condition,
      place,
      front,
      back,
      description,
      vin,
      videos,
      panoramas,
      interiorsByVin
    } = item;
    const frontPhotos = this.getPhotosList(front);
    const backPhotos = this.getPhotosList(back);
    const interiorsByVinPhotos = this.getPhotosList(interiorsByVin);
    const frontPhotosGallery = (
      <Row>
        <Col>
          <Collapsible key={frontPhotos.toString()}
                       header={<Loc locKey="frontPhotos"/>}
                       icon="photo_library"
                       content={<Carousel images={frontPhotos}/>}/>
        </Col>
      </Row>
    );
    const backPhotosGallery = (
      <Row>
        <Col>
          <Collapsible key={backPhotos.toString()}
                       header={<Loc locKey="backPhotos"/>}
                       icon="photo_library"
                       content={<Carousel images={backPhotos}/>}/>
        </Col>
      </Row>
    );
    const interiorsByVinPhotosGallery = (
      <Row>
        <Col>
          <Collapsible key={interiorsByVinPhotos.toString()}
                       header={<Loc locKey="interiorsPhotos"/>}
                       icon="photo_library"
                       content={<Carousel images={interiorsByVinPhotos}/>}/>
        </Col>
      </Row>
    );
    return (
      <div>
        <Row>
          <Autocomplete
            label={<Loc locKey="client"/>}
            items={clients}
            value={client}
            onChange={this.handleChangeClient}
          />
        </Row>
        <Row>
          <Input sm={6} label={<Loc locKey="mark"/>} validate={true} value={mark} onChange={this.handleChangeMark}/>
          <Input sm={6} label={<Loc locKey="model"/>} validate={true} value={model} onChange={this.handleChangeModel}/>
        </Row>
        <Row>
          <Input
            label={<Loc locKey="complectation"/>}
            validate={true}
            value={complectation}
            onChange={this.handleChangeComplectation}
          />
        </Row>
        <Row>
          <Input sm={6} label={<Loc locKey="color"/>} validate={true} value={color} onChange={this.handleChangeColor}/>
          <Input sm={6} label={<Loc locKey="body"/>} validate={true} value={body} onChange={this.handleChangeBody}/>
        </Row>
        <Row>
          <Input sm={6} label={<Loc locKey="condition"/>} value={condition} onChange={this.handleChangeCondition}/>
          <Input sm={6} label={<Loc locKey="place"/>} value={place} onChange={this.handleChangePlace}/>
        </Row>
        <Row>
          <Textarea label={<Loc locKey="frontPhotos"/>} value={front} onChange={this.handleChangeFront}/>
        </Row>
        {frontPhotos.length > 0 && frontPhotosGallery}
        <Row>
          <Textarea label={<Loc locKey="backPhotos"/>} value={back} onChange={this.handleChangeBack}/>
        </Row>
        {backPhotos.length > 0 && backPhotosGallery}
        <Row>
          <Textarea label={<Loc locKey="description"/>} value={description} onChange={this.handleChangeDescription}/>
        </Row>
        <Row>
          <Input label={<Loc locKey="vin"/>} value={vin} onChange={this.handleChangeVin}/>
        </Row>
        <Row>
          <Textarea label={<Loc locKey="videos"/>} value={videos} onChange={this.handleChangeVideos}/>
        </Row>
        <Row>
          <Textarea label={<Loc locKey="panoramas"/>} value={panoramas} onChange={this.handleChangePanoramas}/>
        </Row>
        <Row>
          <Textarea
            label={<Loc locKey="interiorsByVin"/>}
            value={interiorsByVin}
            onChange={this.handleChangeInteriorsByVin}
          />
        </Row>
        {interiorsByVinPhotos.length > 0 && interiorsByVinPhotosGallery}
      </div>
    );
  }
}

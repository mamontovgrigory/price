import * as React from 'react';
const {connect} = require('react-redux');

import { uploadStockItems } from '@data/stock';
import { Row, Col, Input, Modal, Dropzone } from '@ui';
import { Loc } from '@components';

interface IProps {
  show: boolean;
  onClose: () => void;
  uploadStockItems?: (data, files) => Promise<null>;
}

interface IState {
  client?: string;
  files: File[];
}

@connect(
  (state) => ({
    stock: state.stock
  }),
  (dispatch) => ({
    uploadStockItems: (data, files) => dispatch(uploadStockItems(data, files))
  })
)
export class UploadModal extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      files: []
    };
  }

  private handleChangeClient = (client: string) => {
    this.setState({
      client
    });
  }

  private handleChangeFiles = (files: File[]) => {
    this.setState({
      files
    });
  }

  private uploadFiles = () => {
    const {uploadStockItems, onClose} = this.props;
    const {client, files} = this.state;
    if (uploadStockItems && files.length) {
      uploadStockItems({client}, files).then(() => {
        onClose();
      });
    }
  }

  public render() {
    const {show, onClose} = this.props;
    const {client} = this.state;
    const uploadFormActions = [{
      text: <Loc locKey="load"/>,
      onClick: this.uploadFiles
    }];
    return (
      <Modal show={show}
             header={<Loc locKey="upload"/>}
             actions={uploadFormActions}
             onClose={onClose}>
        <Row>
          <Input label={<Loc locKey="client"/>} onChange={this.handleChangeClient} value={client}/>
        </Row>
        <Row>
          <Col>
            <Dropzone onChange={this.handleChangeFiles}/>
          </Col>
        </Row>
      </Modal>
    );
  }
}

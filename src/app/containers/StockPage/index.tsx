import * as React from 'react';
import * as $ from 'jquery';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { IResources } from '@models/localization';
import { IStock, IStockItem } from '@models/stock';
import { getStock, setStockItem, saveStockItem, deleteStockItems, checkStockItems } from '@data/stock';
import { getGroups } from '@data/groups';
import { Crud, Icon } from '@ui';
import { Loc } from '@components';
import { StockForm, UploadModal } from './components';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  stock: IStock;
  setStockItem: (item: IStockItem) => void;
  saveStockItem: (data: { item: IStockItem }) => Promise<null>;
  deleteStockItems: (data: { ids: number[] }) => Promise<null>;
  checkStockItems: () => Promise<null>;
}

interface IState {
  showUploadModal: boolean;
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => {
    return dispatch(getStock());
  }
}, {
  promise: ({store: {dispatch}}) => {
    return dispatch(getGroups());
  }
}])
@connect(
  (state) => ({
    stock: state.stock,
    groups: state.groups,
    currentLanguage: state.localization.currentLanguage,
    resources: state.localization.resources
  }),
  (dispatch) => ({
    setStockItem: (item: IStockItem) => dispatch(setStockItem(item)),
    saveStockItem: (data: { item: IStockItem }) => dispatch(saveStockItem(data)),
    deleteStockItems: (data: { ids: number[] }) => dispatch(deleteStockItems(data)),
    checkStockItems: () => dispatch(checkStockItems())
  })
)
export class StockPage extends React.Component<IProps, IState> {
  public constructor(props: IProps) {
    super(props);
    this.state = {
      showUploadModal: false
    };
  }

  private openUploadModal = () => {
    this.setState({
      showUploadModal: true
    });
  }

  private closeUploadModal = () => {
    this.setState({
      showUploadModal: false
    });
  }

  private getLabel = (locKey: string) => {
    const {currentLanguage, resources} = this.props;
    return Loc.getString({
      currentLanguage,
      resources,
      locKey
    });
  }

  private getPhotoNumber = (photo: string) => photo ?
    photo.replace(/[^0-9,]/g, '').replace(/,/g, ', ').split(',').shift() :
    '';

  private getPhotosNumbers = (photos: string) => photos ?
    photos.replace(/[^0-9,]/g, '').replace(/,/g, ', ') :
    '';

  private getList = () => {
    const {stock: {list}} = this.props;
    return list.map((item) => ({
      ...item,
      frontFirst: this.getPhotoNumber(item.front),
      backFirst: this.getPhotoNumber(item.back),
      preview: item.front
    }));
  }

  private getClients = () => {
    const {stock} = this.props;
    const clients = [];
    stock.list.forEach((item) => {
      if (clients.indexOf(item.client) === -1) {
        clients.push(item.client);
      }
    });
    return clients;
  }

  private previewFormatter = (value: string) => value ? `<img src="${value.split(',').shift()}"/>` : '';

  private frontFirstFormatter = (_, __, row) => this.getPhotosNumbers(row.front);

  private backFirstFormatter = (_, __, row) => this.getPhotosNumbers(row.back);

  private onComplete = () => {
    const {stock: {lastUpdated}} = this.props;
    if (Number.isInteger(lastUpdated)) {
      $('#' + lastUpdated).css('background', '#AFE0AF');
    }
  }

  public render() {
    const {stock: {item}, setStockItem, saveStockItem, deleteStockItems, checkStockItems} = this.props;
    const {showUploadModal} = this.state;
    const colModel = [
      {
        name: 'id',
        label: this.getLabel('id'),
        width: 100,
        key: true
      },
      {
        name: 'client',
        label: this.getLabel('client'),
        width: 100,
        formatter: 'select'
      },
      {
        name: 'preview',
        label: this.getLabel('preview'),
        formatter: this.previewFormatter,
        width: 100
      },
      {
        name: 'frontFirst',
        label: this.getLabel('frontPhotos'),
        formatter: this.frontFirstFormatter,
        width: 100
      },
      {
        name: 'backFirst',
        label: this.getLabel('backPhotos'),
        formatter: this.backFirstFormatter,
        width: 100
      },
      {
        name: 'mark',
        label: this.getLabel('mark'),
        width: 100,
        formatter: 'select'
      },
      {
        name: 'model',
        label: this.getLabel('model'),
        width: 100
      },
      {
        name: 'complectation',
        label: this.getLabel('complectation')
      },
      {
        name: 'color',
        label: this.getLabel('color'),
        width: 100,
        formatter: 'select'
      },
      {
        name: 'body',
        label: this.getLabel('body'),
        width: 100,
        formatter: 'select'
      },
      {
        name: 'place',
        label: this.getLabel('place'),
        width: 100,
        formatter: 'select'
      },
      {
        name: 'description',
        label: this.getLabel('description')
      },
      {
        name: 'vin',
        label: this.getLabel('vin'),
        width: 100
      }
    ];
    const clients = this.getClients();
    const actions = [
      {
        title: <Icon type="file_upload"/>,
        onClick: this.openUploadModal
      },
      {
        title: <Icon type="search"/>,
        onClick: checkStockItems
      }
    ];
    return (
      <div>
        <Crud
          title={<Loc locKey="price"/>}
          colModel={colModel}
          list={this.getList()}
          item={item}
          setItem={setStockItem}
          saveItem={saveStockItem}
          deleteItems={deleteStockItems}
          actions={actions}
          form={<StockForm clients={clients}/>}
          onComplete={this.onComplete}
        />
        <UploadModal show={showUploadModal} onClose={this.closeUploadModal}/>
      </div>
    );
  }
}

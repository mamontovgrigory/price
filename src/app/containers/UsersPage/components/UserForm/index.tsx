import * as React from 'react';
const {connect} = require('react-redux');

import { IUsers, IUserItem } from '@models/users';
import { IGroups } from '@models/groups';
import { setUserItem } from '@data/users';
import { Row, Input, Select } from '@ui';
import { Loc } from '@components';

interface IProps {
  users?: IUsers;
  groups?: IGroups;
  setUserItem?: (item: IUserItem) => void;
}

interface IState {
}

@connect(
  (state) => ({
    users: state.users,
    groups: state.groups
  }),
  (dispatch) => ({
    setUserItem: (item: IUserItem) => dispatch(setUserItem(item))
  })
)
export class UserForm extends React.Component<IProps, IState> {
  private changeItem = (key, value) => {
    const {setUserItem, users: {item}} = this.props;
    if (setUserItem) {
      setUserItem(Object.assign({}, item, {
        [key]: value
      }));
    }
  }

  private handleChangeLogin = (value: string) => {
    this.changeItem('login', String(value).replace(/[A-Z]/g, (match) => {
      return match.toLowerCase();
    }).replace(/[^a-z0-9@._]/g, ''));
  }

  private handleChangeGroupId = (value: any) => {
    this.changeItem('groupId', Number(value));
  }

  private handleChangePassword = (value: any) => {
    this.changeItem('password', value);
  }

  public render() {
    const {users: {item: {login, groupId, password}}, groups: {list: groupsList}} = this.props;
    const groupsOptions = groupsList.map((groupItem) => {
      return {
        value: String(groupItem.id),
        label: groupItem.name
      };
    });
    return (
      <div>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="login"/>}
            validate={true}
            value={login}
            onChange={this.handleChangeLogin}
          />
          <Select
            sm={6}
            label={<Loc locKey="group"/>}
            options={groupsOptions}
            value={String(groupId)}
            onChange={this.handleChangeGroupId}
          />
        </Row>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="password"/>}
            value={password}
            type="password"
            onChange={this.handleChangePassword}
          />
        </Row>
      </div>
    );
  }
}

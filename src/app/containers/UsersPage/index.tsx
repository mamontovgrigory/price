import * as React from 'react';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { IUsers, IUserItem } from '@models/users';
import { getUsers, setUserItem, saveUserItem, deleteUserItems } from '@data/users';
import { getGroups } from '@data/groups';
import { Crud } from '@ui';
import { Loc } from '@components';
import { UserForm } from './components';

interface IProps {
  users: IUsers;
  setUserItem: (item: IUserItem) => void;
  saveUserItem: (data: { item: IUserItem }) => Promise<null>;
  deleteUserItems: (data: { ids: number[] }) => Promise<null>;
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => {
    return dispatch(getUsers());
  }
}, {
  promise: ({store: {dispatch}}) => {
    return dispatch(getGroups());
  }
}])
@connect(
  (state) => ({
    users: state.users,
    groups: state.groups
  }),
  (dispatch) => ({
    setUserItem: (item: IUserItem) => dispatch(setUserItem(item)),
    saveUserItem: (data: { item: IUserItem }) => dispatch(saveUserItem(data)),
    deleteUserItems: (data: { ids: number[] }) => dispatch(deleteUserItems(data))
  })
)
export class UsersPage extends React.Component<IProps, {}> {
  public render() {
    const {users: {list, item}, setUserItem, saveUserItem, deleteUserItems} = this.props;
    const colModel = [
      {
        name: 'id',
        hidden: true
      },
      {
        name: 'groupId',
        hidden: true
      },
      {
        name: 'login',
        label: 'Логин'
      },
      {
        name: 'groupName',
        label: 'Группа'
      }
    ];
    return (
      <Crud
        title={<Loc locKey="users"/>}
        colModel={colModel}
        list={list}
        item={item}
        setItem={setUserItem}
        saveItem={saveUserItem}
        deleteItems={deleteUserItems}
        form={<UserForm/>}
      />
    );
  }
}

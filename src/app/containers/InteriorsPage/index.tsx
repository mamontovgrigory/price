import * as React from 'react';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { IResources } from '@models/localization';
import { IInteriorItem, IInteriors } from '@models/interiors';
import {
  getInteriors,
  setInteriorItem,
  saveInteriorItem,
  deleteInteriorItems,
  uploadInteriorsItems,
  checkInteriorsItems
} from '@data/interiors';
import { Crud, Icon } from '@ui';
import { Loc } from '@components';
import { InteriorForm } from './components';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  interiors: IInteriors;
  setInteriorItem: (item: IInteriorItem) => void;
  saveInteriorItem: (data: { item: IInteriorItem }) => Promise<null>;
  deleteInteriorItems: (data: { ids: number[] }) => Promise<null>;
  uploadInteriorsItems: (files: File[]) => Promise<null>;
  checkInteriorsItems: () => Promise<null>;
}

interface IState {
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => {
    return dispatch(getInteriors());
  }
}])
@connect(
  (state) => ({
    currentLanguage: state.localization.currentLanguage,
    resources: state.localization.resources,
    interiors: state.interiors
  }),
  (dispatch) => ({
    setInteriorItem: (item: IInteriorItem) => dispatch(setInteriorItem(item)),
    saveInteriorItem: (data: { item: IInteriorItem }) => dispatch(saveInteriorItem(data)),
    deleteInteriorItems: (data: { ids: number[] }) => dispatch(deleteInteriorItems(data)),
    uploadInteriorsItems: (files: File[]) => dispatch(uploadInteriorsItems(files)),
    checkInteriorsItems: () => dispatch(checkInteriorsItems())
  })
)
export class InteriorsPage extends React.Component<IProps, IState> {
  private getLabel = (locKey: string) => {
    const { currentLanguage, resources } = this.props;
    return Loc.getString({
      currentLanguage,
      resources,
      locKey
    });
  }

  public render() {
    const {
      interiors: {
        list,
        item
      },
      setInteriorItem,
      saveInteriorItem,
      deleteInteriorItems,
      uploadInteriorsItems,
      checkInteriorsItems
    } = this.props;
    const colModel = [
      {
        name: 'id',
        label: this.getLabel('id'),
        width: 100
      },
      {
        name: 'code',
        label: this.getLabel('interiorCode')
      },
      {
        name: 'mark',
        label: this.getLabel('mark'),
        formatter: 'select'
      },
      {
        name: 'explanation',
        label: this.getLabel('explanation')
      }
    ];
    const actions = [
      {
        title: <Icon type="search"/>,
        onClick: checkInteriorsItems
      }
    ];
    return (
      <Crud
        title={<Loc locKey="interiors"/>}
        colModel={colModel}
        list={list}
        item={item}
        setItem={setInteriorItem}
        saveItem={saveInteriorItem}
        deleteItems={deleteInteriorItems}
        upload={uploadInteriorsItems}
        actions={actions}
        form={<InteriorForm/>}
      />
    );
  }
}

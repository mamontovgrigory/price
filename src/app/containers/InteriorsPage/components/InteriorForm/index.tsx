import * as React from 'react';
const {connect} = require('react-redux');

import { IInteriors, IInteriorItem } from '@models/interiors';
import { setInteriorItem } from '@data/interiors';
import { Row, Input } from '@ui';
import { Loc } from '@components';

interface IProps {
  interiors?: IInteriors;
  setInteriorItem?: (item: IInteriorItem) => void;
}

interface IState {
}

@connect(
  (state) => ({
    interiors: state.interiors
  }),
  (dispatch) => ({
    setInteriorItem: (item: IInteriorItem) => dispatch(setInteriorItem(item))
  })
)
export class InteriorForm extends React.Component<IProps, IState> {
  private changeItem = (key, value) => {
    const {setInteriorItem, interiors: {item}} = this.props;
    if (setInteriorItem) {
      setInteriorItem(Object.assign({}, item, {
        [key]: value
      }));
    }
  }

  private handleChangeCode = (value: string) => {
    this.changeItem('code', value);
  }

  private handleChangeMark = (value: string) => {
    this.changeItem('mark', value);
  }

  private handleChangeExplanation = (value: string) => {
    this.changeItem('explanation', value);
  }

  public render() {
    const {
      interiors: {
        item: {
          code, mark, explanation
        }
      }
    } = this.props;
    return (
      <div>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="code"/>}
            validate={true}
            value={code}
            onChange={this.handleChangeCode}
          />
          <Input
            sm={6}
            label={<Loc locKey="mark"/>}
            validate={true}
            value={mark}
            onChange={this.handleChangeMark}
          />
        </Row>
        <Row>
          <Input
            label={<Loc locKey="explanation"/>}
            validate={true}
            value={explanation}
            onChange={this.handleChangeExplanation}
          />
        </Row>
      </div>
    );
  }
}

import * as React from 'react';
const {connect} = require('react-redux');

import { IResources } from '@models/localization';
import { logIn, checkSession } from '@data/users';
import { Row, Col, Input, Button, Icon } from '@ui';
import { Loc } from '@components';
const style = require('./style.css');

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  logIn?: (data: { login: string, password: string }) => Promise<boolean>;
  checkSession?: () => Promise<boolean>;
}

interface IState {
  login: string;
  password: string;
  message?: string;
}

@connect(
  (state) => ({
    currentLanguage: state.localization.currentLanguage,
    resources: state.localization.resources
  }),
  (dispatch) => ({
    logIn: (data) => dispatch(logIn(data)),
    checkSession: () => dispatch(checkSession())
  })
)
class LoginPage extends React.Component<IProps, IState> {
  private inputLogin: HTMLInputElement;
  private inputPassword: HTMLInputElement;

  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: ''
    };
  }

  public componentWillMount() {
    const {checkSession} = this.props;
    checkSession();
  }

  private setInputRefLogin = (input: HTMLInputElement) => {
    this.inputLogin = input;
  }

  private setInputRefPassword = (input: HTMLInputElement) => {
    this.inputPassword = input;
  }

  private handleChangeLogin = (value: string) => {
    this.setState({
      login: value
    });
  }

  private handleChangePassword = (value: string) => {
    this.setState({
      password: value
    });
  }

  private setMessage = (message: string) => {
    this.setState({
      message
    });
  }

  private handleSubmit = async (event) => {
    event.preventDefault();
    const {currentLanguage, resources, logIn} = this.props;
    const {login, password} = this.state;
    if (!login) {
      this.inputLogin.focus();
      this.setMessage(Loc.getString({
        currentLanguage,
        resources,
        locKey: 'inputLoginAndPassword'
      }));
    } else if (!password) {
      this.inputPassword.focus();
      this.setMessage(Loc.getString({
        currentLanguage,
        resources,
        locKey: 'inputLoginAndPassword'
      }));
    } else {
      const result = await logIn({
        login,
        password
      });
      if (!result) {
        this.setMessage(Loc.getString({
          currentLanguage,
          resources,
          locKey: 'wrongLoginOrPassword'
        }));
      }
    }
  }

  public render() {
    const {message} = this.state;
    return (
      <div className={style.LoginPage}>
        <Row>
          <Col md={4} offset={4}>
            <h4 className={style.title}>
              <Loc locKey="authorization"/>
            </h4>
            <form onSubmit={this.handleSubmit}>
              <Row>
                <Input label={<Loc locKey="login"/>}
                       validate={true}
                       onChange={this.handleChangeLogin}
                       getRef={this.setInputRefLogin}>
                  <Icon type="account_circle" prefix={true}/>
                </Input>
              </Row>
              <Row>
                <Input label={<Loc locKey="password"/>}
                       type="password"
                       validate={true}
                       onChange={this.handleChangePassword}
                       getRef={this.setInputRefPassword}>
                  <Icon type="vpn_key" prefix={true}/>
                </Input>
              </Row>
              <div className="error">
                {message}
              </div>
              <Row>
                <Col align="right">
                  <Button onClick={this.handleSubmit}>
                    <Loc locKey="logIn"/>
                  </Button>
                </Col>
              </Row>
            </form>
          </Col>
        </Row>
      </div>
    );
  }
}

export { LoginPage }

import * as React from 'react';
const {connect} = require('react-redux');

import { IModifications, IModificationItem } from '@models/modifications';
import { setModificationItem } from '@data/modifications';
import { Row, Input } from '@ui';
import { Loc } from '@components';

interface IProps {
  modifications?: IModifications;
  setModificationItem?: (item: IModificationItem) => void;
}

interface IState {
}

@connect(
  (state) => ({
    modifications: state.modifications
  }),
  (dispatch) => ({
    setModificationItem: (item: IModificationItem) => dispatch(setModificationItem(item))
  })
)

export class ModificationForm extends React.Component<IProps, IState> {
  private changeItem = (key, value) => {
    const {setModificationItem, modifications: {item}} = this.props;
    if (setModificationItem) {
      setModificationItem(Object.assign({}, item, {
        [key]: value
      }));
    }
  }

  private handleChangeCode = (value: string) => {
    this.changeItem('code', value);
  }

  private handleChangeComplectationCode = (value: string) => {
    this.changeItem('complectationCode', value);
  }

  private handleChangeComplectationBase = (value: string) => {
    this.changeItem('complectationBase', value);
  }

  private handleChangeComplectationAutoRu = (value: string) => {
    this.changeItem('complectationAutoRu', value);
  }

  private handleChangeMark = (value: string) => {
    this.changeItem('mark', value);
  }

  private handleChangeModel = (value: string) => {
    this.changeItem('model', value);
  }

  private handleChangeComplectation = (value: string) => {
    this.changeItem('complectation', value);
  }

  private handleChangeBody = (value: string) => {
    this.changeItem('body', value);
  }

  private handleChangeEngineCapacity = (value: string) => {
    this.changeItem('engineCapacity', value);
  }

  private handleChangePower = (value: string) => {
    this.changeItem('power', value);
  }

  private handleChangeTransmission = (value: string) => {
    this.changeItem('transmission', value);
  }

  private handleChangeEngineType = (value: string) => {
    this.changeItem('engineType', value);
  }

  private handleChangeDriveUnit = (value: string) => {
    this.changeItem('driveUnit', value);
  }

  private handleChangeLoadCapacity = (value: string) => {
    this.changeItem('loadCapacity', value);
  }

  public render() {
    const {
      modifications: {
        item: {
          code,
          complectationCode,
          complectationBase,
          complectationAutoRu,
          mark,
          model,
          complectation,
          body,
          engineCapacity,
          power,
          transmission,
          engineType,
          driveUnit,
          loadCapacity
        }
      }
    } = this.props;
    return (
      <div>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="code"/>}
            validate={true}
            value={code}
            onChange={this.handleChangeCode}
          />
          <Input
            sm={6}
            label={<Loc locKey="complectationCode"/>}
            validate={true}
            value={complectationCode}
            onChange={this.handleChangeComplectationCode}
          />
        </Row>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="complectationBase"/>}
            validate={true}
            value={complectationBase}
            onChange={this.handleChangeComplectationBase}
          />
          <Input
            sm={6}
            label={<Loc locKey="complectationAutoRu"/>}
            validate={true}
            value={complectationAutoRu}
            onChange={this.handleChangeComplectationAutoRu}
          />
        </Row>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="mark"/>}
            validate={true}
            value={mark}
            onChange={this.handleChangeMark}
          />
          <Input
            sm={6}
            label={<Loc locKey="model"/>}
            validate={true}
            value={model}
            onChange={this.handleChangeModel}
          />
        </Row>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="complectation"/>}
            validate={true}
            value={complectation}
            onChange={this.handleChangeComplectation}
          />
          <Input
            sm={6}
            label={<Loc locKey="body"/>}
            validate={true}
            value={body}
            onChange={this.handleChangeBody}
          />
        </Row>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="engineCapacity"/>}
            validate={true}
            value={engineCapacity}
            onChange={this.handleChangeEngineCapacity}
          />
          <Input
            sm={6}
            label={<Loc locKey="power"/>}
            validate={true}
            value={power}
            onChange={this.handleChangePower}
          />
        </Row>
        <Row>
          <Input
            sm={4}
            label={<Loc locKey="transmission"/>}
            validate={true}
            value={transmission}
            onChange={this.handleChangeTransmission}
          />
          <Input
            sm={4}
            label={<Loc locKey="engineType"/>}
            validate={true}
            value={engineType}
            onChange={this.handleChangeEngineType}
          />
          <Input
            sm={4}
            label={<Loc locKey="driveUnit"/>}
            validate={true}
            value={driveUnit}
            onChange={this.handleChangeDriveUnit}
          />
        </Row>
        <Row>
          <Input
            label={<Loc locKey="loadCapacity"/>}
            validate={true}
            value={loadCapacity}
            onChange={this.handleChangeLoadCapacity}
          />
        </Row>
      </div>
    );
  }
}

import * as React from 'react';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { IResources } from '@models/localization';
import { IModificationItem, IModifications } from '@models/modifications';
import {
  getModifications,
  setModificationItem,
  saveModificationItem,
  deleteModificationItems,
  uploadModificationItems,
  checkModificationsItems
} from '@data/modifications';
import { Crud, Icon } from '@ui';
import { Loc } from '@components';
import { ModificationForm } from './components';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  modifications: IModifications;
  setModificationItem: (item: IModificationItem) => void;
  saveModificationItem: (data: { item: IModificationItem }) => Promise<null>;
  deleteModificationItems: (data: { ids: number[] }) => Promise<null>;
  uploadModificationItems: (files: File[]) => Promise<null>;
  checkModificationsItems: () => Promise<null>;
}

interface IState {
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => {
    return dispatch(getModifications());
  }
}])
@connect(
  (state) => ({
    currentLanguage: state.localization.currentLanguage,
    resources: state.localization.resources,
    modifications: state.modifications
  }),
  (dispatch) => ({
    setModificationItem: (item: IModificationItem) => dispatch(setModificationItem(item)),
    saveModificationItem: (data: { item: IModificationItem }) => dispatch(saveModificationItem(data)),
    deleteModificationItems: (data: { ids: number[] }) => dispatch(deleteModificationItems(data)),
    uploadModificationItems: (files: File[]) => dispatch(uploadModificationItems(files)),
    checkModificationsItems: () => dispatch(checkModificationsItems())
  })
)
export class ModificationsPage extends React.Component<IProps, IState> {
  private getLabel = (locKey: string) => {
    const { currentLanguage, resources } = this.props;
    return Loc.getString({
      currentLanguage,
      resources,
      locKey
    });
  }

  public render() {
    const {
      modifications: {
        list,
        item
      },
      setModificationItem,
      saveModificationItem,
      deleteModificationItems,
      uploadModificationItems,
      checkModificationsItems
    } = this.props;
    const colModel = [
      {
        name: 'id',
        label: this.getLabel('id'),
        width: 100
      },
      {
        name: 'code',
        label: this.getLabel('modificationCode'),
        width: 100
      },
      {
        name: 'complectationCode',
        label: this.getLabel('complectationCode'),
        width: 100
      },
      {
        name: 'complectationBase',
        label: this.getLabel('complectationBase')
      },
      {
        name: 'complectationAutoRu',
        label: this.getLabel('complectationAutoRu')
      },
      {
        name: 'mark',
        label: this.getLabel('mark'),
        width: 100,
        formatter: 'select'
      },
      {
        name: 'model',
        label: this.getLabel('model'),
        width: 100
      },
      {
        name: 'complectation',
        label: this.getLabel('complectation'),
        width: 100
      },
      {
        name: 'body',
        label: this.getLabel('body'),
        width: 100,
        formatter: 'select'
      },
      {
        name: 'engineCapacity',
        label: this.getLabel('engineCapacity'),
        width: 50
      },
      {
        name: 'power',
        label: this.getLabel('power'),
        width: 50
      },
      {
        name: 'transmission',
        label: this.getLabel('transmission'),
        width: 100,
        formatter: 'select'
      },
      {
        name: 'engineType',
        label: this.getLabel('engineType'),
        width: 100,
        formatter: 'select'
      },
      {
        name: 'driveUnit',
        label: this.getLabel('driveUnit'),
        width: 100
      },
      {
        name: 'loadCapacity',
        label: this.getLabel('loadCapacity'),
        width: 100
      }
    ];
    const actions = [
      {
        title: <Icon type="search"/>,
        onClick: checkModificationsItems
      }
    ];
    return (
      <Crud
        title={<Loc locKey="modifications"/>}
        colModel={colModel}
        list={list}
        item={item}
        setItem={setModificationItem}
        saveItem={saveModificationItem}
        deleteItems={deleteModificationItems}
        upload={uploadModificationItems}
        actions={actions}
        form={<ModificationForm/>}
      />
    );
  }
}

import * as React from 'react';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { IResources } from '@models/localization';
import { IInteriorPhotoItem, IInteriorPhotos } from '@models/interiorsPhotos';
import {
  getInteriorPhotos,
  setInteriorPhotoItem,
  saveInteriorPhotoItem,
  deleteInteriorPhotoItems,
  uploadInteriorPhotosItems,
  checkInteriorPhotosItems
} from '@data/interiorsPhotos';
import { Crud, Icon } from '@ui';
import { Loc } from '@components';
import { InteriorPhotoForm } from './components';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  interiorsPhotos: IInteriorPhotos;
  setInteriorPhotoItem: (item: IInteriorPhotoItem) => void;
  saveInteriorPhotoItem: (data: { item: IInteriorPhotoItem }) => Promise<null>;
  deleteInteriorPhotoItems: (data: { ids: number[] }) => Promise<null>;
  uploadInteriorPhotosItems: (files: File[]) => Promise<null>;
  checkInteriorPhotosItems: () => Promise<null>;
}

interface IState {
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => {
    return dispatch(getInteriorPhotos());
  }
}])
@connect(
  (state) => ({
    currentLanguage: state.localization.currentLanguage,
    resources: state.localization.resources,
    interiorsPhotos: state.interiorsPhotos
  }),
  (dispatch) => ({
    setInteriorPhotoItem: (item: IInteriorPhotoItem) => dispatch(setInteriorPhotoItem(item)),
    saveInteriorPhotoItem: (data: { item: IInteriorPhotoItem }) => dispatch(saveInteriorPhotoItem(data)),
    deleteInteriorPhotoItems: (data: { ids: number[] }) => dispatch(deleteInteriorPhotoItems(data)),
    uploadInteriorPhotosItems: (files: File[]) => dispatch(uploadInteriorPhotosItems(files)),
    checkInteriorPhotosItems: () => dispatch(checkInteriorPhotosItems())
  })
)
export class InteriorPhotosPage extends React.Component<IProps, IState> {
  private getLabel = (locKey: string) => {
    const { currentLanguage, resources } = this.props;
    return Loc.getString({
      currentLanguage,
      resources,
      locKey
    });
  }

  private photosFormatter = (value: string) => {
    return value ? value.replace(/[^0-9,]/g, '').replace(/,/g, ', ') : '';
  }

  public render() {
    const {
      interiorsPhotos: {
        list,
        item
      },
      setInteriorPhotoItem,
      saveInteriorPhotoItem,
      deleteInteriorPhotoItems,
      uploadInteriorPhotosItems,
      checkInteriorPhotosItems
    } = this.props;
    const colModel = [
      {
        name: 'id',
        label: this.getLabel('id'),
        width: 100
      },
      {
        name: 'code',
        label: this.getLabel('interiorCode')
      },
      {
        name: 'client',
        label: this.getLabel('client'),
        formatter: 'select'
      },
      {
        name: 'mark',
        label: this.getLabel('mark'),
        formatter: 'select'
      },
      {
        name: 'model',
        label: this.getLabel('model'),
        formatter: 'select'
      },
      {
        name: 'color',
        label: this.getLabel('color'),
        formatter: 'select'
      },
      {
        name: 'body',
        label: this.getLabel('body'),
        formatter: 'select'
      },
      {
        name: 'photo',
        label: this.getLabel('interiorPhotos'),
        formatter: this.photosFormatter
      },
      {
        name: 'explanation',
        label: this.getLabel('explanation')
      }
    ];
    const actions = [
      {
        title: <Icon type="search"/>,
        onClick: checkInteriorPhotosItems
      }
    ];
    return (
      <Crud
        title={<Loc locKey="interiorsPhotos"/>}
        colModel={colModel}
        list={list}
        item={item}
        setItem={setInteriorPhotoItem}
        saveItem={saveInteriorPhotoItem}
        deleteItems={deleteInteriorPhotoItems}
        upload={uploadInteriorPhotosItems}
        actions={actions}
        form={<InteriorPhotoForm/>}
      />
    );
  }
}

import * as React from 'react';
const {connect} = require('react-redux');

import { IInteriorPhotos, IInteriorPhotoItem } from '@models/interiorsPhotos';
import { setInteriorPhotoItem } from '@data/interiorsPhotos';
import { Row, Col, Input, Collapsible, Carousel } from '@ui';
import { Loc } from '@components';

interface IProps {
  interiorsPhotos?: IInteriorPhotos;
  setInteriorPhotoItem?: (item: IInteriorPhotoItem) => void;
}

interface IState {
}

@connect(
  (state) => ({
    interiorsPhotos: state.interiorsPhotos
  }),
  (dispatch) => ({
    setInteriorPhotoItem: (item: IInteriorPhotoItem) => dispatch(setInteriorPhotoItem(item))
  })
)
export class InteriorPhotoForm extends React.Component<IProps, IState> {
  private changeItem = (key, value) => {
    const {setInteriorPhotoItem, interiorsPhotos: {item}} = this.props;
    if (setInteriorPhotoItem) {
      setInteriorPhotoItem(Object.assign({}, item, {
        [key]: value
      }));
    }
  }

  private handleChangeCode = (value: string) => {
    this.changeItem('code', value);
  }

  private handleChangeClient = (value: string) => {
    this.changeItem('client', value);
  }

  private handleChangeMark = (value: string) => {
    this.changeItem('mark', value);
  }

  private handleChangeModel = (value: string) => {
    this.changeItem('model', value);
  }

  private handleChangeColor = (value: string) => {
    this.changeItem('color', value);
  }

  private handleChangePhoto = (value: string) => {
    this.changeItem('photo', value);
  }

  private handleChangeExplanation = (value: string) => {
    this.changeItem('explanation', value);
  }

  private handleChangeBody = (value: string) => {
    this.changeItem('body', value);
  }

  private getPhotosList = (str?: string) => {
    return str ? String(str).replace('', '').split(',') : [];
  }

  public render() {
    const {
      interiorsPhotos: {
        item: {
          client, code, mark, photo, explanation, model, color, body
        }
      }
    } = this.props;
    const photos = this.getPhotosList(photo);
    const photosGallery = (
      <Row>
        <Col>
          <Collapsible key={photos.toString()}
                       header={<Loc locKey="frontPhotos"/>}
                       icon="photo_library"
                       content={<Carousel images={photos}/>}/>
        </Col>
      </Row>
    );
    return (
      <div>
        <Row>
          <Input
            label={<Loc locKey="client"/>}
            validate={true}
            value={client}
            onChange={this.handleChangeClient}
          />
        </Row>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="code"/>}
            validate={true}
            value={code}
            onChange={this.handleChangeCode}
          />
          <Input
            sm={6}
            label={<Loc locKey="mark"/>}
            validate={true}
            value={mark}
            onChange={this.handleChangeMark}
          />
        </Row>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="model"/>}
            validate={true}
            value={model}
            onChange={this.handleChangeModel}
          />
          <Input
            sm={6}
            label={<Loc locKey="color"/>}
            validate={true}
            value={color}
            onChange={this.handleChangeColor}
          />
        </Row>
        <Row>
          <Input
            label={<Loc locKey="body"/>}
            validate={true}
            value={body}
            onChange={this.handleChangeBody}
          />
        </Row>
        <Row>
          <Input
            label={<Loc locKey="interiorPhotos"/>}
            validate={true}
            value={photo}
            onChange={this.handleChangePhoto}
          />
        </Row>
        {photos.length > 0 && photosGallery}
        <Row>
          <Input
            label={<Loc locKey="explanation"/>}
            validate={true}
            value={explanation}
            onChange={this.handleChangeExplanation}
          />
        </Row>
      </div>
    );
  }
}

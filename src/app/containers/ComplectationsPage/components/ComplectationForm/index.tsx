import * as React from 'react';
const {connect} = require('react-redux');

import { IComplectations, IComplectationItem } from '@models/complectations';
import { setComplectationItem } from '@data/complectations';
import { Row, Input, Textarea } from '@ui';
import { Loc } from '@components';

interface IProps {
  complectations?: IComplectations;
  setComplectationItem?: (item: IComplectationItem) => void;
}

interface IState {
}

@connect(
  (state) => ({
    complectations: state.complectations
  }),
  (dispatch) => ({
    setComplectationItem: (item: IComplectationItem) => dispatch(setComplectationItem(item))
  })
)
export class ComplectationForm extends React.Component<IProps, IState> {
  private changeItem = (key, value) => {
    const {setComplectationItem, complectations: {item}} = this.props;
    if (setComplectationItem) {
      setComplectationItem(Object.assign({}, item, {
        [key]: value
      }));
    }
  }

  private handleChangeCode = (value: string) => {
    this.changeItem('code', value);
  }

  private handleChangeMark = (value: string) => {
    this.changeItem('mark', value);
  }

  private handleChangeModel = (value: string) => {
    this.changeItem('model', value);
  }

  private handleChangeDescription = (value: string) => {
    this.changeItem('description', value);
  }

  private handleChangeExplanation = (value: string) => {
    this.changeItem('explanation', value);
  }

  public render() {
    const {
      complectations: {
        item: {
          code, mark, model, description, explanation
        }
      }
    } = this.props;
    return (
      <div>
        <Row>
          <Input
            sm={4}
            label={<Loc locKey="modificationCode"/>}
            validate={true}
            value={code}
            onChange={this.handleChangeCode}
          />
          <Input
            sm={4}
            label={<Loc locKey="mark"/>}
            validate={true}
            value={mark}
            onChange={this.handleChangeMark}
          />
          <Input
            sm={4}
            label={<Loc locKey="model"/>}
            validate={true}
            value={model}
            onChange={this.handleChangeModel}
          />
        </Row>
        <Row>
          <Textarea
            label={<Loc locKey="description"/>}
            value={description}
            onChange={this.handleChangeDescription}
          />
        </Row>
        <Row>
          <Textarea
            label={<Loc locKey="explanation"/>}
            value={explanation}
            onChange={this.handleChangeExplanation}
          />
        </Row>
      </div>
    );
  }
}

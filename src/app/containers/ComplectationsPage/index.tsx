import * as React from 'react';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { IResources } from '@models/localization';
import { IComplectationItem, IComplectations } from '@models/complectations';
import {
  getComplectations,
  setComplectationItem,
  saveComplectationItem,
  deleteComplectationItems,
  uploadComplectationsItems,
  checkComplectationsItems
} from '@data/complectations';
import { Crud, Icon } from '@ui';
import { Loc } from '@components';
import { ComplectationForm } from './components';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  complectations: IComplectations;
  setComplectationItem: (item: IComplectationItem) => void;
  saveComplectationItem: (data: { item: IComplectationItem }) => Promise<null>;
  deleteComplectationItems: (data: { ids: number[] }) => Promise<null>;
  uploadComplectationsItems: (files: File[]) => Promise<null>;
  checkComplectationsItems: () => Promise<null>;
}

interface IState {
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => {
    return dispatch(getComplectations());
  }
}])
@connect(
  (state) => ({
    currentLanguage: state.localization.currentLanguage,
    resources: state.localization.resources,
    complectations: state.complectations
  }),
  (dispatch) => ({
    setComplectationItem: (item: IComplectationItem) => dispatch(setComplectationItem(item)),
    saveComplectationItem: (data: { item: IComplectationItem }) => dispatch(saveComplectationItem(data)),
    deleteComplectationItems: (data: { ids: number[] }) => dispatch(deleteComplectationItems(data)),
    uploadComplectationsItems: (files: File[]) => dispatch(uploadComplectationsItems(files)),
    checkComplectationsItems: () => dispatch(checkComplectationsItems())
  })
)
export class ComplectationsPage extends React.Component<IProps, IState> {
  private getLabel = (locKey: string) => {
    const { currentLanguage, resources } = this.props;
    return Loc.getString({
      currentLanguage,
      resources,
      locKey
    });
  }

  public render() {
    const {
      complectations: {
        list,
        item
      },
      setComplectationItem,
      saveComplectationItem,
      deleteComplectationItems,
      uploadComplectationsItems,
      checkComplectationsItems
    } = this.props;
    const colModel = [
      {
        name: 'id',
        label: this.getLabel('id'),
        width: 100
      },
      {
        name: 'code',
        label: this.getLabel('modificationCode')
      },
      {
        name: 'mark',
        label: this.getLabel('mark'),
        formatter: 'select'
      },
      {
        name: 'model',
        label: this.getLabel('model'),
        formatter: 'select'
      },
      {
        name: 'description',
        label: this.getLabel('description'),
        formatter: 'select'
      },
      {
        name: 'explanation',
        label: this.getLabel('explanation')
      }
    ];
    const actions = [
      {
        title: <Icon type="search"/>,
        onClick: checkComplectationsItems
      }
    ];
    return (
      <Crud
        title={<Loc locKey="complectations"/>}
        colModel={colModel}
        list={list}
        item={item}
        setItem={setComplectationItem}
        saveItem={saveComplectationItem}
        deleteItems={deleteComplectationItems}
        upload={uploadComplectationsItems}
        actions={actions}
        form={<ComplectationForm/>}
      />
    );
  }
}

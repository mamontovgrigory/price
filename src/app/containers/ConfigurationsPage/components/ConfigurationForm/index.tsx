import * as React from 'react';
const {connect} = require('react-redux');

import { IConfigurations, IConfigurationItem } from '@models/configurations';
import { setConfigurationItem } from '@data/configurations';
import { Row, Input } from '@ui';
import { Loc } from '@components';
import { Configuration } from './components';

interface IProps {
  configurations?: IConfigurations;
  setConfigurationItem?: (item: IConfigurationItem) => void;
}

interface IState {}

@connect(
  (state) => ({
    configurations: state.configurations
  }),
  (dispatch) => ({
    setConfigurationItem: (item: IConfigurationItem) => dispatch(setConfigurationItem(item))
  })
)
export class ConfigurationForm extends React.Component<IProps, IState> {
  private changeItem = (key, value) => {
    const {setConfigurationItem, configurations: {item}} = this.props;
    if (setConfigurationItem) {
      setConfigurationItem(Object.assign({}, item, {
        [key]: value
      }));
    }
  }

  private handleChangeName = (value: string) => {
    this.changeItem('name', value);
  }

  private handleChangeConfiguration = (value: string) => {
    this.changeItem('configuration', value);
  }

  public render() {
    const {
      configurations: {
        item: {
          name,
          configuration
        }
      }
    } = this.props;
    return (
      <div>
        <Row>
          <Input
            label={<Loc locKey="name"/>}
            validate={true}
            value={name}
            onChange={this.handleChangeName}
          />
        </Row>
        <Configuration configuration={configuration} onChange={this.handleChangeConfiguration}/>
      </div>
    );
  }
}

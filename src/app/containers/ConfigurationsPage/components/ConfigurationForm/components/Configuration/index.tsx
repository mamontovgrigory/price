import * as React from 'react';
import { isArray, isString } from 'lodash';

import { columnsMapping, fileColumnsMapping } from '@data/stock/constants';
import { Row, Col, Input, Checkbox, Select, Button, Icon } from '@ui';
import { Loc } from '@components';

interface IProps {
  configuration: any;
  onChange: (value: string) => void;
}

interface IState {
}

export class Configuration extends React.Component<IProps, IState> {
  private separator = ',';

  private handleChangeConfiguration = (configuration: any) => {
    const {onChange} = this.props;
    onChange(JSON.stringify(configuration));
  }

  private handleClickAddRule = () => {
    const list = this.getList();
    list.push({
      file: [{}],
      base: [{}]
    });
    this.handleChangeConfiguration(list);
  }

  private handleClickAddCondition = (ruleIndex: number, type: string) => {
    const list = this.getList();
    list[ruleIndex][type].push({});
    this.handleChangeConfiguration(list);
  }

  private handleDeleteCondition = (ruleIndex: number, type: string, conditionIndex: number) => {
    const list = this.getList();
    list[ruleIndex][type].splice(conditionIndex, 1);
    this.handleChangeConfiguration(list);
  }

  private handleClickDeleteRule = (index: number) => {
    const list = this.getList();
    list.splice(index, 1);
    this.handleChangeConfiguration(list);
  }

  private handleChangeIfExists = (index: number, checked: boolean) => {
    const list = this.getList();
    list[index].ifExists = checked;
    this.handleChangeConfiguration(list);
  }

  private handleChangeIntersection = (index: number, checked: boolean) => {
    const list = this.getList();
    list[index].intersection = checked;
    this.handleChangeConfiguration(list);
  }

  private handleChangeColumn = (ruleIndex: number, type: string, conditionIndex: number, value: string) => {
    const list = this.getList();
    list[ruleIndex][type][conditionIndex].column = value;
    this.handleChangeConfiguration(list);
  }

  private handleChangeLike = (ruleIndex: number, type: string, conditionIndex: number, value: string) => {
    const list = this.getList();
    list[ruleIndex][type][conditionIndex].like = value.split(this.separator);
    this.handleChangeConfiguration(list);
  }

  private handleChangeInvert = (ruleIndex: number, type: string, conditionIndex: number, checked: boolean) => {
    const list = this.getList();
    list[ruleIndex][type][conditionIndex].invert = checked;
    this.handleChangeConfiguration(list);
  }

  private getList = () => {
    const {configuration} = this.props;
    return configuration ? JSON.parse(configuration) : [];
  }

  private renderCondition = (ruleIndex: number, type: string, conditionIndex: number, rule) => {
    const likeValue = isArray(rule.like) ?
      rule.like.join(this.separator) :
      rule.like;
    const onChangeColumn = (value) => this.handleChangeColumn(ruleIndex, type, conditionIndex, value);
    const onChangeLike = (value) => this.handleChangeLike(ruleIndex, type, conditionIndex, value);
    const onChangeInvert = (checked) => this.handleChangeInvert(ruleIndex, type, conditionIndex, checked);
    const deleteCondition = () => this.handleDeleteCondition(ruleIndex, type, conditionIndex);
    const deleteButton = (
      <Button onClick={deleteCondition}>
        <Icon type="delete"/>
        <Loc locKey="deleteCondition"/>
      </Button>
    );
    const mapping = type === 'file' ? fileColumnsMapping : columnsMapping;
    const columnOptions = Object.keys(mapping).map((key) => {
      return {
        value: key,
        label: mapping[key]
      };
    });
    const columnValue = rule.column && isString(rule.column) ? rule.column.split(this.separator) : rule.column;
    return (
      <Row key={conditionIndex}>
        <Col className="card-panel">
          <Select
            label={<Loc locKey="column"/>}
            options={columnOptions}
            value={columnValue}
            onChange={onChangeColumn}
            multiple={true}
          />
          <Input label={<Loc locKey="like"/>} value={likeValue} onChange={onChangeLike}/>
          <Checkbox label={<Loc locKey="invert"/>} checked={rule.invert} onChange={onChangeInvert}/>
          {conditionIndex !== 0 && deleteButton}
        </Col>
      </Row>
    );
  }

  private renderRule = (rule, index) => {
    const deleteRule = () => this.handleClickDeleteRule(index);
    const onChangeIfExists = (checked) => this.handleChangeIfExists(index, checked);
    const onChangeIntersection = (checked) => this.handleChangeIntersection(index, checked);
    const addFileCondition = () => this.handleClickAddCondition(index, 'file');
    const addBaseCondition = () => this.handleClickAddCondition(index, 'base');
    const ruleNumber = index + 1;
    const deleteButton = (
      <Button onClick={deleteRule}>
        <Icon type="delete"/>
        <Loc locKey="deleteRule"/>&nbsp;{ruleNumber}
      </Button>
    );
    return (
      <div key={index}>
        <Row>
          <Col md={6}>
            <h5>
              <Loc locKey="rule"/>&nbsp;{ruleNumber}
            </h5>
          </Col>
          <Col md={6}>
            {index !== 0 && deleteButton}
          </Col>
        </Row>
        <Row>
          <Col sm={6}>
            <Loc locKey="file"/>
          </Col>
          <Col sm={6}>
            <Loc locKey="base"/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Checkbox
              label={<Loc locKey="ifExists"/>}
              checked={rule.ifExists}
              onChange={onChangeIfExists}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <Checkbox
              label={<Loc locKey="intersection"/>}
              checked={rule.intersection}
              onChange={onChangeIntersection}
            />
          </Col>
        </Row>
        <Row>
          <Col sm={6}>
            {rule.file.map((condition, conditionIndex) =>
              this.renderCondition(index, 'file', conditionIndex, condition))}
            <Button onClick={addFileCondition}>
              <Icon type="playlist_add"/>
              <Loc locKey="addCondition"/>
            </Button>
          </Col>
          <Col sm={6}>
            {rule.base.map((condition, conditionIndex) =>
              this.renderCondition(index, 'base', conditionIndex, condition))}
            <Button onClick={addBaseCondition}>
              <Icon type="playlist_add"/>
              <Loc locKey="addCondition"/>
            </Button>
          </Col>
        </Row>
      </div>
    );
  }

  public render() {
    const list = this.getList();
    return (
      <div>
        {list.map((rule, index) => this.renderRule(rule, index))}
        <Row>
          <Col>
            <Button onClick={this.handleClickAddRule}>
              <Icon type="playlist_add"/>
              <Loc locKey="addRule"/>
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

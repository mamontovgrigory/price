import * as React from 'react';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { IResources } from '@models/localization';
import { IConfigurationItem, IConfigurations } from '@models/configurations';
import {
  getConfigurations,
  setConfigurationItem,
  saveConfigurationItem,
  deleteConfigurationItems
} from '@data/configurations';
import { Crud } from '@ui';
import { Loc } from '@components';
import { ConfigurationForm } from './components';

interface IProps {
  currentLanguage?: string;
  resources?: IResources;
  configurations: IConfigurations;
  setConfigurationItem: (item: IConfigurationItem) => void;
  saveConfigurationItem: (data: { item: IConfigurationItem }) => Promise<null>;
  deleteConfigurationItems: (data: { ids: number[] }) => Promise<null>;
  uploadConfigurationsItems: (files: File[]) => Promise<null>;
}

interface IState {
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => {
    return dispatch(getConfigurations());
  }
}])
@connect(
  (state) => ({
    currentLanguage: state.localization.currentLanguage,
    resources: state.localization.resources,
    configurations: state.configurations
  }),
  (dispatch) => ({
    setConfigurationItem: (item: IConfigurationItem) => dispatch(setConfigurationItem(item)),
    saveConfigurationItem: (data: { item: IConfigurationItem }) => dispatch(saveConfigurationItem(data)),
    deleteConfigurationItems: (data: { ids: number[] }) => dispatch(deleteConfigurationItems(data))
  })
)
export class ConfigurationsPage extends React.Component<IProps, IState> {
  private getLabel = (locKey: string) => {
    const { currentLanguage, resources } = this.props;
    return Loc.getString({
      currentLanguage,
      resources,
      locKey
    });
  }

  public render() {
    const {
      configurations: {
        list,
        item
      },
      setConfigurationItem,
      saveConfigurationItem,
      deleteConfigurationItems
    } = this.props;
    const colModel = [
      {
        name: 'id',
        hidden: true
      },
      {
        name: 'name',
        label: this.getLabel('name')
      }
    ];
    return (
      <Crud
        title={<Loc locKey="configurations"/>}
        colModel={colModel}
        list={list}
        item={item}
        setItem={setConfigurationItem}
        saveItem={saveConfigurationItem}
        deleteItems={deleteConfigurationItems}
        form={<ConfigurationForm/>}
      />
    );
  }
}

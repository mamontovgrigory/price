import * as React from 'react';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { IGroupItem, IGroups } from '@models/groups';
import { getGroups, setGroupItem, saveGroupItem, deleteGroupItems } from '@data/groups';
import { getPermissions } from '@data/permissions';
import { Crud } from '@ui';
import { Loc } from '@components';
import { GroupForm } from './components';

interface IProps {
  groups: IGroups;
  setGroupItem: (item: IGroupItem) => void;
  saveGroupItem: (data: {item: IGroupItem}) => Promise<null>;
  deleteGroupItems: (data: {ids: number[]}) => Promise<null>;
}

interface IState {
}

@asyncConnect([{
  promise: ({store: {dispatch}}) => {
    return dispatch(getGroups());
  }
}, {
  promise: ({store: {dispatch}}) => {
    return dispatch(getPermissions());
  }
}])
@connect(
  (state) => ({
    groups: state.groups
  }),
  (dispatch) => ({
    setGroupItem: (item: IGroupItem) => dispatch(setGroupItem(item)),
    saveGroupItem: (data: {item: IGroupItem}) => dispatch(saveGroupItem(data)),
    deleteGroupItems: (data: {ids: number[]}) => dispatch(deleteGroupItems(data))
  })
)
class GroupsPage extends React.Component<IProps, IState> {
  public render() {
    const {groups: {list, item}, setGroupItem, saveGroupItem, deleteGroupItems} = this.props;
    const colModel = [
      {
        name: 'id',
        hidden: true
      },
      {
        name: 'name',
        label: 'Группа'
      }
    ];
    return (
      <Crud
        title={<Loc locKey="groups"/>}
        colModel={colModel}
        list={list}
        item={item}
        setItem={setGroupItem}
        saveItem={saveGroupItem}
        deleteItems={deleteGroupItems}
        form={<GroupForm/>}
      />
    );
  }
}

export { GroupsPage }

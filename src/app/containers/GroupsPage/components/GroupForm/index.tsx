import * as React from 'react';
const {connect} = require('react-redux');

import { IGroups, IGroupItem } from '@models/groups';
import { IPermissions } from '@models/permissions';
import { setGroupItem } from '@data/groups';
import { Row, Input, Checkbox } from '@ui';
import { Loc } from '@components';

interface IProps {
  groups?: IGroups;
  permissions?: IPermissions;
  setGroupItem?: (item: IGroupItem) => void;
}

interface IState {
}

@connect(
  (state) => ({
    groups: state.groups,
    permissions: state.permissions
  }),
  (dispatch) => ({
    setGroupItem: (item: IGroupItem) => dispatch(setGroupItem(item))
  })
)
export class GroupForm extends React.Component<IProps, IState> {
  private changeItem = (key, value) => {
    const {setGroupItem, groups: {item}} = this.props;
    if (setGroupItem) {
      setGroupItem(Object.assign({}, item, {
        [key]: value
      }));
    }
  }

  private handleChangeName = (value: string) => {
    this.changeItem('name', value);
  }

  private handleChangeSettings = (value: any) => {
    this.changeItem('settings', value);
  }

  public render() {
    const {groups: {item: {name, settings}}, permissions: {list}} = this.props;
    return (
      <div>
        <Row>
          <Input
            sm={6}
            label={<Loc locKey="groupName"/>}
            value={name}
            validate={true}
            onChange={this.handleChangeName}
          />
        </Row>
        {list && list.map((permission) => {
          const valueObject = settings && settings.find((valueItem) => {
            return valueItem.permissionId === permission.id;
          });
          const checked = Boolean(valueObject && valueObject.value === 'true');
          const onChange = (checked: boolean) => {
            const newValues = [].concat(settings);
            const newValueItem = {
              permissionId: permission.id,
              value: String(checked)
            };
            const valueIndex = newValues.findIndex((valueItem) => {
              return valueItem.permissionId === permission.id;
            });
            if (valueIndex === -1) {
              newValues.push(newValueItem);
            } else {
              newValues[valueIndex] = newValueItem;
            }
            this.handleChangeSettings(newValues);
          };
          return (
            <Row key={permission.id}>
              <Checkbox label={permission.name} checked={checked} onChange={onChange}/>
            </Row>
          );
        })}
      </div>
    );
  }
}

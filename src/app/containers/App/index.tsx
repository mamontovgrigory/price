import * as React from 'react';
import * as Helmet from 'react-helmet';
import * as  ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { isEqual } from 'lodash';
const {connect} = require('react-redux');
const md5 = require('md5');

const {version} = require('../../../../package.json');
const appConfig = require('../../../../config/main');
import { IUsers } from '@models/users';
import { INavigation, INavItem, ISetNavItems } from '@models/navigation';
import { ILanguage, ISetCurrentLanguageAction, ISetLanguagesAction, ISetResources } from '@models/localization';
import {
  setCurrentLanguage,
  setLanguages,
  setResources
} from '@data/localization';
import { setNavItems } from '@data/navigation';
import { logout } from '@data/users';
import { Navbar } from '@ui';
import { LoginPage, Log } from '@containers';
import { Logo, Loc } from '@components';
const style = require('./style.scss');
const languages: ILanguage[] = require('./languages.json');
const resources = require('./resources.json');

interface IProps {
  location?: Location;
  users: IUsers;
  navigation: INavigation;
  setCurrentLanguage: Redux.ActionCreator<ISetCurrentLanguageAction>;
  setLanguages: Redux.ActionCreator<ISetLanguagesAction>;
  setResources: Redux.ActionCreator<ISetResources>;
  setNavItems: Redux.ActionCreator<ISetNavItems>;
  logout: () => void;
}

interface IState {

}

@connect(
  (state) => ({
    users: state.users,
    navigation: state.navigation
  }),
  (dispatch) => ({
    setCurrentLanguage: (languageCode) => dispatch(setCurrentLanguage(languageCode)),
    setLanguages: (languages) => dispatch(setLanguages(languages)),
    setResources: (resources) => dispatch(setResources(resources)),
    setNavItems: (navItems) => dispatch(setNavItems(navItems)),
    logout: () => dispatch(logout())
  })
)
class App extends React.Component<IProps, IState> {
  public componentDidMount() {
    const {setCurrentLanguage, setLanguages, setResources} = this.props;
    const currentLanguage: string = languages[0].code;
    setCurrentLanguage(currentLanguage);
    setLanguages(languages);
    setResources(resources);
    this.setNavItems(this.props);
  }

  public componentWillReceiveProps(nextProps: IProps) {
    const {users} = this.props;
    if (!isEqual(users.account, nextProps.users.account)) {
      this.setNavItems(nextProps);
    }
  }

  private setNavItems = (props: IProps) => {
    const {setNavItems} = this.props;
    const currentLanguage: string = languages[0].code;
    const permissions =
      props.users &&
      props.users.account &&
      props.users.account.permissions;
    const navItems: INavItem[] = [
      {
        name: Loc.getString({currentLanguage, resources, locKey: 'home'}),
        to: '/',
        icon: 'home'
      }
    ];
    if (permissions && permissions.usersManage) {
      navItems.push({
        name: Loc.getString({currentLanguage, resources, locKey: 'users'}),
        to: '/users',
        icon: 'perm_identity'
      });
    }
    if (permissions && permissions.groupsManage) {
      navItems.push({
        name: Loc.getString({currentLanguage, resources, locKey: 'groups'}),
        to: '/groups',
        icon: 'supervisor_account'
      });
    }
    navItems.push({
      name: Loc.getString({currentLanguage, resources, locKey: 'configurations'}),
      to: '/configurations',
      icon: 'settings'
    });
    navItems.push({
      name: Loc.getString({currentLanguage, resources, locKey: 'price'}),
      to: '/price',
      icon: 'photo_camera'
    });
    navItems.push({
      name: Loc.getString({currentLanguage, resources, locKey: 'modifications'}),
      icon: 'directions_car',
      to: '/modifications'
    });
    navItems.push({
      name: Loc.getString({currentLanguage, resources, locKey: 'colors'}),
      to: '/colors',
      icon: 'color_lens'
    });
    navItems.push({
      name: Loc.getString({currentLanguage, resources, locKey: 'interiors'}),
      to: '/interiors',
      icon: 'photo'
    });
    navItems.push({
      name: Loc.getString({currentLanguage, resources, locKey: 'interiorsPhotos'}),
      to: '/interior-photos',
      icon: 'photo_library'
    });
    navItems.push({
      name: Loc.getString({currentLanguage, resources, locKey: 'options'}),
      to: '/options',
      icon: 'playlist_add_check'
    });
    navItems.push({
      name: Loc.getString({currentLanguage, resources, locKey: 'complectations'}),
      to: '/complectations',
      icon: 'devices_other'
    });
    setNavItems(navItems);
  }

  public render() {
    const {location: {pathname}, users: {account}, navigation: {navItems}, children, logout} = this.props;
    const key = md5(pathname);
    const transitionName = {
      enter: style.page_transition_enter,
      enterActive: style.page_transition_enter_active,
      leave: style.page_transition_leave,
      leaveActive: style.page_transition_leave_active,
      appear: style.page_transition_appear,
      appearActive: style.page_transition_appear_active
    };
    const accountItem = account ? {
      name: account.login
    } : null;
    return (
      <section className={style.App}>
        <Helmet {...appConfig.app} {...appConfig.app.head}/>
        <Navbar
          version={version}
          logo={<Logo/>}
          links={navItems}
          account={accountItem}
          onExit={logout}
        />
        <div className={style.page}>
          <ReactCSSTransitionGroup
            key={key}
            component="div"
            transitionName={transitionName}
            transitionAppear={true}
            transitionAppearTimeout={500}
            transitionEnter={true}
            transitionEnterTimeout={500}
            transitionLeave={false}>
            {account ? children : <LoginPage/>}
          </ReactCSSTransitionGroup>
        </div>
        <Log/>
      </section>
    );
  }
}

export { App }

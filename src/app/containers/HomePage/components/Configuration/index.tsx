import * as React from 'react';
import { isArray } from 'lodash';

import { columnsMapping, fileColumnsMapping } from '@data/stock/constants';
import { Collapsible, Row, Col, Input, Checkbox } from '@ui';
import { Loc } from '@components';

interface IProps {
  configuration: any;
}

interface IState {
}

export class Configuration extends React.Component<IProps, IState> {
  private separator = '/';

  private renderRule = (rule, mapping) => {
    const value = isArray(rule.column) ?
      rule.column.map((column) => mapping[column]).join(this.separator) :
      mapping[rule.column];
    const likeValue = isArray(rule.like) ?
      rule.like.join(this.separator) :
      rule.like;
    return (
      <Row>
        <Col className="card-panel">
          <Input label={<Loc locKey="column"/>} value={value} disabled={true}/>
          {rule.like && <Input label={<Loc locKey="like"/>} value={likeValue} disabled={true}/>}
          {rule.invert && <Checkbox label={<Loc locKey="invert"/>} checked={rule.invert} disabled={true}/>}
        </Col>
      </Row>
    );
  }

  private renderContent = () => {
    const {configuration} = this.props;
    return configuration.map((rules, index) => {
      const fileRules = rules.file;
      const baseRules = rules.base;
      return (
        <div key={index}>
          <Row>
            <Col sm={6}>
              <h5>
                <Loc locKey="file"/>
              </h5>
            </Col>
            <Col sm={6}>
              <h5>
                <Loc locKey="base"/>
              </h5>
            </Col>
          </Row>
          <Row>
            <Col>
              <Loc locKey="rule"/> {index + 1}
            </Col>
          </Row>
          {
            rules.ifExists && (
              <Row>
                <Col>
                  <Checkbox label={<Loc locKey="ifExists"/>} checked={rules.ifExists} disabled={true}/>
                </Col>
              </Row>
            )
          }
          {
            rules.intersection && (
              <Row>
                <Col>
                  <Checkbox label={<Loc locKey="intersection"/>} checked={rules.intersection} disabled={true}/>
                </Col>
              </Row>
            )
          }
          <Row>
            <Col sm={6}>
              {fileRules.map((rule) => this.renderRule(rule, fileColumnsMapping))}
            </Col>
            <Col sm={6}>
              {baseRules.map((rule) => this.renderRule(rule, columnsMapping))}
            </Col>
          </Row>
        </div>
      );
    });
  }

  public render() {
    return (
      <Collapsible
        icon="settings"
        header={<Loc locKey="configuration"/>}
        content={this.renderContent()}/>
    );
  }
}

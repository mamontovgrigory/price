import * as React from 'react';
const {connect} = require('react-redux');
const {asyncConnect} = require('redux-connect');

import { INavigation } from '@models/navigation';
import { IStock } from '@models/stock';
import { IConfigurations } from '@models/configurations';
import { getClients, generatePrice, fillPricePhotos, getTemplate, initProcess } from '@data/stock';
import { getConfigurations } from '@data/configurations';
import { Row, Col, Button, Input, Select, Checkbox, Dropzone, ProgressBar } from '@ui';
import { Loc } from '@components';
import { defaultConfiguration } from '@data/stock/constants';
import { Configuration } from './components';
const style = require('./style.scss');

interface IProps {
  navigation: INavigation;
  stock: IStock;
  configurations: IConfigurations;
  generatePrice(data, files): Promise<null>;
  fillPricePhotos(data, files): Promise<null>;
  getTemplate(): Promise<null>;
  initProcess(data, files): void;
}

interface IState {
  client: string;
  frontLength: number;
  backLength: number;
  interiorsLength: number;
  onlySalon: boolean;
  files: File[];
  configurationId?: number;
}

@asyncConnect([
  {
    promise: ({store: {dispatch}}) => {
      return dispatch(getClients());
    }
  },
  {
    promise: ({store: {dispatch}}) => {
      return dispatch(getConfigurations());
    }
  }
])
@connect(
  (state) => ({
    navigation: state.navigation,
    stock: state.stock,
    configurations: state.configurations
  }),
  {
    generatePrice,
    fillPricePhotos,
    getTemplate,
    initProcess
  }
)
export class HomePage extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      client: '',
      frontLength: 1,
      backLength: 3,
      interiorsLength: 0,
      onlySalon: false,
      files: []
    };
  }

  private handleChangeClient = (client: string) => {
    this.setState({
      client
    });
  }

  private handleChangeFrontLength = (value: string) => {
    this.setState({
      frontLength: Number(value)
    });
  }

  private handleChangeBackLength = (value: string) => {
    this.setState({
      backLength: Number(value)
    });
  }

  private handleChangeInteriorsLength = (value: string) => {
    this.setState({
      interiorsLength: Number(value)
    });
  }

  private handleChangeSalon = (checked: boolean) => {
    this.setState({
      onlySalon: checked
    });
  }

  private handleChangeFiles = (files: File[]) => {
    this.setState({
      files
    });
  }

  private handleChangeConfigurationId = (value: string) => {
    this.setState({
      configurationId: value ? Number(value) : null
    });
  }

  private handleSubmit = () => {
    const {initProcess} = this.props;
    const {client, frontLength, backLength, interiorsLength, onlySalon, files} = this.state;
    const configuration = this.getSelectedConfiguration();
    initProcess({client, configuration, frontLength, backLength, interiorsLength, onlySalon}, files);
  }

  private handleClickTemplateButton = () => {
    const {getTemplate} = this.props;
    getTemplate();
  }

  private getSelectedConfiguration = () => {
    const {configurations} = this.props;
    const {configurationId} = this.state;
    return Number.isInteger(configurationId) ?
      JSON.parse(configurations.list.find((item) => item.id === configurationId).configuration) :
      defaultConfiguration;
  }

  public render() {
    const {stock, configurations} = this.props;
    const {client, frontLength, backLength, interiorsLength, configurationId} = this.state;
    const selectedConfiguration = this.getSelectedConfiguration();
    const clientOptions = stock.clientsList.map((item) => {
      return {
        value: item,
        label: item
      };
    });
    const configurationOptions = configurations.list.map((item) => {
      return {
        value: String(item.id),
        label: item.name
      };
    });
    const loading = Number.isInteger(stock.progress);
    return (
      <div className={style.HomePage}>
        <Row>
          <Col md={6} offset={3}>
            <Row>
              <Col>
                <h4>
                  <Loc locKey="home"/>
                </h4>
              </Col>
            </Row>
            <Row>
              <Col>
                <Select
                  label={<Loc locKey="client"/>}
                  options={clientOptions}
                  value={client}
                  onChange={this.handleChangeClient}
                />
              </Col>
            </Row>
            <Row>
              <Col sm={4}>
                <Input
                  label={<Loc locKey="frontPhotos"/>}
                  type="number"
                  value={String(frontLength)}
                  onChange={this.handleChangeFrontLength}
                />
              </Col>
              <Col sm={4}>
                <Input
                  label={<Loc locKey="backPhotos"/>}
                  type="number"
                  value={String(backLength)}
                  onChange={this.handleChangeBackLength}
                />
              </Col>
              <Col sm={4}>
                <Input
                  label={<Loc locKey="interiorsPhotos"/>}
                  type="number"
                  value={String(interiorsLength)}
                  onChange={this.handleChangeInteriorsLength}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <Checkbox label="Только фото салона" onChange={this.handleChangeSalon}/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Dropzone onChange={this.handleChangeFiles}/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Select
                  label={<Loc locKey="configuration"/>}
                  options={configurationOptions}
                  value={String(configurationId)}
                  onChange={this.handleChangeConfigurationId}
                />
              </Col>
            </Row>
            <Configuration configuration={selectedConfiguration}/>
            <Row>
              <Col>
                <Button disabled={loading} onClick={this.handleSubmit}>
                  <Loc locKey="load"/>
                </Button>
                <Button onClick={this.handleClickTemplateButton}>
                  <Loc locKey="downloadTemplate"/>
                </Button>
              </Col>
            </Row>
            {loading && <ProgressBar progress={stock.progress}/>}
          </Col>
        </Row>
      </div>
    );
  }
}

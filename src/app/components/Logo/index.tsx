import * as React from 'react';

export const Logo = () => (
  <img src={require('./content/logo.png')}/>
);

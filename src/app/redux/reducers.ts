import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
const {reducer} = require('redux-connect');

import { colorsReducer } from '@data/colors';
import { complectationsReducer } from '@data/complectations';
import { configurationsReducer } from '@data/configurations';
import { groupsReducer } from '@data/groups';
import { interiorsPhotosReducer } from '@data/interiorsPhotos';
import { interiorsReducer } from '@data/interiors';
import { localizationReducer } from '@data/localization';
import { logReducer } from '@data/log';
import { modificationsReducer } from '@data/modifications';
import { navigationReducer } from '@data/navigation';
import { optionsReducer } from '@data/options';
import { permissionsReducer } from '@data/permissions';
import { stockReducer } from '@data/stock';
import { usersReducer } from '@data/users';
import { IStore } from './IStore';

const rootReducer: Redux.Reducer<IStore> = combineReducers<IStore>({
  routing: routerReducer,
  reduxAsyncConnect: reducer,
  colors: colorsReducer,
  complectations: complectationsReducer,
  configurations: configurationsReducer,
  groups: groupsReducer,
  interiorsPhotos: interiorsPhotosReducer,
  interiors: interiorsReducer,
  localization: localizationReducer,
  log: logReducer,
  modifications: modificationsReducer,
  navigation: navigationReducer,
  options: optionsReducer,
  permissions: permissionsReducer,
  stock: stockReducer,
  users: usersReducer
});

export default rootReducer;

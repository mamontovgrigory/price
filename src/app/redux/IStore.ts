import { IColors } from '@models/colors';
import { IComplectations } from '@models/complectations';
import { IGroups } from '@models/groups';
import { IInteriorPhotos } from '@models/interiorsPhotos';
import { IInteriors } from '@models/interiors';
import { ILocalization } from '@models/localization';
import { ILog } from '@models/log';
import { IModifications } from '@models/modifications';
import { INavigation } from '@models/navigation';
import { IOptions } from '@models/options';
import { IPermissions } from '@models/permissions';
import { IStock } from '@models/stock';
import { IUsers } from '@models/users';

export interface IStore {
  colors: IColors;
  complectations: IComplectations;
  groups: IGroups;
  interiorsPhotos: IInteriorPhotos;
  interiors: IInteriors;
  localization: ILocalization;
  log: ILog;
  modifications: IModifications;
  navigation: INavigation;
  options: IOptions;
  permissions: IPermissions;
  stock: IStock;
  users: IUsers;
}

import { INavigation, INavItem, ISetNavItems } from '@models/navigation';

const SET_NAV_ITEMS: string = 'navigation/SET_NAV_ITEMS';

const initialState: INavigation = {
  navItems: []
};

export function navigationReducer(state: INavigation = initialState, action: Redux.Action) {
  switch (action.type) {
    case SET_NAV_ITEMS:
      const {navItems} = action as ISetNavItems;
      return Object.assign({}, state, {navItems});
    default:
      return state;
  }
}

export function setNavItems(navItems: INavItem[]) {
  return {
    type: SET_NAV_ITEMS,
    navItems
  };
}

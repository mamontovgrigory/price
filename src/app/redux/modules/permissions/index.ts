import { IPermissions, IPermissionsAction } from '@models/permissions';
import { request } from '@utils/request';

export const GET_REQUEST: string = 'permissions/GET_REQUEST';
export const GET_SUCCESS: string = 'permissions/GET_SUCCESS';
export const GET_FAILURE: string = 'permissions/GET_FAILURE';

const initialState: IPermissions = {
  isFetching: false,
  list: []
};

export function permissionsReducer(state = initialState, action: IPermissionsAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    default:
      return state;
  }
}

export function getRequest(): IPermissionsAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): IPermissionsAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): IPermissionsAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getPermissions() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response = await request({
        url: '/Api/Permissions/GetList'
      });
      dispatch(getSuccess(response));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

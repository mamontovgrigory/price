import { IInteriorPhotos, IInteriorPhotoItem, IInteriorPhotosAction } from '@models/interiorsPhotos';
import { getLog, openLog } from '@data/log';
import { request } from '@utils/request';

export const GET_REQUEST: string = 'interiorsPhotos/GET_REQUEST';
export const GET_SUCCESS: string = 'interiorsPhotos/GET_SUCCESS';
export const GET_FAILURE: string = 'interiorsPhotos/GET_FAILURE';
export const SAVE_REQUEST: string = 'interiorsPhotos/SAVE_REQUEST';
export const SAVE_SUCCESS: string = 'interiorsPhotos/SAVE_SUCCESS';
export const SAVE_FAILURE: string = 'interiorsPhotos/SAVE_FAILURE';
export const DELETE_REQUEST: string = 'interiorsPhotos/DELETE_REQUEST';
export const DELETE_SUCCESS: string = 'interiorsPhotos/DELETE_SUCCESS';
export const DELETE_FAILURE: string = 'interiorsPhotos/DELETE_FAILURE';
export const UPLOAD_REQUEST: string = 'interiorsPhotos/UPLOAD_REQUEST';
export const UPLOAD_SUCCESS: string = 'interiorsPhotos/UPLOAD_SUCCESS';
export const UPLOAD_FAILURE: string = 'interiorsPhotos/UPLOAD_FAILURE';
export const CHECK_REQUEST: string = 'interiorsPhotos/CHECK_REQUEST';
export const CHECK_SUCCESS: string = 'interiorsPhotos/CHECK_SUCCESS';
export const CHECK_FAILURE: string = 'interiorsPhotos/CHECK_FAILURE';
export const SET_ITEM: string = 'interiorsPhotos/SET_ITEM';

const initialState: IInteriorPhotos = {
  isFetching: false,
  item: {},
  list: []
};

export function interiorsPhotosReducer(state = initialState, action: IInteriorPhotosAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SAVE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case SAVE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case SAVE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case DELETE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case DELETE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case DELETE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case UPLOAD_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case UPLOAD_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case CHECK_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case CHECK_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case CHECK_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case SET_ITEM:
      return Object.assign({}, state, {
        item: action.payload.item
      });

    default:
      return state;
  }
}

export function getRequest(): IInteriorPhotosAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): IInteriorPhotosAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): IInteriorPhotosAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getInteriorPhotos() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response: any = await request({
        url: '/Api/InteriorPhotos/GetList'
      });
      dispatch(getSuccess(response));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function saveRequest(): IInteriorPhotosAction {
  return {
    type: SAVE_REQUEST
  };
}

export function saveSuccess(): IInteriorPhotosAction {
  return {
    type: SAVE_SUCCESS
  };
}

export function saveFailure(message: any): IInteriorPhotosAction {
  return {
    type: SAVE_FAILURE,
    payload: {
      message
    }
  };
}

export function saveInteriorPhotoItem(data: {item: IInteriorPhotoItem}) {
  return async (dispatch) => {
    dispatch(saveRequest());
    try {
      await request({
        url: '/Api/InteriorPhotos/Save',
        data
      });
      dispatch(saveSuccess());
      dispatch(getInteriorPhotos());
    } catch (error) {
      dispatch(saveFailure(error));
    }
  };
}

export function deleteRequest(): IInteriorPhotosAction {
  return {
    type: DELETE_REQUEST
  };
}

export function deleteSuccess(): IInteriorPhotosAction {
  return {
    type: DELETE_SUCCESS
  };
}

export function deleteFailure(message: any): IInteriorPhotosAction {
  return {
    type: DELETE_FAILURE,
    payload: {
      message
    }
  };
}

export function deleteInteriorPhotoItems(data: {ids: number[]}) {
  return async (dispatch) => {
    dispatch(deleteRequest());
    try {
      await request({
        url: '/Api/InteriorPhotos/Delete',
        data
      });
      dispatch(deleteSuccess());
      dispatch(getInteriorPhotos());
    } catch (error) {
      dispatch(deleteFailure(error));
    }
  };
}

export function uploadRequest(): IInteriorPhotosAction {
  return {
    type: UPLOAD_REQUEST
  };
}

export function uploadSuccess(): IInteriorPhotosAction {
  return {
    type: UPLOAD_SUCCESS
  };
}

export function uploadFailure(message: any): IInteriorPhotosAction {
  return {
    type: UPLOAD_FAILURE,
    payload: {
      message
    }
  };
}

export function uploadInteriorPhotosItems(files: File[]) {
  return async (dispatch) => {
    dispatch(uploadRequest());
    try {
      await request({
        url: '/Api/InteriorPhotos/Upload',
        files
      });
      dispatch(uploadSuccess());
      dispatch(getLog());
      dispatch(openLog());
      dispatch(getInteriorPhotos());
    } catch (error) {
      dispatch(uploadFailure(error));
    }
  };
}

export function checkRequest(): IInteriorPhotosAction {
  return {
    type: CHECK_REQUEST
  };
}

export function checkSuccess(): IInteriorPhotosAction {
  return {
    type: CHECK_SUCCESS
  };
}

export function checkFailure(message: any): IInteriorPhotosAction {
  return {
    type: CHECK_FAILURE,
    payload: {
      message
    }
  };
}

export function checkInteriorPhotosItems() {
  return async (dispatch) => {
    dispatch(checkRequest());
    try {
      await request({
        url: '/Api/InteriorPhotos/Check'
      });
      dispatch(checkSuccess());
      dispatch(getLog());
      dispatch(openLog());
    } catch (error) {
      dispatch(checkFailure(error));
    }
  };
}

export function setInteriorPhotoItem(item: IInteriorPhotoItem): IInteriorPhotosAction {
  return {
    type: SET_ITEM,
    payload: {
      item: {
        id: item.id ? Number(item.id) : undefined,
        code: item.code ? item.code : '',
        client: item.client ? item.client : '',
        mark: item.mark ? item.mark : '',
        model: item.model ? item.model : '',
        color: item.color ? item.color : '',
        body: item.body ? item.body : '',
        photo: item.photo ? item.photo : '',
        explanation: item.explanation ? item.explanation : ''
      }
    }
  };
}

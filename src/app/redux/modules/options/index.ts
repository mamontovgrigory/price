import { IOptions, IOptionItem, IOptionsAction } from '@models/options';
import { getLog, openLog } from '@data/log';
import { request } from '@utils/request';

export const GET_REQUEST: string = 'options/GET_REQUEST';
export const GET_SUCCESS: string = 'options/GET_SUCCESS';
export const GET_FAILURE: string = 'options/GET_FAILURE';
export const SAVE_REQUEST: string = 'options/SAVE_REQUEST';
export const SAVE_SUCCESS: string = 'options/SAVE_SUCCESS';
export const SAVE_FAILURE: string = 'options/SAVE_FAILURE';
export const DELETE_REQUEST: string = 'options/DELETE_REQUEST';
export const DELETE_SUCCESS: string = 'options/DELETE_SUCCESS';
export const DELETE_FAILURE: string = 'options/DELETE_FAILURE';
export const UPLOAD_REQUEST: string = 'options/UPLOAD_REQUEST';
export const UPLOAD_SUCCESS: string = 'options/UPLOAD_SUCCESS';
export const UPLOAD_FAILURE: string = 'options/UPLOAD_FAILURE';
export const CHECK_REQUEST: string = 'options/CHECK_REQUEST';
export const CHECK_SUCCESS: string = 'options/CHECK_SUCCESS';
export const CHECK_FAILURE: string = 'options/CHECK_FAILURE';
export const SET_ITEM: string = 'options/SET_ITEM';

const initialState: IOptions = {
  isFetching: false,
  item: {},
  list: []
};

export function optionsReducer(state = initialState, action: IOptionsAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SAVE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case SAVE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case SAVE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case DELETE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case DELETE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case DELETE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case UPLOAD_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case UPLOAD_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case CHECK_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case CHECK_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case CHECK_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case SET_ITEM:
      return Object.assign({}, state, {
        item: action.payload.item
      });

    default:
      return state;
  }
}

export function getRequest(): IOptionsAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): IOptionsAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): IOptionsAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getOptions() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response: any = await request({
        url: '/Api/Options/GetList'
      });
      const list = [].concat(response).map((item) => { // TODO: Move mapping to component
        return {
          ...item,
          doNotUse: item.disabled ? 'Да' : 'Нет'
        };
      });
      dispatch(getSuccess(list));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function saveRequest(): IOptionsAction {
  return {
    type: SAVE_REQUEST
  };
}

export function saveSuccess(): IOptionsAction {
  return {
    type: SAVE_SUCCESS
  };
}

export function saveFailure(message: any): IOptionsAction {
  return {
    type: SAVE_FAILURE,
    payload: {
      message
    }
  };
}

export function saveOptionItem(data: {item: IOptionItem}) {
  return async (dispatch) => {
    dispatch(saveRequest());
    try {
      await request({
        url: '/Api/Options/Save',
        data
      });
      dispatch(saveSuccess());
      dispatch(getOptions());
    } catch (error) {
      dispatch(saveFailure(error));
    }
  };
}

export function deleteRequest(): IOptionsAction {
  return {
    type: DELETE_REQUEST
  };
}

export function deleteSuccess(): IOptionsAction {
  return {
    type: DELETE_SUCCESS
  };
}

export function deleteFailure(message: any): IOptionsAction {
  return {
    type: DELETE_FAILURE,
    payload: {
      message
    }
  };
}

export function deleteOptionItems(data: {ids: number[]}) {
  return async (dispatch) => {
    dispatch(deleteRequest());
    try {
      await request({
        url: '/Api/Options/Delete',
        data
      });
      dispatch(deleteSuccess());
      dispatch(getOptions());
    } catch (error) {
      dispatch(deleteFailure(error));
    }
  };
}

export function uploadRequest(): IOptionsAction {
  return {
    type: UPLOAD_REQUEST
  };
}

export function uploadSuccess(): IOptionsAction {
  return {
    type: UPLOAD_SUCCESS
  };
}

export function uploadFailure(message: any): IOptionsAction {
  return {
    type: UPLOAD_FAILURE,
    payload: {
      message
    }
  };
}

export function uploadOptionsItems(files: File[]) {
  return async (dispatch) => {
    dispatch(uploadRequest());
    try {
      await request({
        url: '/Api/Options/Upload',
        files
      });
      dispatch(uploadSuccess());
      dispatch(getLog());
      dispatch(openLog());
      dispatch(getOptions());
    } catch (error) {
      dispatch(uploadFailure(error));
    }
  };
}

export function checkRequest(): IOptionsAction {
  return {
    type: CHECK_REQUEST
  };
}

export function checkSuccess(): IOptionsAction {
  return {
    type: CHECK_SUCCESS
  };
}

export function checkFailure(message: any): IOptionsAction {
  return {
    type: CHECK_FAILURE,
    payload: {
      message
    }
  };
}

export function checkOptionsItems() {
  return async (dispatch) => {
    dispatch(checkRequest());
    try {
      await request({
        url: '/Api/Options/Check'
      });
      dispatch(checkSuccess());
      dispatch(getLog());
      dispatch(openLog());
    } catch (error) {
      dispatch(checkFailure(error));
    }
  };
}

export function setOptionItem(item: IOptionItem): IOptionsAction {
  return {
    type: SET_ITEM,
    payload: {
      item: {
        id: item.id ? Number(item.id) : undefined,
        code: item.code ? item.code : '',
        mark: item.mark ? item.mark : '',
        model: item.model ? item.model : '',
        explanation: item.explanation ? item.explanation : '',
        disabled: item.disabled ? item.disabled : false
      }
    }
  };
}

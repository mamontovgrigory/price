import { browserHistory } from 'react-router';

import { IUsers, IUserItem, IAccount, IAccountPermissions, IUsersAction } from '@models/users';
import { request } from '@utils/request';

export const LOG_IN_REQUEST: string = 'users/LOG_IN_REQUEST';
export const LOG_IN_SUCCESS: string = 'users/LOG_IN_SUCCESS';
export const LOG_IN_FAILURE: string = 'users/LOG_IN_FAILURE';
export const SESSION_REQUEST: string = 'users/SESSION_REQUEST';
export const SESSION_SUCCESS: string = 'users/SESSION_SUCCESS';
export const SESSION_FAILURE: string = 'users/SESSION_FAILURE';
export const PERMISSIONS_REQUEST: string = 'users/PERMISSIONS_REQUEST';
export const PERMISSIONS_SUCCESS: string = 'users/PERMISSIONS_SUCCESS';
export const PERMISSIONS_FAILURE: string = 'users/PERMISSIONS_FAILURE';
export const LOGOUT_REQUEST: string = 'users/LOGOUT_REQUEST';
export const LOGOUT_SUCCESS: string = 'users/LOGOUT_SUCCESS';
export const LOGOUT_FAILURE: string = 'users/LOGOUT_FAILURE';
export const GET_REQUEST: string = 'users/GET_REQUEST';
export const GET_SUCCESS: string = 'users/GET_SUCCESS';
export const GET_FAILURE: string = 'users/GET_FAILURE';
export const SAVE_REQUEST: string = 'users/SAVE_REQUEST';
export const SAVE_SUCCESS: string = 'users/SAVE_SUCCESS';
export const SAVE_FAILURE: string = 'users/SAVE_FAILURE';
export const DELETE_REQUEST: string = 'users/DELETE_REQUEST';
export const DELETE_SUCCESS: string = 'users/DELETE_SUCCESS';
export const DELETE_FAILURE: string = 'users/DELETE_FAILURE';
export const SET_ITEM: string = 'users/SET_ITEM';

const initialState: IUsers = {
  isFetching: false,
  item: {},
  list: []
};

export function usersReducer(state = initialState, action: IUsersAction) {
  switch (action.type) {
    case LOG_IN_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case LOG_IN_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        account: action.payload.account
      });

    case LOG_IN_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SESSION_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case SESSION_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        account: action.payload.account
      });

    case SESSION_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case PERMISSIONS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case PERMISSIONS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        account: action.payload.account
      });

    case PERMISSIONS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case LOGOUT_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case LOGOUT_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        account: null
      });

    case LOGOUT_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SET_ITEM:
      return Object.assign({}, state, {
        item: action.payload.item
      });

    default:
      return state;
  }
}

export function logInRequest(): IUsersAction {
  return {
    type: LOG_IN_REQUEST
  };
}

export function logInSuccess(account): IUsersAction {
  return {
    type: LOG_IN_SUCCESS,
    payload: {
      account
    }
  };
}

export function logInFailure(message: any): IUsersAction {
  return {
    type: LOG_IN_FAILURE,
    payload: {
      message
    }
  };
}

export function logIn(data: { login: string, password: string }) {
  return async (dispatch) => {
    dispatch(logInRequest());
    try {
      const response: any = await request({
        url: '/Api/Users/Login',
        data
      });
      dispatch(logInSuccess(response));
      dispatch(sessionSuccess(response));
      const result = Boolean(response);
      if (result) {
        browserHistory.push('/');
        dispatch(getAccountPermissions(response));
      }
      return result;
    } catch (error) {
      dispatch(logInFailure(error));
    }
  };
}

export function sessionRequest(): IUsersAction {
  return {
    type: SESSION_REQUEST
  };
}

export function sessionSuccess(account): IUsersAction {
  return {
    type: SESSION_SUCCESS,
    payload: {
      account
    }
  };
}

export function sessionFailure(message: any): IUsersAction {
  return {
    type: SESSION_FAILURE,
    payload: {
      message
    }
  };
}

export function checkSession() { // TODO: Save session by passport
  return async (dispatch) => {
    dispatch(sessionRequest());
    try {
      const response: any = await request({
        url: '/Api/Users/CheckSession'
      });
      dispatch(sessionSuccess(response));
      const result = Boolean(response);
      if (result) {
        browserHistory.push('/');
        dispatch(getAccountPermissions(response));
      }
      return result;
    } catch (error) {
      dispatch(sessionFailure(error));
    }
  };
}

export function permissionsRequest(): IUsersAction {
  return {
    type: PERMISSIONS_REQUEST
  };
}

export function permissionsSuccess(account): IUsersAction {
  return {
    type: PERMISSIONS_SUCCESS,
    payload: {
      account
    }
  };
}

export function permissionsFailure(message: any): IUsersAction {
  return {
    type: PERMISSIONS_FAILURE,
    payload: {
      message
    }
  };
}

const defaultAccountPermissions: IAccountPermissions = {
  usersManage: false,
  groupsManage: false
};

export function getAccountPermissions(account: IAccount) {
  return async (dispatch) => {
    dispatch(permissionsRequest());
    try {
      const response: any = await request({
        url: '/Api/Users/GetAccount'
      });
      const permissions = Object.assign({}, defaultAccountPermissions);
      if (response) {
        response.forEach((item) => {
          if (permissions.hasOwnProperty(item.alias)) {
            permissions[item.alias] = item.value;
          }
        });
      }
      const result = Object.assign({}, account, {
        permissions
      });
      dispatch(permissionsSuccess(result));
    } catch (error) {
      dispatch(permissionsFailure(error));
    }
  };
}

export function logoutRequest(): IUsersAction {
  return {
    type: LOGOUT_REQUEST
  };
}

export function logoutSuccess(): IUsersAction {
  return {
    type: LOGOUT_SUCCESS
  };
}

export function logoutFailure(message: any): IUsersAction {
  return {
    type: LOGOUT_FAILURE,
    payload: {
      message
    }
  };
}

export function logout() {
  return async (dispatch) => {
    dispatch(logoutRequest());
    try {
      await request({
        url: '/Api/Users/Logout'
      });
      dispatch(logoutSuccess());
    } catch (error) {
      dispatch(logoutFailure(error));
    }
  };
}

export function getUsersRequest(): IUsersAction {
  return {
    type: GET_REQUEST
  };
}

export function getUsersSuccess(list): IUsersAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getUsersFailure(message: any): IUsersAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getUsers() {
  return async (dispatch) => {
    dispatch(getUsersRequest());
    try {
      const response = await request({
        url: '/Api/Users/GetList'
      });
      dispatch(getUsersSuccess(response));
    } catch (error) {
      dispatch(getUsersFailure(error));
    }
  };
}

export function saveRequest(): IUsersAction {
  return {
    type: SAVE_REQUEST
  };
}

export function saveSuccess(): IUsersAction {
  return {
    type: SAVE_SUCCESS
  };
}

export function saveFailure(message: any): IUsersAction {
  return {
    type: SAVE_FAILURE,
    payload: {
      message
    }
  };
}

export function saveUserItem(data: {item: IUserItem}) {
  return async (dispatch) => {
    dispatch(saveRequest());
    try {
      await request({
        url: '/Api/Users/Save',
        data
      });
      dispatch(saveSuccess());
      dispatch(getUsers());
    } catch (error) {
      dispatch(saveFailure(error));
    }
  };
}

export function deleteRequest(): IUsersAction {
  return {
    type: DELETE_REQUEST
  };
}

export function deleteSuccess(): IUsersAction {
  return {
    type: DELETE_SUCCESS
  };
}

export function deleteFailure(message: any): IUsersAction {
  return {
    type: DELETE_FAILURE,
    payload: {
      message
    }
  };
}

export function deleteUserItems(data: {ids: number[]}) {
  return async (dispatch) => {
    dispatch(deleteRequest());
    try {
      await request({
        url: '/Api/Users/Delete',
        data
      });
      dispatch(deleteSuccess());
      dispatch(getUsers());
    } catch (error) {
      dispatch(deleteFailure(error));
    }
  };
}

export function setUserItem(item: IUserItem): IUsersAction {
  return {
    type: SET_ITEM,
    payload: {
      item
    }
  };
}

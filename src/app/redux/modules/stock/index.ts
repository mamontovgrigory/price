import { IStock, IStockItem, IConfiguration, IStockAction } from '@models/stock';
import { request } from '@utils/request';
import { getLog, getLogByProcessId, openLog } from '@data/log';
import { defaultConfiguration } from './constants';

export const GET_REQUEST: string = 'stock/GET_REQUEST';
export const GET_SUCCESS: string = 'stock/GET_SUCCESS';
export const GET_FAILURE: string = 'stock/GET_FAILURE';
export const SAVE_REQUEST: string = 'stock/SAVE_REQUEST';
export const SAVE_SUCCESS: string = 'stock/SAVE_SUCCESS';
export const SAVE_FAILURE: string = 'stock/SAVE_FAILURE';
export const DELETE_REQUEST: string = 'stock/DELETE_REQUEST';
export const DELETE_SUCCESS: string = 'stock/DELETE_SUCCESS';
export const DELETE_FAILURE: string = 'stock/DELETE_FAILURE';
export const UPLOAD_REQUEST: string = 'stock/UPLOAD_REQUEST';
export const UPLOAD_SUCCESS: string = 'stock/UPLOAD_SUCCESS';
export const UPLOAD_FAILURE: string = 'stock/UPLOAD_FAILURE';
export const GET_CLIENTS_REQUEST: string = 'stock/GET_CLIENTS_REQUEST';
export const GET_CLIENTS_SUCCESS: string = 'stock/GET_CLIENTS_SUCCESS';
export const GET_CLIENTS_FAILURE: string = 'stock/GET_CLIENTS_FAILURE';
export const SET_ITEM: string = 'stock/SET_ITEM';
export const FILL_REQUEST: string = 'stock/FILL_REQUEST';
export const FILL_SUCCESS: string = 'stock/FILL_SUCCESS';
export const FILL_FAILURE: string = 'stock/FILL_FAILURE';
export const GENERATE_REQUEST: string = 'stock/GENERATE_REQUEST';
export const GENERATE_SUCCESS: string = 'stock/GENERATE_SUCCESS';
export const GENERATE_FAILURE: string = 'stock/GENERATE_FAILURE';
export const GET_TEMPLATE_REQUEST: string = 'stock/GET_TEMPLATE_REQUEST';
export const GET_TEMPLATE_SUCCESS: string = 'stock/GET_TEMPLATE_SUCCESS';
export const GET_TEMPLATE_FAILURE: string = 'stock/GET_TEMPLATE_FAILURE';
export const SET_CONFIGURATION: string = 'stock/SET_CONFIGURATION';
export const CHECK_REQUEST: string = 'stock/CHECK_REQUEST';
export const CHECK_SUCCESS: string = 'stock/CHECK_SUCCESS';
export const CHECK_FAILURE: string = 'stock/CHECK_FAILURE';
export const SET_PROGRESS: string = 'stock/SET_PROGRESS';

const initialState: IStock = {
  isFetching: false,
  item: {},
  list: [],
  clientsList: [],
  configuration: defaultConfiguration
};

export function stockReducer(state = initialState, action: IStockAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SAVE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case SAVE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        lastUpdated: action.payload.lastUpdated
      });

    case SAVE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case DELETE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case DELETE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case DELETE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case UPLOAD_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case UPLOAD_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case GET_CLIENTS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_CLIENTS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        clientsList: action.payload.list
      });

    case GET_CLIENTS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case SET_ITEM:
      return Object.assign({}, state, {
        item: action.payload.item
      });

    case FILL_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case FILL_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case FILL_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case GENERATE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GENERATE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case GENERATE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case GET_TEMPLATE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_TEMPLATE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case GET_TEMPLATE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case SET_CONFIGURATION:
      return Object.assign({}, state, {
        configuration: action.payload.configuration
      });

    case SET_PROGRESS:
      return Object.assign({}, state, {
        progress: action.payload.progress
      });

    case CHECK_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case CHECK_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case CHECK_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    default:
      return state;
  }
}

export function getRequest(): IStockAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): IStockAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): IStockAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getStock() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response = await request({ // TODO: Change to GET
        url: '/Api/Stock/GetList'
      });
      dispatch(getSuccess(response));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function deleteStockItems(data: { ids: number[] }) {
  return async (dispatch) => {
    dispatch(deleteRequest());
    try {
      await request({
        url: '/Api/Stock/Delete',
        data
      });
      dispatch(deleteSuccess());
      dispatch(getStock());
    } catch (error) {
      dispatch(deleteFailure(error));
    }
  };
}

export function saveStockItem(data: { item: IStockItem }) {
  return async (dispatch) => {
    dispatch(saveRequest());
    try {
      await request({
        url: '/Api/Stock/Save',
        data
      });
      dispatch(saveSuccess(data.item.id));
      dispatch(getStock());
    } catch (error) {
      dispatch(saveFailure(error));
    }
  };
}

export function deleteRequest(): IStockAction {
  return {
    type: DELETE_REQUEST
  };
}

export function deleteSuccess(): IStockAction {
  return {
    type: DELETE_SUCCESS
  };
}

export function deleteFailure(message: any): IStockAction {
  return {
    type: DELETE_FAILURE,
    payload: {
      message
    }
  };
}

export function saveRequest(): IStockAction {
  return {
    type: SAVE_REQUEST
  };
}

export function saveSuccess(lastUpdated?: number): IStockAction {
  return {
    type: SAVE_SUCCESS,
    payload: {
      lastUpdated
    }
  };
}

export function saveFailure(message: any): IStockAction {
  return {
    type: SAVE_FAILURE,
    payload: {
      message
    }
  };
}

export function uploadRequest(): IStockAction {
  return {
    type: UPLOAD_REQUEST
  };
}

export function uploadSuccess(): IStockAction {
  return {
    type: UPLOAD_SUCCESS
  };
}

export function uploadFailure(message: any): IStockAction {
  return {
    type: UPLOAD_FAILURE,
    payload: {
      message
    }
  };
}

export function uploadStockItems(data, files: File[]) {
  return async (dispatch) => {
    dispatch(uploadRequest());
    try {
      await request({
        url: '/Api/Stock/Upload',
        data,
        files
      });
      dispatch(uploadSuccess());
      dispatch(getStock());
      dispatch(getLog());
      dispatch(openLog());
    } catch (error) {
      dispatch(uploadFailure(error));
    }
  };
}

export function getClientsRequest(): IStockAction {
  return {
    type: GET_CLIENTS_REQUEST
  };
}

export function getClientsSuccess(list): IStockAction {
  return {
    type: GET_CLIENTS_SUCCESS,
    payload: {
      list
    }
  };
}

export function getClientsFailure(message: any): IStockAction {
  return {
    type: GET_CLIENTS_FAILURE,
    payload: {
      message
    }
  };
}

export function getClients() {
  return async (dispatch) => {
    dispatch(getClientsRequest());
    try {
      const response = await request({
        url: '/Api/Stock/GetClients'
      });
      dispatch(getClientsSuccess(response));
    } catch (error) {
      dispatch(getClientsFailure(error));
    }
  };
}

export function fillPricePhotosRequest(): IStockAction {
  return {
    type: FILL_REQUEST
  };
}

export function fillPricePhotosSuccess(): IStockAction {
  return {
    type: FILL_SUCCESS
  };
}

export function fillPricePhotosFailure(message: any): IStockAction {
  return {
    type: FILL_FAILURE,
    payload: {
      message
    }
  };
}

export function fillPricePhotos(data, files: File[]) {
  return async (dispatch) => {
    dispatch(fillPricePhotosRequest());
    try {
      const response = await request({
        url: '/Api/Stock/Fill',
        data,
        files
      });
      response.blob().then((blob) => {
        const url = URL.createObjectURL(blob); // TODO: Find normal decision for downloading
        const a = document.createElement('a');
        a.href = url;
        a.download = 'price.xlsx';
        a.click();
        window.URL.revokeObjectURL(url);
      });
      dispatch(fillPricePhotosSuccess());
    } catch (error) {
      dispatch(fillPricePhotosFailure(error));
    }
  };
}

export function generateRequest(): IStockAction {
  return {
    type: GENERATE_REQUEST
  };
}

export function generateSuccess(): IStockAction {
  return {
    type: GENERATE_SUCCESS
  };
}

export function generateFailure(message: any): IStockAction {
  return {
    type: GENERATE_FAILURE,
    payload: {
      message
    }
  };
}

export function generatePrice(data, files: File[]) {
  return async (dispatch) => {
    dispatch(generateRequest());
    try {
      const response = await request({
        url: '/Api/Stock/Generate',
        data,
        files
      });
      response.blob().then((blob) => {
        const url = URL.createObjectURL(blob); // TODO: Find normal decision for downloading
        const a = document.createElement('a');
        a.href = url;
        a.download = 'price.xlsx';
        a.click();
        window.URL.revokeObjectURL(url);
      });
      dispatch(generateSuccess());
      dispatch(getLog());
      dispatch(openLog());
    } catch (error) {
      dispatch(generateFailure(error));
    }
  };
}

export function getTemplateRequest(): IStockAction {
  return {
    type: GET_TEMPLATE_REQUEST
  };
}

export function getTemplateSuccess(): IStockAction {
  return {
    type: GET_TEMPLATE_SUCCESS
  };
}

export function getTemplateFailure(message: any): IStockAction {
  return {
    type: GET_TEMPLATE_FAILURE,
    payload: {
      message
    }
  };
}

export function getTemplate() {
  return async (dispatch) => {
    dispatch(getTemplateRequest());
    try {
      const response = await request({
        url: '/Api/Stock/GetTemplate'
      });
      response.blob().then((blob) => {
        const url = URL.createObjectURL(blob); // TODO: Find normal decision for downloading
        const a = document.createElement('a');
        a.href = url;
        a.download = 'template.xlsx';
        a.click();
        window.URL.revokeObjectURL(url);
      });
      dispatch(getTemplateSuccess());
    } catch (error) {
      dispatch(getTemplateFailure(error));
    }
  };
}

export function setProgress(progress: number): IStockAction {
  return {
    type: SET_PROGRESS,
    payload: {
      progress
    }
  };
}

export function initProcess(data, files: File[]) {
  return async (dispatch) => {
    try {
      let progress = 0;
      dispatch(setProgress(progress));
      const response: any = await request({
        url: '/Api/Stock/StartProcess',
        data,
        files
      });
      response.json().then(async (data) => {
        const {processId} = data;
        while (progress !== 100) {
          const response: any = await request({
            url: '/Api/Stock/GetProcessStep',
            data: {processId}
          });
          progress = response.progress;
          dispatch(setProgress(progress));
        }
        const response = await request({
          url: '/Api/Stock/GetProcessResult',
          data: {processId},
          contentType: false
        });
        dispatch(setProgress(null));
        response.blob().then((blob) => {
          const url = URL.createObjectURL(blob); // TODO: Find normal decision for downloading
          const a = document.createElement('a');
          a.href = url;
          a.download = 'template.xlsx';
          a.click();
          window.URL.revokeObjectURL(url);
        });
        dispatch(getLogByProcessId(processId));
        dispatch(openLog());
      });
    } catch (error) {
      console.log('error', error);
    }
  };
}

export function checkRequest(): IStockAction {
  return {
    type: CHECK_REQUEST
  };
}

export function checkSuccess(): IStockAction {
  return {
    type: CHECK_SUCCESS
  };
}

export function checkFailure(message: any): IStockAction {
  return {
    type: CHECK_FAILURE,
    payload: {
      message
    }
  };
}

export function checkStockItems() {
  return async (dispatch) => {
    dispatch(checkRequest());
    try {
      await request({
        url: '/Api/Stock/Check'
      });
      dispatch(checkSuccess());
      dispatch(getLog());
      dispatch(openLog());
    } catch (error) {
      dispatch(checkFailure(error));
    }
  };
}

export function setStockItem(item: IStockItem): IStockAction {
  return {
    type: SET_ITEM,
    payload: {
      item: {
        id: item.id ? Number(item.id) : undefined,
        client: item.client ? item.client : '',
        front: item.front ? item.front : '',
        back: item.back ? item.back : '',
        mark: item.mark ? item.mark : '',
        model: item.model ? item.model : '',
        complectation: item.complectation ? item.complectation : '',
        description: item.description ? item.description : '',
        options: item.options ? item.options : '',
        color: item.color ? item.color : '',
        body: item.body ? item.body : '',
        condition: item.condition ? item.condition : '',
        place: item.place ? item.place : '',
        date: item.date ? item.date : '',
        vin: item.vin ? item.vin : '',
        videos: item.videos ? item.videos : '',
        panoramas: item.panoramas ? item.panoramas : '',
        interiorsByVin: item.interiorsByVin ? item.interiorsByVin : ''
      }
    }
  };
}

export function setConfiguration(configuration: IConfiguration[]): IStockAction {
  return {
    type: SET_ITEM,
    payload: {
      configuration
    }
  };
}

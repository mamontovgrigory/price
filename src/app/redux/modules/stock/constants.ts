export const defaultConfiguration = [
  {
    file: [
      {
        column: 'mark'
      }
    ],
    base: [
      {
        column: 'mark'
      }
    ]
  },
  {
    file: [
      {
        column: 'model'
      }
    ],
    base: [
      {
        column: 'model'
      }
    ]
  },
  {
    file: [
      {
        column: 'complectation'
      }
    ],
    base: [
      {
        column: 'complectation'
      }
    ],
    intersection: true,
    ifExists: true
  },
  {
    file: [
      {
        column: 'body'
      }
    ],
    base: [
      {
        column: 'body'
      }
    ]
  },
  {
    file: [
      {
        column: 'color'
      }
    ],
    base: [
      {
        column: 'color'
      }
    ]
  }/*,
  {
    file: [
      {
        column: 'model',
        like: ['amg'],
        invert: true
      },
      {
        column: 'complectation',
        like: ['amg'],
        invert: true
      },
      {
        column: ['options', 'description'],
        like: [
          '250',
          '750',
          '772',
          '773',
          '950',
          '956',
          'B26',
          'B25',
          'DC7',
          'P31',
          'P30',
          'P39',
          'P60',
          'P90',
          'PEL'
        ]
      }
    ],
    base: [
      {
        column: 'model',
        like: ['amg'],
        invert: true
      },
      {
        column: 'complectation',
        like: ['amg'],
        invert: true
      },
      {
        column: 'description',
        like: ['amg']
      }
    ]
  },
  {
    file: [
      {
        column: 'options',
        like: ['8EX', '8Q0', '8WM', 'PX2', 'PXF', 'WBQPX2', '8IT']
      }
    ],
    base: [
      {
        column: 'description',
        like: ['Led']
      }
    ]
  },
  {
    file: [
      {
        column: 'options',
        like: ['PXC']
      }
    ],
    base: [
      {
        column: 'description',
        like: ['Matrix']
      }
    ]
  },
  {
    file: [
      {
        column: 'options',
        like: ['WD6', 'PQD', 'WRUPQD', 'WQS', 'WQV', 'WRUWQV']
      }
    ],
    base: [
      {
        column: 'description',
        like: ['S Line']
      }
    ]
  }*/
];

export const columnsMapping = {
  mark: 'Марка',
  model: 'Модель',
  complectation: 'Комплектация',
  color: 'Цвет',
  body: 'Кузов',
  place: 'Место',
  condition: 'Состояние',
  date: 'Дата съемки',
  description: 'Описание',
  options: 'Коды опций комплектации'
};

export const fileColumnsMapping = {
  ...columnsMapping,
  modificationCode: 'Код модификации'
};

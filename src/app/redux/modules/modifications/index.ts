import { IModifications, IModificationItem, IModificationsAction } from '@models/modifications';
import { getLog, openLog } from '@data/log';
import { request } from '@utils/request';

export const GET_REQUEST: string = 'modifications/GET_REQUEST';
export const GET_SUCCESS: string = 'modifications/GET_SUCCESS';
export const GET_FAILURE: string = 'modifications/GET_FAILURE';
export const SAVE_REQUEST: string = 'modifications/SAVE_REQUEST';
export const SAVE_SUCCESS: string = 'modifications/SAVE_SUCCESS';
export const SAVE_FAILURE: string = 'modifications/SAVE_FAILURE';
export const DELETE_REQUEST: string = 'modifications/DELETE_REQUEST';
export const DELETE_SUCCESS: string = 'modifications/DELETE_SUCCESS';
export const DELETE_FAILURE: string = 'modifications/DELETE_FAILURE';
export const UPLOAD_REQUEST: string = 'modifications/UPLOAD_REQUEST';
export const UPLOAD_SUCCESS: string = 'modifications/UPLOAD_SUCCESS';
export const UPLOAD_FAILURE: string = 'modifications/UPLOAD_FAILURE';
export const CHECK_REQUEST: string = 'modifications/CHECK_REQUEST';
export const CHECK_SUCCESS: string = 'modifications/CHECK_SUCCESS';
export const CHECK_FAILURE: string = 'modifications/CHECK_FAILURE';
export const SET_ITEM: string = 'modifications/SET_ITEM';

const initialState: IModifications = {
  isFetching: false,
  item: {},
  list: []
};

export function modificationsReducer(state = initialState, action: IModificationsAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SAVE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case SAVE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case SAVE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case DELETE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case DELETE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case DELETE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case UPLOAD_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case UPLOAD_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case SET_ITEM:
      return Object.assign({}, state, {
        item: action.payload.item
      });

    case CHECK_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case CHECK_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case CHECK_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    default:
      return state;
  }
}

export function getRequest(): IModificationsAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): IModificationsAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): IModificationsAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getModifications() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response: any = await request({
        url: '/Api/Modifications/GetList'
      });
      dispatch(getSuccess(response));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function saveRequest(): IModificationsAction {
  return {
    type: SAVE_REQUEST
  };
}

export function saveSuccess(): IModificationsAction {
  return {
    type: SAVE_SUCCESS
  };
}

export function saveFailure(message: any): IModificationsAction {
  return {
    type: SAVE_FAILURE,
    payload: {
      message
    }
  };
}

export function saveModificationItem(data: {item: IModificationItem}) {
  return async (dispatch) => {
    dispatch(saveRequest());
    try {
      await request({
        url: '/Api/Modifications/Save',
        data
      });
      dispatch(saveSuccess());
      dispatch(getModifications());
    } catch (error) {
      dispatch(saveFailure(error));
    }
  };
}

export function deleteRequest(): IModificationsAction {
  return {
    type: DELETE_REQUEST
  };
}

export function deleteSuccess(): IModificationsAction {
  return {
    type: DELETE_SUCCESS
  };
}

export function deleteFailure(message: any): IModificationsAction {
  return {
    type: DELETE_FAILURE,
    payload: {
      message
    }
  };
}

export function deleteModificationItems(data: {ids: number[]}) {
  return async (dispatch) => {
    dispatch(deleteRequest());
    try {
      await request({
        url: '/Api/Modifications/Delete',
        data
      });
      dispatch(deleteSuccess());
      dispatch(getModifications());
    } catch (error) {
      dispatch(deleteFailure(error));
    }
  };
}

export function uploadRequest(): IModificationsAction {
  return {
    type: UPLOAD_REQUEST
  };
}

export function uploadSuccess(): IModificationsAction {
  return {
    type: UPLOAD_SUCCESS
  };
}

export function uploadFailure(message: any): IModificationsAction {
  return {
    type: UPLOAD_FAILURE,
    payload: {
      message
    }
  };
}

export function uploadModificationItems(files: File[]) {
  return async (dispatch) => {
    dispatch(uploadRequest());
    try {
      await request({
        url: '/Api/Modifications/Upload',
        files
      });
      dispatch(uploadSuccess());
      dispatch(getLog());
      dispatch(openLog());
      dispatch(getModifications());
    } catch (error) {
      dispatch(uploadFailure(error));
    }
  };
}

export function checkRequest(): IModificationsAction {
  return {
    type: CHECK_REQUEST
  };
}

export function checkSuccess(): IModificationsAction {
  return {
    type: CHECK_SUCCESS
  };
}

export function checkFailure(message: any): IModificationsAction {
  return {
    type: CHECK_FAILURE,
    payload: {
      message
    }
  };
}

export function checkModificationsItems() {
  return async (dispatch) => {
    dispatch(checkRequest());
    try {
      await request({
        url: '/Api/Modifications/Check'
      });
      dispatch(checkSuccess());
      dispatch(getLog());
      dispatch(openLog());
    } catch (error) {
      dispatch(checkFailure(error));
    }
  };
}

export function setModificationItem(item: IModificationItem): IModificationsAction {
  return {
    type: SET_ITEM,
    payload: {
      item: {
        id: item.id ? Number(item.id) : undefined,
        code: item.code ? item.code : '',
        complectationCode: item.complectationCode ? item.complectationCode : '',
        complectationBase: item.complectationBase ? item.complectationBase : '',
        complectationAutoRu: item.complectationAutoRu ? item.complectationAutoRu : '',
        mark: item.mark ? item.mark : '',
        model: item.model ? item.model : '',
        complectation: item.complectation ? item.complectation : '',
        body: item.body ? item.body : '',
        engineCapacity: item.engineCapacity ? item.engineCapacity : '',
        power: item.power ? item.power : '',
        transmission: item.transmission ? item.transmission : '',
        engineType: item.engineType ? item.engineType : '',
        driveUnit: item.driveUnit ? item.driveUnit : '',
        loadCapacity: item.loadCapacity ? item.loadCapacity : ''
      }
    }
  };
}

import { IColors, IColorItem, IColorsAction } from '@models/colors';
import { getLog, openLog } from '@data/log';
import { request } from '@utils/request';

export const GET_REQUEST: string = 'colors/GET_REQUEST';
export const GET_SUCCESS: string = 'colors/GET_SUCCESS';
export const GET_FAILURE: string = 'colors/GET_FAILURE';
export const SAVE_REQUEST: string = 'colors/SAVE_REQUEST';
export const SAVE_SUCCESS: string = 'colors/SAVE_SUCCESS';
export const SAVE_FAILURE: string = 'colors/SAVE_FAILURE';
export const DELETE_REQUEST: string = 'colors/DELETE_REQUEST';
export const DELETE_SUCCESS: string = 'colors/DELETE_SUCCESS';
export const DELETE_FAILURE: string = 'colors/DELETE_FAILURE';
export const UPLOAD_REQUEST: string = 'colors/UPLOAD_REQUEST';
export const UPLOAD_SUCCESS: string = 'colors/UPLOAD_SUCCESS';
export const UPLOAD_FAILURE: string = 'colors/UPLOAD_FAILURE';
export const CHECK_REQUEST: string = 'colors/CHECK_REQUEST';
export const CHECK_SUCCESS: string = 'colors/CHECK_SUCCESS';
export const CHECK_FAILURE: string = 'colors/CHECK_FAILURE';
export const SET_ITEM: string = 'colors/SET_ITEM';

const initialState: IColors = {
  isFetching: false,
  item: {},
  list: []
};

export function colorsReducer(state = initialState, action: IColorsAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SAVE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case SAVE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case SAVE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case DELETE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case DELETE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case DELETE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case UPLOAD_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case UPLOAD_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case CHECK_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case CHECK_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case CHECK_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case SET_ITEM:
      return Object.assign({}, state, {
        item: action.payload.item
      });

    default:
      return state;
  }
}

export function getRequest(): IColorsAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): IColorsAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): IColorsAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getColors() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response: any = await request({
        url: '/Api/Colors/GetList'
      });
      dispatch(getSuccess(response));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function saveRequest(): IColorsAction {
  return {
    type: SAVE_REQUEST
  };
}

export function saveSuccess(): IColorsAction {
  return {
    type: SAVE_SUCCESS
  };
}

export function saveFailure(message: any): IColorsAction {
  return {
    type: SAVE_FAILURE,
    payload: {
      message
    }
  };
}

export function saveColorItem(data: {item: IColorItem}) {
  return async (dispatch) => {
    dispatch(saveRequest());
    try {
      await request({
        url: '/Api/Colors/Save',
        data
      });
      dispatch(saveSuccess());
      dispatch(getColors());
    } catch (error) {
      dispatch(saveFailure(error));
    }
  };
}

export function deleteRequest(): IColorsAction {
  return {
    type: DELETE_REQUEST
  };
}

export function deleteSuccess(): IColorsAction {
  return {
    type: DELETE_SUCCESS
  };
}

export function deleteFailure(message: any): IColorsAction {
  return {
    type: DELETE_FAILURE,
    payload: {
      message
    }
  };
}

export function deleteColorItems(data: {ids: number[]}) {
  return async (dispatch) => {
    dispatch(deleteRequest());
    try {
      await request({
        url: '/Api/Colors/Delete',
        data
      });
      dispatch(deleteSuccess());
      dispatch(getColors());
    } catch (error) {
      dispatch(deleteFailure(error));
    }
  };
}

export function uploadRequest(): IColorsAction {
  return {
    type: UPLOAD_REQUEST
  };
}

export function uploadSuccess(): IColorsAction {
  return {
    type: UPLOAD_SUCCESS
  };
}

export function uploadFailure(message: any): IColorsAction {
  return {
    type: UPLOAD_FAILURE,
    payload: {
      message
    }
  };
}

export function uploadColorsItems(files: File[]) {
  return async (dispatch) => {
    dispatch(uploadRequest());
    try {
      await request({
        url: '/Api/Colors/Upload',
        files
      });
      dispatch(uploadSuccess());
      dispatch(getLog());
      dispatch(openLog());
      dispatch(getColors());
    } catch (error) {
      dispatch(uploadFailure(error));
    }
  };
}

export function checkRequest(): IColorsAction {
  return {
    type: CHECK_REQUEST
  };
}

export function checkSuccess(): IColorsAction {
  return {
    type: CHECK_SUCCESS
  };
}

export function checkFailure(message: any): IColorsAction {
  return {
    type: CHECK_FAILURE,
    payload: {
      message
    }
  };
}

export function checkColorsItems() {
  return async (dispatch) => {
    dispatch(checkRequest());
    try {
      await request({
        url: '/Api/Colors/Check'
      });
      dispatch(checkSuccess());
      dispatch(getLog());
      dispatch(openLog());
    } catch (error) {
      dispatch(checkFailure(error));
    }
  };
}

export function setColorItem(item: IColorItem): IColorsAction {
  return {
    type: SET_ITEM,
    payload: {
      item: {
        id: item.id ? Number(item.id) : undefined,
        code: item.code ? item.code : '',
        mark: item.mark ? item.mark : '',
        name: item.name ? item.name : '',
        fullName: item.fullName ? item.fullName : ''
      }
    }
  };
}

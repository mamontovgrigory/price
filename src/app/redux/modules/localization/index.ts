import {
  ILocalization,
  IResources,
  ISetCurrentLanguageAction,
  ISetLanguagesAction,
  ISetResources
} from '@models/localization';

export const SET_CURRENT_LANGUAGE = 'localization/SET_CURRENT_LANGUAGE';
export const SET_LANGUAGES = 'localization/SET_LANGUAGES';
export const SET_RESOURCES = 'localization/SET_RESOURCES';

const initialState: ILocalization = {
  languages: [],
  resources: {}
};

export function localizationReducer(state = initialState, action: Redux.Action) {
  switch (action.type) {
    case SET_CURRENT_LANGUAGE: {
      const {languageCode} = action as ISetCurrentLanguageAction;
      return Object.assign({}, state, {
        currentLanguage: languageCode
      });
    }

    case SET_LANGUAGES : {
      const {languages} = action as ISetLanguagesAction;
      return Object.assign({}, state, {languages});
    }

    case SET_RESOURCES: {
      const {resources} = action as ISetResources;
      return Object.assign({}, state, {resources});
    }

    default:
      return state;
  }
}

export function setCurrentLanguage(languageCode): ISetCurrentLanguageAction {
  return {
    type: SET_CURRENT_LANGUAGE,
    languageCode
  };
}

export function setLanguages(languages): ISetLanguagesAction {
  return {
    type: SET_LANGUAGES,
    languages
  };
}

export function setResources(resources: IResources): ISetResources {
  return {
    type: SET_RESOURCES,
    resources
  };
}

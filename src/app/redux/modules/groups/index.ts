import { IGroups, IGroupItem, IGroupsAction } from '@models/groups';
import { request } from '@utils/request';

export const GET_REQUEST: string = 'groups/GET_REQUEST';
export const GET_SUCCESS: string = 'groups/GET_SUCCESS';
export const GET_FAILURE: string = 'groups/GET_FAILURE';
export const SAVE_REQUEST: string = 'groups/SAVE_REQUEST';
export const SAVE_SUCCESS: string = 'groups/SAVE_SUCCESS';
export const SAVE_FAILURE: string = 'groups/SAVE_FAILURE';
export const DELETE_REQUEST: string = 'groups/DELETE_REQUEST';
export const DELETE_SUCCESS: string = 'groups/DELETE_SUCCESS';
export const DELETE_FAILURE: string = 'groups/DELETE_FAILURE';
export const SET_ITEM: string = 'groups/SET_ITEM';

const initialState: IGroups = {
  isFetching: false,
  item: {},
  list: []
};

export function groupsReducer(state = initialState, action: IGroupsAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SAVE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case SAVE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case SAVE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case DELETE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case DELETE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case DELETE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case SET_ITEM:
      return Object.assign({}, state, {
        item: action.payload.item
      });

    default:
      return state;
  }
}

export function getRequest(): IGroupsAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): IGroupsAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): IGroupsAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getGroups() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response: any = await request({
        url: '/Api/Groups/GetList'
      });
      dispatch(getSuccess(response));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function saveRequest(): IGroupsAction {
  return {
    type: SAVE_REQUEST
  };
}

export function saveSuccess(): IGroupsAction {
  return {
    type: SAVE_SUCCESS
  };
}

export function saveFailure(message: any): IGroupsAction {
  return {
    type: SAVE_FAILURE,
    payload: {
      message
    }
  };
}

export function saveGroupItem(data: {item: IGroupItem}) {
  return async (dispatch) => {
    dispatch(saveRequest());
    try {
      await request({
        url: '/Api/Groups/Save',
        data
      });
      dispatch(saveSuccess());
      dispatch(getGroups());
    } catch (error) {
      dispatch(saveFailure(error));
    }
  };
}

export function deleteRequest(): IGroupsAction {
  return {
    type: DELETE_REQUEST
  };
}

export function deleteSuccess(): IGroupsAction {
  return {
    type: DELETE_SUCCESS
  };
}

export function deleteFailure(message: any): IGroupsAction {
  return {
    type: DELETE_FAILURE,
    payload: {
      message
    }
  };
}

export function deleteGroupItems(data: {ids: number[]}) {
  return async (dispatch) => {
    dispatch(deleteRequest());
    try {
      await request({
        url: '/Api/Groups/Delete',
        data
      });
      dispatch(deleteSuccess());
      dispatch(getGroups());
    } catch (error) {
      dispatch(deleteFailure(error));
    }
  };
}

export function setGroupItem(item: IGroupItem): IGroupsAction {
  return {
    type: SET_ITEM,
    payload: {
      item: {
        id: item.id ? Number(item.id) : undefined,
        name: item.name ? item.name : '',
        settings: item.settings ? item.settings : []
      }
    }
  };
}

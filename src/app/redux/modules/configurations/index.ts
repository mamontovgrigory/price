import { IConfigurations, IConfigurationItem, IConfigurationsAction } from '@models/configurations';
import { request } from '@utils/request';
import { defaultConfiguration } from '@data/stock/constants';

export const GET_REQUEST: string = 'configurations/GET_REQUEST';
export const GET_SUCCESS: string = 'configurations/GET_SUCCESS';
export const GET_FAILURE: string = 'configurations/GET_FAILURE';
export const SAVE_REQUEST: string = 'configurations/SAVE_REQUEST';
export const SAVE_SUCCESS: string = 'configurations/SAVE_SUCCESS';
export const SAVE_FAILURE: string = 'configurations/SAVE_FAILURE';
export const DELETE_REQUEST: string = 'configurations/DELETE_REQUEST';
export const DELETE_SUCCESS: string = 'configurations/DELETE_SUCCESS';
export const DELETE_FAILURE: string = 'configurations/DELETE_FAILURE';
export const SET_ITEM: string = 'configurations/SET_ITEM';

const initialState: IConfigurations = {
  isFetching: false,
  item: {},
  list: []
};

export function configurationsReducer(state = initialState, action: IConfigurationsAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SAVE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case SAVE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case SAVE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case DELETE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case DELETE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case DELETE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case SET_ITEM:
      return Object.assign({}, state, {
        item: action.payload.item
      });

    default:
      return state;
  }
}

export function getRequest(): IConfigurationsAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): IConfigurationsAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): IConfigurationsAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getConfigurations() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response: any = await request({
        url: '/Api/Configurations/GetList'
      });
      dispatch(getSuccess(response));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function saveRequest(): IConfigurationsAction {
  return {
    type: SAVE_REQUEST
  };
}

export function saveSuccess(): IConfigurationsAction {
  return {
    type: SAVE_SUCCESS
  };
}

export function saveFailure(message: any): IConfigurationsAction {
  return {
    type: SAVE_FAILURE,
    payload: {
      message
    }
  };
}

export function saveConfigurationItem(data: { item: IConfigurationItem }) {
  return async (dispatch) => {
    dispatch(saveRequest());
    try {
      await request({
        url: '/Api/Configurations/Save',
        data
      });
      dispatch(saveSuccess());
      dispatch(getConfigurations());
    } catch (error) {
      dispatch(saveFailure(error));
    }
  };
}

export function deleteRequest(): IConfigurationsAction {
  return {
    type: DELETE_REQUEST
  };
}

export function deleteSuccess(): IConfigurationsAction {
  return {
    type: DELETE_SUCCESS
  };
}

export function deleteFailure(message: any): IConfigurationsAction {
  return {
    type: DELETE_FAILURE,
    payload: {
      message
    }
  };
}

export function deleteConfigurationItems(data: { ids: number[] }) {
  return async (dispatch) => {
    dispatch(deleteRequest());
    try {
      await request({
        url: '/Api/Configurations/Delete',
        data
      });
      dispatch(deleteSuccess());
      dispatch(getConfigurations());
    } catch (error) {
      dispatch(deleteFailure(error));
    }
  };
}

export function setConfigurationItem(item: IConfigurationItem): IConfigurationsAction {
  return {
    type: SET_ITEM,
    payload: {
      item: {
        id: item.id ? Number(item.id) : undefined,
        name: item.name ? item.name : '',
        configuration: item.configuration ? item.configuration : JSON.stringify(defaultConfiguration)
      }
    }
  };
}

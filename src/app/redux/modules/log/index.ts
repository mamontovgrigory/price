import { ILog, ILogAction } from '@models/log';
import { request } from '@utils/request';

export const GET_REQUEST: string = 'log/GET_REQUEST';
export const GET_SUCCESS: string = 'log/GET_SUCCESS';
export const GET_FAILURE: string = 'log/GET_FAILURE';
export const OPEN_LOG: string = 'log/OPEN_LOG';
export const CLOSE_LOG: string = 'log/CLOSE_LOG';

const initialState: ILog = {
  isFetching: false,
  item: {},
  list: [],
  show: false
};

export function logReducer(state = initialState, action: ILogAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case OPEN_LOG:
      return Object.assign({}, state, {
        show: true
      });

    case CLOSE_LOG:
      return Object.assign({}, state, {
        show: false
      });

    default:
      return state;
  }
}

export function getRequest(): ILogAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): ILogAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): ILogAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getLog() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response: any = await request({
        url: '/Api/Log/GetList'
      });
      dispatch(getSuccess(response));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function getLogByProcessId(processId: number) {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response: any = await request({
        url: '/Api/Log/GetByProcessId',
        data: {processId}
      });
      dispatch(getSuccess([response]));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function openLog(): ILogAction {
  return {
    type: OPEN_LOG
  };
}

export function closeLog(): ILogAction {
  return {
    type: CLOSE_LOG
  };
}

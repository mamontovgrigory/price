import { IInteriors, IInteriorItem, IInteriorsAction } from '@models/interiors';
import { getLog, openLog } from '@data/log';
import { request } from '@utils/request';

export const GET_REQUEST: string = 'interiors/GET_REQUEST';
export const GET_SUCCESS: string = 'interiors/GET_SUCCESS';
export const GET_FAILURE: string = 'interiors/GET_FAILURE';
export const SAVE_REQUEST: string = 'interiors/SAVE_REQUEST';
export const SAVE_SUCCESS: string = 'interiors/SAVE_SUCCESS';
export const SAVE_FAILURE: string = 'interiors/SAVE_FAILURE';
export const DELETE_REQUEST: string = 'interiors/DELETE_REQUEST';
export const DELETE_SUCCESS: string = 'interiors/DELETE_SUCCESS';
export const DELETE_FAILURE: string = 'interiors/DELETE_FAILURE';
export const UPLOAD_REQUEST: string = 'interiors/UPLOAD_REQUEST';
export const UPLOAD_SUCCESS: string = 'interiors/UPLOAD_SUCCESS';
export const UPLOAD_FAILURE: string = 'interiors/UPLOAD_FAILURE';
export const CHECK_REQUEST: string = 'interiors/CHECK_REQUEST';
export const CHECK_SUCCESS: string = 'interiors/CHECK_SUCCESS';
export const CHECK_FAILURE: string = 'interiors/CHECK_FAILURE';
export const SET_ITEM: string = 'interiors/SET_ITEM';

const initialState: IInteriors = {
  isFetching: false,
  item: {},
  list: []
};

export function interiorsReducer(state = initialState, action: IInteriorsAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SAVE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case SAVE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case SAVE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case DELETE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case DELETE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case DELETE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case UPLOAD_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case UPLOAD_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case UPLOAD_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case CHECK_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case CHECK_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case CHECK_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case SET_ITEM:
      return Object.assign({}, state, {
        item: action.payload.item
      });

    default:
      return state;
  }
}

export function getRequest(): IInteriorsAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): IInteriorsAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): IInteriorsAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getInteriors() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response: any = await request({
        url: '/Api/Interiors/GetList'
      });
      dispatch(getSuccess(response));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function saveRequest(): IInteriorsAction {
  return {
    type: SAVE_REQUEST
  };
}

export function saveSuccess(): IInteriorsAction {
  return {
    type: SAVE_SUCCESS
  };
}

export function saveFailure(message: any): IInteriorsAction {
  return {
    type: SAVE_FAILURE,
    payload: {
      message
    }
  };
}

export function saveInteriorItem(data: {item: IInteriorItem}) {
  return async (dispatch) => {
    dispatch(saveRequest());
    try {
      await request({
        url: '/Api/Interiors/Save',
        data
      });
      dispatch(saveSuccess());
      dispatch(getInteriors());
    } catch (error) {
      dispatch(saveFailure(error));
    }
  };
}

export function deleteRequest(): IInteriorsAction {
  return {
    type: DELETE_REQUEST
  };
}

export function deleteSuccess(): IInteriorsAction {
  return {
    type: DELETE_SUCCESS
  };
}

export function deleteFailure(message: any): IInteriorsAction {
  return {
    type: DELETE_FAILURE,
    payload: {
      message
    }
  };
}

export function deleteInteriorItems(data: {ids: number[]}) {
  return async (dispatch) => {
    dispatch(deleteRequest());
    try {
      await request({
        url: '/Api/Interiors/Delete',
        data
      });
      dispatch(deleteSuccess());
      dispatch(getInteriors());
    } catch (error) {
      dispatch(deleteFailure(error));
    }
  };
}

export function uploadRequest(): IInteriorsAction {
  return {
    type: UPLOAD_REQUEST
  };
}

export function uploadSuccess(): IInteriorsAction {
  return {
    type: UPLOAD_SUCCESS
  };
}

export function uploadFailure(message: any): IInteriorsAction {
  return {
    type: UPLOAD_FAILURE,
    payload: {
      message
    }
  };
}

export function uploadInteriorsItems(files: File[]) {
  return async (dispatch) => {
    dispatch(uploadRequest());
    try {
      await request({
        url: '/Api/Interiors/Upload',
        files
      });
      dispatch(uploadSuccess());
      dispatch(getLog());
      dispatch(openLog());
      dispatch(getInteriors());
    } catch (error) {
      dispatch(uploadFailure(error));
    }
  };
}

export function checkRequest(): IInteriorsAction {
  return {
    type: CHECK_REQUEST
  };
}

export function checkSuccess(): IInteriorsAction {
  return {
    type: CHECK_SUCCESS
  };
}

export function checkFailure(message: any): IInteriorsAction {
  return {
    type: CHECK_FAILURE,
    payload: {
      message
    }
  };
}

export function checkInteriorsItems() {
  return async (dispatch) => {
    dispatch(checkRequest());
    try {
      await request({
        url: '/Api/Interiors/Check'
      });
      dispatch(checkSuccess());
      dispatch(getLog());
      dispatch(openLog());
    } catch (error) {
      dispatch(checkFailure(error));
    }
  };
}

export function setInteriorItem(item: IInteriorItem): IInteriorsAction {
  return {
    type: SET_ITEM,
    payload: {
      item: {
        id: item.id ? Number(item.id) : undefined,
        code: item.code ? item.code : '',
        mark: item.mark ? item.mark : '',
        explanation: item.explanation ? item.explanation : ''
      }
    }
  };
}

import { IComplectations, IComplectationItem, IComplectationsAction } from '@models/complectations';
import { getLog, openLog } from '@data/log';
import { request } from '@utils/request';

export const GET_REQUEST: string = 'complectations/GET_REQUEST';
export const GET_SUCCESS: string = 'complectations/GET_SUCCESS';
export const GET_FAILURE: string = 'complectations/GET_FAILURE';
export const SAVE_REQUEST: string = 'complectations/SAVE_REQUEST';
export const SAVE_SUCCESS: string = 'complectations/SAVE_SUCCESS';
export const SAVE_FAILURE: string = 'complectations/SAVE_FAILURE';
export const DELETE_REQUEST: string = 'complectations/DELETE_REQUEST';
export const DELETE_SUCCESS: string = 'complectations/DELETE_SUCCESS';
export const DELETE_FAILURE: string = 'complectations/DELETE_FAILURE';
export const UPLOAD_REQUEST: string = 'complectations/UPLOAD_REQUEST';
export const UPLOAD_SUCCESS: string = 'complectations/UPLOAD_SUCCESS';
export const UPLOAD_FAILURE: string = 'complectations/UPLOAD_FAILURE';
export const CHECK_REQUEST: string = 'complectations/CHECK_REQUEST';
export const CHECK_SUCCESS: string = 'complectations/CHECK_SUCCESS';
export const CHECK_FAILURE: string = 'complectations/CHECK_FAILURE';
export const SET_ITEM: string = 'complectations/SET_ITEM';

const initialState: IComplectations = {
  isFetching: false,
  item: {},
  list: []
};

export function complectationsReducer(state = initialState, action: IComplectationsAction) {
  switch (action.type) {
    case GET_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case GET_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        list: action.payload.list
      });

    case GET_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        message: action.payload.message,
        error: true
      });

    case SAVE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case SAVE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case SAVE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case DELETE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case DELETE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case DELETE_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case CHECK_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });

    case CHECK_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false
      });

    case CHECK_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: true
      });

    case SET_ITEM:
      return Object.assign({}, state, {
        item: action.payload.item
      });

    default:
      return state;
  }
}

export function getRequest(): IComplectationsAction {
  return {
    type: GET_REQUEST
  };
}

export function getSuccess(list): IComplectationsAction {
  return {
    type: GET_SUCCESS,
    payload: {
      list
    }
  };
}

export function getFailure(message: any): IComplectationsAction {
  return {
    type: GET_FAILURE,
    payload: {
      message
    }
  };
}

export function getComplectations() {
  return async (dispatch) => {
    dispatch(getRequest());
    try {
      const response: any = await request({
        url: '/Api/Complectations/GetList'
      });
      dispatch(getSuccess(response));
    } catch (error) {
      dispatch(getFailure(error));
    }
  };
}

export function saveRequest(): IComplectationsAction {
  return {
    type: SAVE_REQUEST
  };
}

export function saveSuccess(): IComplectationsAction {
  return {
    type: SAVE_SUCCESS
  };
}

export function saveFailure(message: any): IComplectationsAction {
  return {
    type: SAVE_FAILURE,
    payload: {
      message
    }
  };
}

export function saveComplectationItem(data: {item: IComplectationItem}) {
  return async (dispatch) => {
    dispatch(saveRequest());
    try {
      await request({
        url: '/Api/Complectations/Save',
        data
      });
      dispatch(saveSuccess());
      dispatch(getComplectations());
    } catch (error) {
      dispatch(saveFailure(error));
    }
  };
}

export function deleteRequest(): IComplectationsAction {
  return {
    type: DELETE_REQUEST
  };
}

export function deleteSuccess(): IComplectationsAction {
  return {
    type: DELETE_SUCCESS
  };
}

export function deleteFailure(message: any): IComplectationsAction {
  return {
    type: DELETE_FAILURE,
    payload: {
      message
    }
  };
}

export function deleteComplectationItems(data: {ids: number[]}) {
  return async (dispatch) => {
    dispatch(deleteRequest());
    try {
      await request({
        url: '/Api/Complectations/Delete',
        data
      });
      dispatch(deleteSuccess());
      dispatch(getComplectations());
    } catch (error) {
      dispatch(deleteFailure(error));
    }
  };
}

export function uploadRequest(): IComplectationsAction {
  return {
    type: UPLOAD_REQUEST
  };
}

export function uploadSuccess(): IComplectationsAction {
  return {
    type: UPLOAD_SUCCESS
  };
}

export function uploadFailure(message: any): IComplectationsAction {
  return {
    type: UPLOAD_FAILURE,
    payload: {
      message
    }
  };
}

export function uploadComplectationsItems(files: File[]) {
  return async (dispatch) => {
    dispatch(uploadRequest());
    try {
      await request({
        url: '/Api/Complectations/Upload',
        files
      });
      dispatch(uploadSuccess());
      dispatch(getLog());
      dispatch(openLog());
      dispatch(getComplectations());
    } catch (error) {
      dispatch(uploadFailure(error));
    }
  };
}

export function checkRequest(): IComplectationsAction {
  return {
    type: CHECK_REQUEST
  };
}

export function checkSuccess(): IComplectationsAction {
  return {
    type: CHECK_SUCCESS
  };
}

export function checkFailure(message: any): IComplectationsAction {
  return {
    type: CHECK_FAILURE,
    payload: {
      message
    }
  };
}

export function checkComplectationsItems() {
  return async (dispatch) => {
    dispatch(checkRequest());
    try {
      await request({
        url: '/Api/Complectations/Check'
      });
      dispatch(checkSuccess());
      dispatch(getLog());
      dispatch(openLog());
    } catch (error) {
      dispatch(checkFailure(error));
    }
  };
}

export function setComplectationItem(item: IComplectationItem): IComplectationsAction {
  return {
    type: SET_ITEM,
    payload: {
      item: {
        id: item.id ? Number(item.id) : undefined,
        code: item.code ? item.code : '',
        mark: item.mark ? item.mark : '',
        model: item.model ? item.model : '',
        description: item.description ? item.description : '',
        explanation: item.explanation ? item.explanation : ''
      }
    }
  };
}

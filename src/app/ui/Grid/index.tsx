import * as React from 'react';
import * as $ from 'jquery';
import { uniqueId, isEqual, uniq } from 'lodash';
import 'free-jqgrid';
import 'free-jqgrid/css/ui.jqgrid.min.css';

enum SortOrder {
  ASC = 'asc',
  DESC = 'desc'
}

interface IProps {
  colModel: Array<{
    name: string;
    key?: boolean;
    hidden?: boolean;
    label?: string;
    classes?: string;
    formatter?: any; // (cellvalue: string, options?: any, rowObject?: any) => string;
    width?: number;
  }>;
  data?: any[];
  rowNum?: number;
  rowList?: number[];
  sortName?: string;
  sortOrder?: SortOrder;
  onComplete?: () => void;
  onDoubleClick?: (rowId: string, data: any) => void;
  onChange?: (selected: string[], data: any) => void;
}

interface IState {
}

class Grid extends React.Component<IProps, IState> {
  private id: string = 'grid-' + uniqueId();

  public componentDidMount() {
    require('free-jqgrid/js/i18n/grid.locale-ru.js'); // TODO: Get language from localization reducer
    this.init(this.props);
  }

  public componentWillReceiveProps(nextProps: IProps) {
    const {data} = this.props;
    if (!isEqual(data, nextProps.data)) {
      const gridId = this.id;
      const $grid: any = $(`#${gridId}`);
      $grid.jqGrid('clearGridData').jqGrid('setGridParam', {data: nextProps.data}).trigger('reloadGrid');
    }
  }

  private onChange = () => {
    const {onChange} = this.props;
    const gridId = this.id;
    const $grid: any = $(`#${gridId}`);
    if (onChange) {
      const ids = $grid.jqGrid('getGridParam', 'selarrrow');
      const data = ids.map((item) => {
        return $grid.jqGrid('getRowData', item);
      });
      onChange([].concat(ids), [].concat(data));
    }
  }

  private getColModel = () => {
    const {colModel, data} = this.props;
    return colModel.map((columnItem) => {
      switch (columnItem.formatter) {
        case 'select':
          const uniqueValues =
            uniq(data.filter((item) => item[columnItem.name])
              .map((item) => String(item[columnItem.name]).toLowerCase()))
              .sort()
              .map((item) => `${item}:${item}`).join(';');
          return {
            ...columnItem,
            stype: 'select',
            searchoptions: {
              value: `:;${uniqueValues}`
            }
          };
        default:
          return {
            ...columnItem,
            sorttype: 'string',
            searchoptions: {sopt: ['cn', 'eq', 'bw', 'bn', 'nc', 'ew', 'en']}
          };
      }
    });
  }

  private init(props: IProps) {
    const {data, rowNum, rowList, sortName, sortOrder, onDoubleClick, onComplete} = props;
    const gridId = this.id;
    const $grid: any = $(`#${gridId}`);
    const pagerId = `p-${gridId}`;
    const colModel = this.getColModel();

    $grid.jqGrid('GridUnload');

    $grid.after($('<div>', {
      id: pagerId
    }));

    function setGridWidth() {
      const gridParentWidth = $('#gbox_' + gridId).parent().width();
      $grid.jqGrid('setGridWidth', gridParentWidth - 2, $grid.width() < gridParentWidth);
    }

    $(window).bind('resize', () => {
      setGridWidth();
    });

    $grid.jqGrid({
      colModel,
      data: data ? [].concat(data) : null,
      datatype: 'local',
      mtype: null,
      multiselect: true,
      multiboxonly: false,
      viewrecords: true,
      altRows: true,
      rowNum: rowNum ? rowNum : 25,
      rowList: rowList ? rowList : [15, 25, 50, 100, 200, 500],
      pager: '#' + pagerId,
      autoWidth: true,
      shrinkToFit: false,
      sortname: sortName ? sortName : 'id',
      sortorder: sortOrder ? sortOrder : 'desc',
      jsonReader: {
        page: 'page',
        total: 'total',
        records: 'records',
        root: 'rows',
        repeatitems: false,
        id: 'id'
      },
      gridComplete: () => {
        if (onComplete) {
          onComplete();
        }
        setTimeout(() => {
          setGridWidth();
        }, 50);
      },
      onSelectRow: () => {
        this.onChange();
      },
      onSelectAll: () => {
        this.onChange();
      },
      ondblClickRow: (rowId: string) => {
        const data = $grid.jqGrid('getRowData', rowId);
        if (onDoubleClick) {
          onDoubleClick(rowId, data);
        }
      }
    });

    $grid.jqGrid('filterToolbar',
      {
        searchOnEnter: false,
        stringResult: true,
        defaultSearch: 'cn',
        autoSearch: true,
        searchOperators: true,
        afterSearch: () => {
          const lastSelectedData: any = [].concat($grid.jqGrid('getGridParam', 'lastSelectedData'));
          colModel.forEach((columnItem: any) => {
            if (columnItem.stype === 'select') {
              const uniqueValues =
                uniq(lastSelectedData.filter((item) => item[columnItem.name])
                  .map((item) => String(item[columnItem.name]).toLowerCase()));
              const $select = $(`#gs_${gridId}_${columnItem.name}`);
              $select.find('option').hide();
              $select.find(`option[value='']`).show();
              uniqueValues.forEach((value) => {
                $select.find(`option[value='${value}']`).show();
              });
            }
          });
        }
      });
  }

  public render() {
    return (
      <table id={this.id}/>
    );
  }
}

export { Grid }

import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

interface IItem {
  header?: string | JSX.Element;
  icon?: string;
  content?: string | JSX.Element | JSX.Element[];
}

const onSelect = () => {
  $(window).trigger('resize');
};

const CollapsibleItem = ({header, icon, content}: IItem) => (
  <ReactMaterialize.CollapsibleItem header={header} icon={icon} onSelect={onSelect}>
    {content}
  </ReactMaterialize.CollapsibleItem>
);

interface IProps extends IItem {
  items?: IItem[];
  popout?: boolean;
}

export const Collapsible = ({header, icon, content, items, popout}: IProps) => (
  <ReactMaterialize.Collapsible popout={popout}>
    {header && content && <CollapsibleItem header={header} icon={icon} content={content}/>}
    {items && items.map((item, index) => {
      return (<CollapsibleItem key={index} header={item.header} icon={item.icon} content={item.content}/>);
    })}
  </ReactMaterialize.Collapsible>
);

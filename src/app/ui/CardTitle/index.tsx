import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

interface IProps {
  image: string;
}

interface IState {}

export class CardTitle extends React.Component<IProps, IState> {
  public render() {
    const {image} = this.props;
    return (
      <ReactMaterialize.CardTitle reveal={true} image={image} waves="light" />
    );
  }
}

import * as React from 'react';

import { Modal } from '@ui';
import { Loc } from '@components';

interface IProps {
  show: boolean;
  text?: string | JSX.Element;
  onConfirm?: () => void;
  onClose?: () => void;
}

interface IState {}

export class Confirm extends React.Component<IProps, IState> {
  public render() {
    const { show, text, onConfirm, onClose } = this.props;
    const actions = [{
      text: <Loc locKey="ok"/>,
      onClick: onConfirm
    }];
    return (
      <Modal show={show}
             actions={actions}
             header={<Loc locKey="confirmAction"/>}
             onClose={onClose}>
        <div>{text}</div>
      </Modal>
    );
  }
}

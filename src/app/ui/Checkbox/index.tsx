import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

import { Col } from '@ui';
const style = require('./style.scss');

interface IProps {
  checked?: boolean;
  disabled?: boolean;
  label?: string | React.ReactNode;
  sm?: number;
  md?: number;
  lg?: number;
  onChange?: (checked: boolean) => void;
}

interface IState {
}

export class Checkbox extends React.Component<IProps, IState> {
  private handleChange = (e) => {
    const {onChange} = this.props;
    if (onChange) {
      onChange(e.target.checked);
    }
  }

  public render() {
    const {checked, disabled, label, sm, md, lg} = this.props;
    return (
      <Col className={style.Checkbox} sm={sm} md={md} lg={lg}>
        <ReactMaterialize.Input
          type="checkbox"
          label={label}
          checked={checked}
          defaultChecked={checked}
          disabled={disabled}
          onChange={this.handleChange}
        />
      </Col>
    );
  }
}

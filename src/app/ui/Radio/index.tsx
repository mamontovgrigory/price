import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

import { Col } from '@ui';

interface IProps {
  name: string;
  value: string;
  label?: string | React.ReactNode;
  checked?: boolean;
  disabled?: boolean;
  sm?: number;
  md?: number;
  lg?: number;
  onChange?: (value: string) => void;
}

interface IState {
}

export class Radio extends React.Component<IProps, IState> {
  private handleClick = (event) => {
    const {onChange} = this.props;
    if (onChange) {
      onChange(event.target.value);
    }
  }

  public render() {
    const {name, value, checked, disabled, label, sm, md, lg} = this.props;
    return (
      <Col sm={sm} md={md} lg={lg}>
        <ReactMaterialize.Input
          name={name}
          checked={checked}
          type="radio"
          value={value}
          label={label}
          disabled={disabled}
          className="with-gap"
          onClick={this.handleClick}
        />
      </Col>
    );
  }
}

import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

const style = require('./style.scss');

interface IProps {
  label?: string | React.ReactNode;
  name?: string;
  value?: string;
  type?: 'password' | 'number' | 'text';
  validate?: boolean;
  disabled?: boolean;
  onChange?: (value: string) => void;
  sm?: number;
  md?: number;
  lg?: number;
  getRef?: (input: HTMLInputElement) => void;
}

interface IState {
}

export class Input extends React.Component<IProps, IState> {
  private setRef = (input) => {
    const {getRef} = this.props;
    if (getRef && input && input.input) {
      getRef(input.input);
    }
  }

  private handleChange = (e) => {
    const {onChange} = this.props;
    if (onChange) {
      onChange(e.target.value);
    }
  }

  public render() {
    const {label, name, value, type, disabled, validate, sm, md, lg, children} = this.props;
    const max = 12;
    let s = sm;
    let m = md;
    let l = lg;
    if (!sm && !md && !lg) {
      s = m = l = max;
    }
    return (
      <ReactMaterialize.Input className={style.Input}
                              name={name}
                              label={label}
                              type={type ? type : 'text'}
                              validate={validate}
                              disabled={disabled}
                              onChange={this.handleChange}
                              value={value}
                              ref={this.setRef}
                              s={s}
                              m={m}
                              l={l}>
        {children}
      </ReactMaterialize.Input>
    );
  }
}

import * as React from 'react';

import { Row, Col, Grid, Button, Icon, Modal, Confirm, Dropzone } from '@ui';
import { Loc } from '@components';

interface IProps {
  title: string | JSX.Element;
  colModel: any;
  list: any;
  item: any;
  form: JSX.Element;
  setItem?: (item: any) => void;
  saveItem?: (data: { item: any }) => Promise<null>;
  deleteItems?: (data: { ids: number[] }) => Promise<null>;
  upload?: (files: File[]) => Promise<null>;
  onComplete?: () => void;
  actions?: Array<{
    title: string | JSX.Element;
    onClick: () => void;
  }>;
}

interface IState {
  showForm: boolean;
  showDeleteConfirmForm: boolean;
  showMessage: boolean;
  showUploadForm: boolean;
  messageHeader: string | JSX.Element;
  messageText: string | JSX.Element;
  selected: string[];
  files: File[];
}

export class Crud extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      showForm: false,
      showDeleteConfirmForm: false,
      showMessage: false,
      showUploadForm: false,
      messageHeader: '',
      messageText: '',
      selected: [],
      files: []
    };
  }

  private openForm = () => {
    this.setState({
      showForm: true
    });
  }

  private closePriceForm = () => {
    this.setState({
      showForm: false
    });
  }

  private openMessage = (title, text) => {
    this.setState({
      showMessage: true,
      messageHeader: title,
      messageText: text
    });
  }

  private closeMessage = () => {
    this.setState({
      showMessage: false
    });
  }

  private openDeleteConfirmForm = () => {
    const {selected} = this.state;
    if (selected && selected.length) {
      this.setState({
        showDeleteConfirmForm: true
      });
    } else {
      this.openMessage(
        <Loc locKey="noOneRowChosen"/>,
        <Loc locKey="pleaseChooseRowsToDelete" options={{count: selected.length}}/>
      );
    }
  }

  private closeDeleteConfirmForm = () => {
    this.setState({
      showDeleteConfirmForm: false
    });
  }

  private openUploadForm = () => {
    this.setState({
      showUploadForm: true
    });
  }

  private closeUploadForm = () => {
    this.setState({
      showUploadForm: false
    });
  }

  private handleChangeFiles = (files: File[]) => {
    this.setState({
      files
    });
  }

  private uploadFiles = () => {
    const {upload} = this.props;
    const {files} = this.state;
    if (upload && files.length) {
      upload(files).then(() => {
        this.closeUploadForm();
      });
    }
  }

  private handleSelect = (selected) => {
    this.setState({
      selected
    });
  }

  private createItem = () => {
    const {setItem} = this.props;
    if (setItem) {
      setItem({});
      this.openForm();
    }
  }

  private editItem = (id: string) => {
    const {list, setItem} = this.props;
    const item = list.find((item) => Number(item.id) === Number(id));
    if (setItem) {
      setItem(item);
      this.openForm();
    }
  }

  private handleClickEdit = () => {
    const {selected} = this.state;
    if (selected && selected.length === 1) {
      this.editItem(selected[0]);
    } else {
      this.openMessage(<Loc locKey="chooseOneRow"/>, <Loc locKey="pleaseChooseOneRowToEdit"/>);
    }
  }

  private saveItem = () => {
    const {item, saveItem} = this.props;
    this.closePriceForm();
    if (saveItem) {
      saveItem({item});
    }
  }

  private handleDoubleClick = (id: string) => {
    this.editItem(id);
  }

  private deleteRows = () => {
    const {deleteItems} = this.props;
    if (deleteItems) {
      const {selected} = this.state;
      const ids = selected.map((id) => Number(id));
      deleteItems({ids});
      this.closeDeleteConfirmForm();
    }
  }

  public render() {
    const {title, colModel, list, form, saveItem, setItem, deleteItems, upload, actions, onComplete} = this.props;
    const {
      selected,
      showForm,
      showMessage,
      messageHeader,
      messageText,
      showDeleteConfirmForm,
      showUploadForm
    } = this.state;
    const formActions = [{
      text: <Loc locKey="save"/>,
      onClick: this.saveItem
    }];
    const uploadFormActions = [{
      text: <Loc locKey="load"/>,
      onClick: this.uploadFiles
    }];
    const createButton = saveItem ? (
      <Button onClick={this.createItem}>
        <Icon type="playlist_add"/>
      </Button>
    ) : null;
    const editButton = saveItem && setItem ? (
      <Button onClick={this.handleClickEdit}>
        <Icon type="mode_edit"/>
      </Button>
    ) : null;
    const deleteButton = deleteItems ? (
      <Button onClick={this.openDeleteConfirmForm}>
        <Icon type="delete"/>
      </Button>
    ) : null;
    const uploadButton = upload ? (
      <Button onClick={this.openUploadForm}>
        <Icon type="file_upload"/>
      </Button>
    ) : null;
    return (
      <div>
        <Row>
          <Col>
            <h4>{title}</h4>
          </Col>
        </Row>
        <Row>
          <Col>
            {createButton}
            {editButton}
            {deleteButton}
            {uploadButton}
            {actions && actions.length && actions.map((action) => (
              <Button onClick={action.onClick}>
                {action.title}
              </Button>
            ))}
          </Col>
        </Row>
        <Row>
          <Col>
            <Grid
              colModel={colModel}
              data={list}
              onChange={this.handleSelect}
              onDoubleClick={this.handleDoubleClick}
              onComplete={onComplete}
            />
          </Col>
        </Row>
        <Modal show={showForm}
               header={<Loc locKey="editing"/>}
               actions={formActions}
               onClose={this.closePriceForm}>
          {form}
        </Modal>
        <Modal show={showMessage}
               header={messageHeader}
               onClose={this.closeMessage}>
          {messageText}
        </Modal>
        <Confirm show={showDeleteConfirmForm}
                 text={<Loc locKey="deleteChosenData" options={{count: selected.length}}/>}
                 onConfirm={this.deleteRows}
                 onClose={this.closeDeleteConfirmForm}/>
        <Modal show={showUploadForm}
               header={<Loc locKey="upload"/>}
               actions={uploadFormActions}
               onClose={this.closeUploadForm}>
          <Dropzone onChange={this.handleChangeFiles}/>
        </Modal>
      </div>
    );
  }
}

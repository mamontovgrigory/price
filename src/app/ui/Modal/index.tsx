import * as React from 'react';
import * as $ from 'jquery';
import * as classNames from 'classnames';
import { uniqueId } from 'lodash';

import { Loc } from '@components';
import { Button } from '@ui';
const style = require('./style.scss');

interface IProps {
  header: string | React.ReactNode;
  trigger?: React.ReactElement<any>;
  show?: boolean;
  actions?: Array<{
    text: string | React.ReactNode;
    onClick?: () => void;
  }>;
  onClose?: () => void;
}

interface IState {
}

export class Modal extends React.Component<IProps, IState> {
  private modalId: string = uniqueId('modal-');

  private updateTextFields;

  public componentDidMount() {
    this.updateTextFields = () => {
      const Materialize = require('materialize-css');
      if (Materialize && Materialize.updateTextFields) {
        Materialize.updateTextFields();
      }
    };
  }

  public componentWillReceiveProps(nextProps: IProps) {
    const {show} = this.props;
    if (nextProps.show !== show) {
      if (nextProps.show) {
        this.open();
      } else {
        this.close();
      }
    }
    return true;
  }

  private open = () => {
    const $modal: any = $('#' + this.modalId);
    $modal.modal({
      ready: () => {
        $(window).trigger('resize');
        if (this.updateTextFields) {
          this.updateTextFields();
        }
      },
      complete: () => {
        this.handleClose();
      }
    });
    $modal.modal('open');
  }

  private close = () => {
    const $modal: any = $('#' + this.modalId);
    $modal.modal('close');
  }

  private handleClose = () => {
    const {onClose} = this.props;
    if (onClose) {
      onClose();
    }
  }

  public render() {
    const {header, trigger, actions, children} = this.props;
    const className = classNames([style.Modal, 'modal', 'modal-fixed-footer']);
    return (
      <span>
        {trigger && <span className="" onClick={this.open}>{trigger}</span>}
        <div id={this.modalId} className={className}>
          <div className="modal-content">
            <h4>{header}</h4>
            <div>
              {children}
            </div>
          </div>
          <div className="modal-footer">
            {actions && actions.map(({text, onClick}, index) => {
              return (<Button key={index} onClick={onClick}>{text}</Button>);
            })}
            <Button onClick={this.close}>
              <Loc locKey="close"/>
            </Button>
          </div>
        </div>
      </span>
    );
  }
}

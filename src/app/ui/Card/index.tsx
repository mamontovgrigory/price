import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

interface IProps {
  header: JSX.Element;
  title: string;
  className: string;
  reveal: JSX.Element;
}

interface IState {}

export class Card extends React.Component<IProps, IState> {
  public render() {
    return (
      <ReactMaterialize.Card {...this.props} />
    );
  }
}

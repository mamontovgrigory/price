import * as React from 'react';
import * as $ from 'jquery';
import * as classNames from 'classnames';
import { Link } from 'react-router';

import { Container, Icon } from '@ui';
import { Loc } from '@components';
const style = require('./style.scss');

interface IProps {
  version: string;
  logo: JSX.Element;
  account?: {
    name: string;
  };
  links?: any[];
  onExit?: () => void;
}

interface IState {
}

class Navbar extends React.Component<IProps, IState> {
  public componentDidMount() {
    ($('.side-nav-collapse') as any).sideNav({
      menuWidth: 300,
      edge: 'left',
      closeOnClick: false,
      draggable: true,
      onOpen: () => {
        $('#sidenav-overlay').hide();
      }
    });
    ($('.side-nav-collapse') as any).sideNav('show');
    ($('.dropdown-button') as any).dropdown();
  }

  private handleClickExit = () => {
    const {onExit} = this.props;
    if (onExit) {
      onExit();
    }
  }

  public render() {
    const {version, logo, links, account} = this.props;
    const authorized = Boolean(account);
    const panelClassName = classNames([
      'side-nav',
      'fixed',
      style.panel
    ]);
    return (
      <nav className={style.Navbar}>
        <div className="nav-wrapper">
          <Container>
            {version}
            <Link to="/" className="right" style={{height: '64px'}}>
              {logo}
            </Link>
          </Container>
        </div>

        <ul className={panelClassName} hidden={!authorized}>
          {
            links && links.map((el, index) => {
              return (
                <li key={index}>
                  <Link to={el.to} className="waves-effect">
                    <i className="material-icons">{el.icon}</i>
                    <span className={style.link}>{el.name}</span>
                  </Link>
                </li>
              );
            })
          }
          <li>
            <div className="divider"/>
          </li>
          <li>
            <a className="waves-effect" onClick={this.handleClickExit}>
              <Icon type="exit_to_app"/>
              <span className={style.link}>
                <Loc locKey="logout"/>
              </span>
            </a>
          </li>
        </ul>
      </nav>
    );
  }
}

export { Navbar }

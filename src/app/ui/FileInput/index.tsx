import * as React from 'react';

interface IProps {
  label?: string | React.ReactNode;
  sm?: number;
  md?: number;
  lg?: number;
}

interface IState {}

export class FileInput extends React.Component<IProps, IState> {
  public render() {
    const {label} = this.props;
    return (
      <div className="file-field input-field">
        <div className="btn">
          <span>{label}</span>
          <input type="file"/>
        </div>
        <div className="file-path-wrapper">
          <input className="file-path validate" type="text"/>
        </div>
      </div>
    );
  }
}

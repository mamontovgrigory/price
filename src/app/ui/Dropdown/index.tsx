import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

import {Button, Icon} from '@ui';

const style = require('./style.scss');

interface IProps {
  label: string | React.ReactNode;
  actions: Array<{
    label: string | React.ReactNode;
    onClick?: () => void;
  }>;
}

interface IState {
}

class Dropdown extends React.Component<IProps, IState> {
  public render() {
    const {label, actions} = this.props;
    const trigger = (
      <Button>
        {label}<Icon type="arrow_drop_down" right={true}/>
      </Button>
    );
    return (
      <ReactMaterialize.Dropdown className={style.Dropdown}
                                 trigger={<span className={style.trigger}>{trigger}</span>}>
        {actions.map((action, index) => {
          const onClick = (event) => {
            if (action.onClick) {
              action.onClick();
            }
            event.preventDefault();
          };
          return (
            <ReactMaterialize.NavItem key={index} onClick={onClick}>
              {action.label}
            </ReactMaterialize.NavItem>
          );
        })}
      </ReactMaterialize.Dropdown>
    );
  }
}

export { Dropdown }

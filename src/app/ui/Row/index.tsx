import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

interface IProps {}

interface IState {}

export class Row extends React.Component<IProps, IState> {
  public render() {
    const {children} = this.props;
    return (
      <ReactMaterialize.Row>
        {children}
      </ReactMaterialize.Row>
    );
  }
}

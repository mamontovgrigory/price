import * as React from 'react';

interface IProps {
  image: string;
  height?: number;
}

interface IState {
}

export class MediaBox extends React.Component<IProps, IState> {
  public render() {
    const {image, height} = this.props;
    return (
      <img className="materialboxed" height={height} src={image}/>
    );
  }
}

import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

const style = require('./style.scss');

interface IProps {}

interface IState {}

export class Container extends React.Component<IProps, IState> {
  public render() {
    const {children} = this.props;
    return (
      <ReactMaterialize.Container className={style.Container}>
        {children}
      </ReactMaterialize.Container>
    );
  }
}

import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';

const style = require('./style.scss');

interface IProps {
  disabled?: boolean;
  onClick?: (e) => void;
}

interface IState {}

export class Button extends React.Component<IProps, IState> {
  public render() {
    const {disabled, onClick, children} = this.props;
    return (
      <ReactMaterialize.Button className={style.Button} waves="light" disabled={disabled} onClick={onClick}>
        {children}
      </ReactMaterialize.Button>
    );
  }
}

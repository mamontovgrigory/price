import * as React from 'react';
import {isEqual} from 'lodash';

interface IProps {
  images: string[];
}

interface IState {
}

export class Carousel extends React.Component<IProps, IState> {
  public componentDidMount() {
    this.init();
  }

  public componentWillReceiveProps(nextProps: IProps) {
    if (!isEqual(this.props, nextProps)) {
      this.init();
    }
  }

  private init = () => {
    ($(`.carousel`) as any).carousel({fullWidth: true, indicators: true});
  }

  private handleClickPrev = () => {
    ($(`.carousel`) as any).carousel('next');
  }

  private handleClickNext = () => {
    ($(`.carousel`) as any).carousel('prev');
  }

  public render() {
    const {images} = this.props;
    return (
      <div className="carousel carousel-slider" data-indicators="true">
        <div className="carousel-fixed-item">
          <a className="btn waves-effect white grey-text darken-text-2 left" onClick={this.handleClickNext}>&lt;</a>
          <a className="btn waves-effect white grey-text darken-text-2 right" onClick={this.handleClickPrev}>&gt;</a>
        </div>
        {images.map((image, index) => {
          return (
            <a key={index} className="carousel-item">
              <img src={image}/>
            </a>
          );
        })}
      </div>
    );
  }
}

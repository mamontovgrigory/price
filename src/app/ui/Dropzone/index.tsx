import * as React from 'react';
import * as classNames from 'classnames';
import ReactDropzone from 'react-dropzone';

import { Loc } from '@components';
const style = require('./style.scss');

interface IProps {
  onChange?: (files: File[]) => void;
}

interface IState {
  files: File[];
}

export class Dropzone extends React.Component<IProps, IState> {
  constructor() {
    super();
    this.state = {files: []};
  }

  private onDrop = (files: File[]) => {
    const { onChange } = this.props;
    if (onChange) {
      onChange(files);
    }
    this.setState({
      files
    });
  }

  public render() {
    const {files} = this.state;
    const className = classNames([
      'dropzone',
      style.Dropzone
    ]);
    const text = files.length ? (
      <div>
        <span>
          <Loc locKey="chosenFiles"/>
        </span>
        <ul>
          {files.map((f) => <li key={f.name}>{f.name}</li>)}
        </ul>
      </div>
    ) : (
      <Loc locKey="chooseFiles"/>
    );
    return (
      <div className={className}>
        <ReactDropzone onDrop={this.onDrop}>
          <div className={style.text}>
            {text}
          </div>
        </ReactDropzone>
      </div>
    );
  }
}

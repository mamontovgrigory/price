import * as React from 'react';
import * as ReactMaterialize from 'react-materialize';
import {isUndefined} from 'lodash';

interface IProps {
  label: string | JSX.Element;
  value?: string;
  items: string[];
  onChange?: (value: string) => void;
  sm?: number;
  md?: number;
  lg?: number;
}

interface IState {
}

export class Autocomplete extends React.Component<IProps, IState> {
  private handleChange = (e) => {
    const {onChange} = this.props;
    if (onChange && !isUndefined(e.target.value)) {
      onChange(e.target.value);
    }
  }

  private handleAutocomplete = (value) => {
    const {onChange} = this.props;
    if (onChange) {
      onChange(value);
    }
  }

  public render() {
    const {label, value, items, sm, md, lg} = this.props;
    const data = {};
    for (const item of items) {
      data[item] = null;
    }
    const max = 12;
    let s = sm;
    let m = md;
    let l = lg;
    if (!sm && !md && !lg) {
      s = m = l = max;
    }
    return (
      <ReactMaterialize.Autocomplete
        title={label}
        value={value}
        data={data}
        onAutocomplete={this.handleAutocomplete}
        onChange={this.handleChange}
        s={s}
        m={m}
        l={l}
      />
    );
  }
}

import * as React from 'react';
import { IndexRoute, Route } from 'react-router';

import {
  App,
  GroupsPage,
  ColorsPage,
  ComplectationsPage,
  ConfigurationsPage,
  HomePage,
  InteriorPhotosPage,
  InteriorsPage,
  ModificationsPage,
  OptionsPage,
  UsersPage,
  StockPage
} from '@containers';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage}/>
    <Route path="groups" component={GroupsPage}/>
    <Route path="colors" component={ColorsPage}/>
    <Route path="complectations" component={ComplectationsPage}/>
    <Route path="configurations" component={ConfigurationsPage}/>
    <Route path="interior-photos" component={InteriorPhotosPage}/>
    <Route path="interiors" component={InteriorsPage}/>
    <Route path="modifications" component={ModificationsPage}/>
    <Route path="options" component={OptionsPage}/>
    <Route path="users" component={UsersPage}/>
    <Route path="price" component={StockPage}/>
  </Route>
);

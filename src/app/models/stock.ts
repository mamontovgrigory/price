export interface IStockItem {
  id?: number;
  client?: string;
  front?: string;
  back?: string;
  mark?: string;
  model?: string;
  complectation?: string;
  color?: string;
  body?: string;
  condition?: string;
  place?: string;
  date?: string;
  description?: string;
  options?: string;
  vin?: string;
  videos?: string;
  panoramas?: string;
  interiorsByVin?: string;
}

export interface IConfiguration {
  file: Array<{
    column: string | string[];
    like?: string[];
    invert?: boolean;
  }>;
  base: Array<{
    column: string | string[];
    like?: string[];
    invert?: boolean;
  }>;
  intersection?: boolean;
  ifExists?: boolean;
}

export interface IStock {
  isFetching?: boolean;
  item: IStockItem;
  list: IStockItem[];
  lastUpdated?: number;
  clientsList: string[];
  error?: boolean;
  message?: any;
  configuration: IConfiguration[];
  progress?: number;
}

interface IPayload {
  item?: IStockItem;
  list?: IStockItem[];
  configuration?: IConfiguration[];
  message?: any;
  progress?: number;
  lastUpdated?: number;
}

export interface IStockAction {
  type: string;
  payload?: IPayload;
}

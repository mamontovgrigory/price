export interface IInteriorPhotoItem {
  id?: number;
  code?: string;
  client?: string;
  mark?: string;
  model?: string;
  color?: string;
  photo?: string;
  explanation?: string;
  body?: string;
}

export interface IInteriorPhotos {
  isFetching?: boolean;
  item: IInteriorPhotoItem;
  list: IInteriorPhotoItem[];
  error?: boolean;
  message?: any;
}

export interface IInteriorPhotosAction {
  type: string;
  payload?: {
    list?: IInteriorPhotoItem[];
    item?: IInteriorPhotoItem;
    message?: any;
  };
}

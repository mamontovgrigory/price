export interface INavigation {
  navItems: INavItem[];
}

export interface INavItem {
  path?: string;
  name: string;
  src?: string;
  icon?: string;
  description?: string;
  to: string;
}

export interface ISetNavItems extends Redux.Action {
  navItems: INavItem[];
}

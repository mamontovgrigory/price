export interface IModificationItem {
  id?: number;
  code?: string;
  complectationCode?: string;
  complectationBase?: string;
  complectationAutoRu?: string;
  mark?: string;
  model?: string;
  complectation?: string;
  body?: string;
  engineCapacity?: string;
  power?: string;
  transmission?: string;
  engineType?: string;
  driveUnit?: string;
  loadCapacity?: string;
}

export interface IModifications {
  isFetching?: boolean;
  item: IModificationItem;
  list: IModificationItem[];
  error?: boolean;
  message?: any;
}

export interface IModificationsAction {
  type: string;
  payload?: {
    list?: IModificationItem[];
    item?: IModificationItem;
    message?: any;
  };
}

export interface IInteriorItem {
  id?: number;
  code?: string;
  mark?: string;
  explanation?: string;
}

export interface IInteriors {
  isFetching?: boolean;
  item: IInteriorItem;
  list: IInteriorItem[];
  error?: boolean;
  message?: any;
}

export interface IInteriorsAction {
  type: string;
  payload?: {
    list?: IInteriorItem[];
    item?: IInteriorItem;
    message?: any;
  };
}

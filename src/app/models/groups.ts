export interface IGroupItem {
  id?: number;
  name?: string;
  settings?: Array<{
    permissionId: number;
    value: string;
  }>;
}

export interface IGroups {
  isFetching?: boolean;
  item: IGroupItem;
  list: IGroupItem[];
  error?: boolean;
  message?: any;
}

export interface IGroupsAction {
  type: string;
  payload?: {
    list?: IGroupItem[];
    item?: IGroupItem;
    message?: any;
  };
}

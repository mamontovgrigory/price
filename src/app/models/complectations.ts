export interface IComplectationItem {
  id?: number;
  code?: string;
  mark?: string;
  model?: string;
  description?: string;
  explanation?: string;
}

export interface IComplectations {
  isFetching?: boolean;
  item: IComplectationItem;
  list: IComplectationItem[];
  error?: boolean;
  message?: any;
}

export interface IComplectationsAction {
  type: string;
  payload?: {
    list?: IComplectationItem[];
    item?: IComplectationItem;
    message?: any;
  };
}

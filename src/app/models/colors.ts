export interface IColorItem {
  id?: number;
  code?: string;
  mark?: string;
  name?: string;
  fullName?: string;
}

export interface IColors {
  isFetching?: boolean;
  item: IColorItem;
  list: IColorItem[];
  error?: boolean;
  message?: any;
}

export interface IColorsAction {
  type: string;
  payload?: {
    list?: IColorItem[];
    item?: IColorItem;
    message?: any;
  };
}

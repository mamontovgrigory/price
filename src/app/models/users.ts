export interface IUserItem {
  id?: number;
  login?: string;
  password?: string;
  groupId?: number;
}

export interface IAccountPermissions {
  usersManage: boolean;
  groupsManage: boolean;
}

export interface IAccount extends IUserItem {
  permissions: IAccountPermissions;
}

export interface IUsers {
  isFetching?: boolean;
  account?: IAccount;
  item: IUserItem;
  list: IUserItem[];
  error?: boolean;
  message?: any;
}

interface IPayload extends IUserItem {
  account?: IAccount;
  list?: IUserItem[];
  item?: IUserItem;
  message?: any;
}

export interface IUsersAction {
  type: string;
  payload?: IPayload;
}

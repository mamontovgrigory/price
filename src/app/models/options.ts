export interface IOptionItem {
  id?: number;
  code?: string;
  mark?: string;
  model?: string;
  explanation?: string;
  disabled?: boolean;
}

export interface IOptions {
  isFetching?: boolean;
  item: IOptionItem;
  list: IOptionItem[];
  error?: boolean;
  message?: any;
}

export interface IOptionsAction {
  type: string;
  payload?: {
    list?: IOptionItem[];
    item?: IOptionItem;
    message?: any;
  };
}

export interface ILogItem {
  id?: number;
  log?: string;
}

export interface ILog {
  isFetching?: boolean;
  item: ILogItem;
  list: ILogItem[];
  show: boolean;
  error?: boolean;
  message?: any;
}

export interface ILogAction {
  type: string;
  payload?: {
    list?: ILogItem[];
    item?: ILogItem;
    message?: any;
  };
}

export interface IPermissionItem {
  id: number;
  alias: string;
  column?: string;
  name: string;
  sort?: number;
  table?: string;
}

export interface IPermissions {
  isFetching?: boolean;
  list: IPermissionItem[];
  error?: boolean;
  message?: any;
}

export interface IPermissionsAction {
  type: string;
  payload?: {
    list?: IPermissionItem[];
    message?: any;
  };
}

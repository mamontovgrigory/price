export interface ILocalization {
  currentLanguage?: string;
  languages: ILanguage[];
  resources: IResources;
}

export interface ILanguage {
  code: string;
  name: string;
}

export interface IResources {
  [languageCode: string]: {
    [key: string]: any
  };
}

export interface ISetCurrentLanguageAction extends Redux.Action {
  languageCode: string;
}

export interface ISetLanguagesAction extends Redux.Action {
  languages: ILanguage[];
}

export interface ISetResources extends Redux.Action {
  resources: any;
}

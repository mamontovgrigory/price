export interface IConfigurationItem {
  id?: number;
  name?: string;
  configuration?: string;
}

export interface IConfigurations {
  isFetching?: boolean;
  item: IConfigurationItem;
  list: IConfigurationItem[];
  error?: boolean;
  message?: any;
}

export interface IConfigurationsAction {
  type: string;
  payload?: {
    list?: IConfigurationItem[];
    item?: IConfigurationItem;
    message?: any;
  };
}

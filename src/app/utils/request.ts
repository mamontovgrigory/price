import { isObject, isArray } from 'lodash';

interface IRequestProps {
  url: string;
  host?: string;
  method?: string;
  contentType?: boolean;
  credentials?: string;
  data?: any;
  files?: File[];
}

export const request = async (props: IRequestProps) => {
  const {host, url, method = 'POST', data, files} = props;
  const href = (host ? host : window.location.origin) + url;
  if ((files && files.length) || props.contentType === false) { // TODO: Create universal data format
    const body = new FormData();
    if (data) {
      for (const key of Object.keys(data)) {
        body.append(key, isObject(data[key]) || isArray(data[key]) ? JSON.stringify(data[key]) : data[key]);
      }
    }
    if (files && files.length) {
      for (const file of files) {
        body.append('file', file);
      }
    }
    return await fetch(href, {
      method,
      credentials: 'include',
      body
    });
  } else {
    const response = await fetch(href, {
      method,
      credentials: 'include',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    if (!response.ok) {
      throw response.json();
    }
    const contentType = response.headers.get('content-type');
    if (contentType && contentType.indexOf('application/json') !== -1) {
      return response.json();
    }
    return response;
  }
};

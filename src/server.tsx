import 'babel-polyfill';
import * as e6p from 'es6-promise';
(e6p as any).polyfill();
import 'isomorphic-fetch';

import * as React from 'react';
import * as ReactDOMServer from 'react-dom/server';
import * as moment from 'moment-timezone';

import { Provider } from 'react-redux';
import { createMemoryHistory, match } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
const {ReduxAsyncConnect, loadOnServer} = require('redux-connect');
import HTTP_STATUS_CODES from 'http-status-enum';
import { configureStore } from './app/redux/store';

const express = require('express');
const http = require('http');
const path = require('path');
const Chalk = require('chalk');
const favicon = require('serve-favicon');

moment.tz.setDefault('Europe/Moscow');
import routes from './app/routes';
import { Html } from './app/containers';
const appConfig = require('../config/main');
const manifest = require('../build/manifest.json');
import { appApi } from './server/index';

const app = express();

app.use(appApi);

if (process.env.NODE_ENV !== 'production') {
  const webpack = require('webpack');
  const webpackConfig = require('../config/webpack/dev');
  const webpackCompiler = webpack(webpackConfig);

  app.use(require('webpack-dev-middleware')(webpackCompiler, {
    publicPath: webpackConfig.output.publicPath,
    stats: {colors: true},
    noInfo: true,
    hot: true,
    inline: true,
    lazy: false,
    historyApiFallback: true,
    quiet: true
  }));

  app.use(require('webpack-hot-middleware')(webpackCompiler));
}

app.use(favicon(path.join(__dirname, 'public/favicon.ico')));

app.use('/public', express.static(path.join(__dirname, 'public')));

app.get('*', async (req, res) => {
  const location = req.url;
  const memoryHistory = createMemoryHistory(req.originalUrl);
  const store = configureStore(memoryHistory);
  const history = syncHistoryWithStore(memoryHistory, store);

  match({history, routes, location},
    (error, redirectLocation, renderProps) => {
      if (error) {
        res.status(HTTP_STATUS_CODES.INTERNAL_SERVER_ERROR).send(error.message);
      } else if (redirectLocation) {
        res.redirect(HTTP_STATUS_CODES.FOUND, redirectLocation.pathname + redirectLocation.search);
      } else if (renderProps) {
        const asyncRenderData = Object.assign({}, renderProps, {store});

        loadOnServer(asyncRenderData).then(() => {
          const markup = ReactDOMServer.renderToString(
            <Provider store={store} key="provider">
              <ReduxAsyncConnect {...renderProps} />
            </Provider>
          );
          res.status(HTTP_STATUS_CODES.OK).send(renderHTML(markup, store));
        });
      } else {
        res.status(HTTP_STATUS_CODES.NOT_FOUND).send('Not Found?');
      }
    });
});

const server = http.createServer(app);

server.timeout = 300000; // TODO: Remove after optimization

server.listen(appConfig.port, (err) => {
  if (err) {
    console.error(Chalk.bgRed(err));
  } else {
    console.info(Chalk.black.bgGreen(
      `\n\n💂  Listening at http://${appConfig.host}:${appConfig.port}\n`
    ));
  }
});

function renderHTML(markup: string, store: any) {
  const html = ReactDOMServer.renderToString(
    <Html markup={markup} manifest={manifest} store={store}/>
  );

  return `<!doctype html> ${html}`;
}

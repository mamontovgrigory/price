// import HTTP_STATUS_CODES from 'http-status-enum';

import { router as appApi } from './router';
// import {Ioc} from './ioc';
// import { appApi } from './registration';
import './modules';

export { appApi };

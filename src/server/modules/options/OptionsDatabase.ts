import { PostgreEngine } from '../../database/PostgreEngine';

export class OptionsDatabase {
  private static tableName: string = 'options';

  public static async createTable() {
    const text = `CREATE TABLE IF NOT EXISTS ${OptionsDatabase.tableName} (
                    id SERIAL PRIMARY KEY,
                    code TEXT,
                    mark TEXT,
                    explanation TEXT,
                    UNIQUE (code, mark)
                );
                ALTER TABLE ${OptionsDatabase.tableName}
                ADD COLUMN IF NOT EXISTS disabled BOOLEAN DEFAULT false;
                ALTER TABLE ${OptionsDatabase.tableName}
                ADD COLUMN IF NOT EXISTS model TEXT;`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async getList() {
    return PostgreEngine.getList(OptionsDatabase.tableName);
  }

  public static async getItem(filter) {
    return PostgreEngine.getItem(OptionsDatabase.tableName, filter);
  }

  public static async save(item: any) {
    return PostgreEngine.save(OptionsDatabase.tableName, item);
  }

  public static async saveList(list: any[]) {
    return PostgreEngine.saveList(OptionsDatabase.tableName, list);
  }

  public static async update(item: any) {
    return PostgreEngine.update(OptionsDatabase.tableName, item);
  }

  public static async deleteList(ids: number[]) {
    return PostgreEngine.deleteList(OptionsDatabase.tableName, ids);
  }
}

OptionsDatabase.createTable();

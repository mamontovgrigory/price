const formidable = require('formidable');
const fs = require('fs');

import { router } from '../../router';
import { OptionsLogic } from './OptionsLogic';

const fileTypes = [
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/vnd.ms-excel'
]; // TODO: Move to config

router.post('/Api/Options/GetList', async (req, res) => {
  try {
    const list = await OptionsLogic.getList();
    res.json(list);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Options/Save', async (req, res) => {
  try {
    const {item} = req.body;
    await OptionsLogic.save(item);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Options/Delete', async (req, res) => {
  try {
    const {ids} = req.body;
    await OptionsLogic.deleteList(ids);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Options/Upload', async (req, res) => {
  try {
    const form = new formidable.IncomingForm();

    form.parse(req, (err, fields, files) => {
      if (err) {
        res.json({
          url: req.url,
          error: err,
          fields
        });
      } else {
        if (files.file &&
          fileTypes.indexOf(files.file.type) !== -1) {
          fs.readFile(files.file.path, async (err, data) => {
            if (err) {
              res.json({
                url: req.url,
                error: err
              });
            } else {
              await OptionsLogic.upload(data);
              res.json({
                success: true
              });
            }
          });
        } else {
          res.json({
            error: 'Required xlsx file'
          });
        }
      }
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Options/Check', async (req, res) => {
  try {
    await OptionsLogic.check();
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

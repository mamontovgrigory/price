import { WorksheetsLogic } from '../worksheets/WorksheetsLogic';
import { OptionsDatabase } from './OptionsDatabase';
import { logLogic } from '../../registration';

export class OptionsLogic {
  public static mapping = {
    code: 'Код опции',
    mark: 'Марка',
    model: 'Модель',
    disabled: 'Не использовать',
    explanation: 'Расшифровка'
  };

  public static async getList() {
    return await OptionsDatabase.getList();
  }

  public static async save(item: any) {
    delete item.settings;
    if (typeof item.id === 'undefined') {
      await OptionsDatabase.save(item);
    } else {
      await OptionsDatabase.update(item);
    }
  }

  public static async deleteList(ids: number[]) {
    await OptionsDatabase.deleteList(ids);
  }

  public static async upload(file) {
    const list = WorksheetsLogic.getMappedData(OptionsLogic.mapping, file);
    const baseList = await OptionsDatabase.getList();
    const resultList = await logLogic.prepareUploadList(
      list,
      baseList,
      OptionsLogic.mapping,
      ['code', 'mark', 'model']
    );
    await OptionsDatabase.saveList(resultList.map((item) => ({
      ...item,
      disabled: Boolean(Number(item.disabled)),
    })));
  }

  public static async check() {
    const list = await OptionsDatabase.getList();
    const log = logLogic.checkSpaces(list, OptionsLogic.mapping);
    await logLogic.save({log});
  }
}

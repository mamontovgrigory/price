import { PostgreEngine } from '../../database/PostgreEngine';

const groupsTable = 'groups';

export class GroupsDatabase {
  public static async getList() {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT id, name FROM ${groupsTable} ORDER BY name`
    });
    return result.rows;
  }

  public static async save(item: any) {
    const keys = Object.keys(item).filter((key) => key !== 'id');
    const values = keys.map((key) => item[key]);
    const result = await PostgreEngine.executeQuery({
      text: `INSERT INTO ${groupsTable} (${keys.join(`,`)}) VALUES ('${values.join(`','`)}') RETURNING id`
    });
    return result.rows.length ? result.rows[0] : null;
  }

  public static async update(item: any) {
    let text = `UPDATE ${groupsTable} SET `;
    text += Object.keys(item).filter((key) => key !== 'id').map((key) => {
      return `${key} = '${item[key]}'`;
    }).join(`,`);
    text += ` WHERE id = ${item.id}`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async deleteList(ids: number[]) {
    await PostgreEngine.executeQuery({
      text: `DELETE FROM ${groupsTable} WHERE id IN (${ids.join(',')})`
    });
  }
}

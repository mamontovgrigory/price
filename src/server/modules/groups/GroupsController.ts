import { router } from '../../router';

import { GroupsLogic } from './GroupsLogic';

router.post('/Api/Groups/GetList', async (req, res) => {
  try {
    const list = await GroupsLogic.getList();
    res.json(list);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Groups/Save', async (req, res) => {
  try {
    const {item} = req.body;
    await GroupsLogic.save(item);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Groups/Delete', async (req, res) => {
  try {
    const {ids} = req.body;
    await GroupsLogic.deleteList(ids);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

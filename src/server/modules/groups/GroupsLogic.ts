import { GroupsDatabase } from './GroupsDatabase';
import { PermissionsLogic } from '../permissions/PermissionsLogic';

export class GroupsLogic {
  public static async getList() {
    const list = await GroupsDatabase.getList();
    const permissions = await PermissionsLogic.getList();
    const values = await PermissionsLogic.getValues();
    return list.map((item) => {
      item.settings = values[item.id] ? values[item.id] : [];
      item.permissions = permissions;
      return item;
    });
  }

  public static async save(item: any) {
    const settings = item.settings;
    delete item.settings;
    if (typeof item.id === 'undefined') {
      const {id} = await GroupsDatabase.save(item);
      item.id = id;
    } else {
      await GroupsDatabase.update(item);
    }
    if (typeof item.id !== 'undefined' && settings) {
      await PermissionsLogic.saveList(settings, item.id);
    }
  }

  public static async deleteList(ids: number[]) {
    await GroupsDatabase.deleteList(ids);
  }
}

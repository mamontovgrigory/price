import { router } from '../../router';

import { PermissionsLogic } from './PermissionsLogic';

router.post('/Api/Permissions/GetList', async (req, res) => {
  try {
    const list = await PermissionsLogic.getList();
    res.json(list);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

import { PermissionsDatabase } from './PermissionsDatabase';

export class PermissionsLogic {
  public static async getList() {
    let list: any[] = await PermissionsDatabase.getList();
    list = list.map((item) => {
      item.table = item.resource_table; // TODO: Delete mapping
      item.column = item.name_column;
      delete item.resource_table;
      delete item.name_column;
      return item;
    });
    for (const index of Object.keys(list)) {
      const item = list[index];
      const {table, column} = item;
      if (table) {
        list[index].list = await PermissionsLogic.getResourceList(table, column ? column : 'name');
      }
    }
    return list;
  }

  public static async getResourceList(table: string, column: string) {
    return await PermissionsDatabase.getResourceList(table, column);
  }

  public static async getValues() {
    const result = {};
    const list = await PermissionsDatabase.getValues();
    for (const item of list) {
      item.groupId = item.group_id; // TODO: Delete mapping
      item.permissionId = item.permission_id;
      delete item.group_id;
      delete item.permission_id;
      if (typeof (result[item.groupId]) === 'undefined') {
        result[item.groupId] = [];
      }
      result[item.groupId].push({
        permissionId: item.permissionId,
        value: item.value
      });
    }
    return result;
  }

  public static async saveList(list: any[], groupId: number) {
    list = list.map((item) => {
      item.group_id = groupId;
      item.permission_id = item.permissionId;
      delete item.permissionId; // TODO: Delete mapping;
      if (Array.isArray(item.value)) {
        item.value = item.value.join(',');
      }
      return item;
    });
    await PermissionsDatabase.saveList(list);
  }

  public static async getUserPermissions(id?: number) {
    const result: any = typeof (id) !== 'undefined' ? await PermissionsDatabase.getUsersPermissions(id) : [];
    const aliasesMapping = { // TODO: Delete mapping;
      users_manage: 'usersManage',
      groups_manage: 'groupsManage'
    };
    return result.map((item) => {
      if (item.type === 'list') {
        item.value = item.value.split(',');
      } else if (item.type === 'boolean') {
        item.value = item.value === 'true';
      }
      item.alias = aliasesMapping[item.alias];
      return item;
    });
  }
}

import { PostgreEngine } from '../../database/PostgreEngine';

const permissionsTable = 'permissions';
const permissionsValuesTable = 'permissions_values';

export class PermissionsDatabase {
  public static async getList() {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT * FROM ${permissionsTable} ORDER BY id`
    });
    return result.rows.length ? result.rows : [];
  }

  public static async getResourceList(table: string, column: string) {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT id, ${column} as name FROM ${table} WHERE active = 1`
    });
    return result.rows.length ? result.rows : [];
  }

  public static async getValues() {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT id, permission_id, group_id, value FROM ${permissionsValuesTable}`
    });
    return result.rows.length ? result.rows : [];
  }

  public static getInsertQuery(item) {
    const keys = Object.keys(item).filter((key) => key !== 'id');
    const values = keys.map((key) => item[key]);
    return `DELETE FROM ${permissionsValuesTable} 
            WHERE group_id = ${item.group_id} AND permission_id = ${item.permission_id}; 
            INSERT INTO ${permissionsValuesTable} (${keys.join(`,`)}) VALUES ('${values.join(`','`)}');`;
  }

  public static async saveList(list: any[]) {
    let text = '';
    for (const item of list) {
      text += PermissionsDatabase.getInsertQuery(item);
    }
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async getUsersPermissions(id: number) {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT 
                p.alias, 
                p.name, 
                CASE WHEN p.resource_table IS NOT NULL THEN 'list' ELSE 'boolean' END AS type, pv.value 
              FROM users u 
              LEFT JOIN permissions_values pv ON pv.group_id = u.group_id
              LEFT JOIN permissions p ON pv.permission_id = p.id
              WHERE u.id = ${id}`
    });
    return result.rows.length ? result.rows : [];
  }
}

import xlsx from 'node-xlsx';

export class WorksheetsLogic {
  public static parse(file: string): Array<{ name: string, data: any[] }> {
    const worksheets = xlsx.parse(file);
    try {
      if (worksheets && worksheets.length) {
        return worksheets.map((worksheet) => ({
          ...worksheet,
          data: worksheet.data.filter((row) => row.length)
        }));
      }
    } catch (e) {
      console.log(e);
    }
  }

  public static getBuffer(name, data) {
    return (xlsx as any).build([{name, data}]);
  }

  public static getMappedData(mapping, file) {
    const worksheets = WorksheetsLogic.parse(file);
    const worksheet = worksheets && worksheets.length ? worksheets[0] : null;
    const result = [];
    // for (const worksheet of worksheets) {
    if (worksheet) {
      const {data} = worksheet;
      if (data && data.length) {
        const headerIndex = 0;
        const headers = data[headerIndex];
        for (let index = 1; index < data.length; index++) {
          const row = data[index];
          const item = {};
          for (const key of Object.keys(mapping)) {
            const keyIndex = headers.indexOf(mapping[key]);
            const value = row[keyIndex];
            item[key] = value ? value : '';
          }
          result.push(item);
        }
      }
    }
    // }
    return result;
  }
}

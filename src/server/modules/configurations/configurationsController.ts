import { router } from '../../router';
import { configurationsLogic } from '../../registration';

router.post('/Api/Configurations/GetList', async (req, res) => {
  try {
    const list = await configurationsLogic.getList();
    res.json(list);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Configurations/Save', async (req, res) => {
  try {
    const {item} = req.body;
    await configurationsLogic.save(item);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Configurations/Delete', async (req, res) => {
  try {
    const {ids} = req.body;
    await configurationsLogic.deleteList(ids);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

import { IConfigurationDto, IConfigurationsDatabase } from '../../models/configurations';
import { PostgreEngine } from '../../database/PostgreEngine';

export class ConfigurationsDatabase implements IConfigurationsDatabase {
  constructor() {
    this.createTable();
  }

  private tableName: string = 'configurations';

  private async createTable() {
    const text = `CREATE TABLE IF NOT EXISTS ${this.tableName} (
                    id SERIAL PRIMARY KEY,
                    name TEXT,
                    configuration TEXT,
                    UNIQUE (name)
                )`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public async getList() {
    return PostgreEngine.getList(this.tableName);
  }

  public async save(item: IConfigurationDto) {
    await PostgreEngine.save(this.tableName, item);
  }

  public async saveList(list: IConfigurationDto[]) {
    await PostgreEngine.saveList(this.tableName, list);
  }

  public async update(item: IConfigurationDto) {
    await PostgreEngine.update(this.tableName, item);
  }

  public async deleteList(ids: number[]) {
    return PostgreEngine.deleteList(this.tableName, ids);
  }
}

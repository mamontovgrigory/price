import { IConfigurationDto, IConfigurationsLogic } from '../../models/configurations';
import { configurationsDatabase } from '../../registration';

export class ConfigurationsLogic implements IConfigurationsLogic {
  public async getList() {
    return configurationsDatabase.getList();
  }

  public async save(item: IConfigurationDto) {
    if (typeof item.id === 'undefined') {
      await configurationsDatabase.save(item);
    } else {
      await configurationsDatabase.update(item);
    }
  }

  public async saveList(list: IConfigurationDto[]) {
    await configurationsDatabase.saveList(list);
  }

  public async deleteList(ids: number[]) {
    return configurationsDatabase.deleteList(ids);
  }
}

import { WorksheetsLogic } from '../worksheets/WorksheetsLogic';
import { ColorsDatabase } from './ColorsDatabase';
import { logLogic } from '../../registration';

export class ColorsLogic {
  public static mapping = {
    code: 'Код цвета',
    mark: 'Марка',
    name: 'Название',
    fullName: 'Полное название'
  };

  public static async getList() {
    return await ColorsDatabase.getList();
  }

  public static async save(item: any) {
    delete item.settings;
    if (typeof item.id === 'undefined') {
      await ColorsDatabase.save(item);
    } else {
      await ColorsDatabase.update(item);
    }
  }

  public static async deleteList(ids: number[]) {
    await ColorsDatabase.deleteList(ids);
  }

  public static async upload(file) {
    const list = WorksheetsLogic.getMappedData(ColorsLogic.mapping, file);
    const baseList = await ColorsDatabase.getList();
    const resultList = await logLogic.prepareUploadList(
      list,
      baseList,
      ColorsLogic.mapping,
      ['code', 'mark']
    );
    await ColorsDatabase.saveList(resultList);
  }

  public static async check() {
    const list = await ColorsDatabase.getList();
    const log = logLogic.checkSpaces(list, ColorsLogic.mapping);
    await logLogic.save({log});
  }
}

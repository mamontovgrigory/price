import { PostgreEngine } from '../../database/PostgreEngine';

export class ColorsDatabase {
  private static tableName: string = 'colors';

  public static async createTable() {
    const text = `CREATE TABLE IF NOT EXISTS ${ColorsDatabase.tableName} (
                    id SERIAL PRIMARY KEY,
                    code TEXT,
                    mark TEXT,
                    name TEXT,
                    full_name TEXT,
                    UNIQUE (code, mark)
                )`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async getList() {
    return PostgreEngine.getList(ColorsDatabase.tableName);
  }

  public static async getItem(filter) {
    return PostgreEngine.getItem(ColorsDatabase.tableName, filter);
  }

  public static async save(item: any) {
    return PostgreEngine.save(ColorsDatabase.tableName, item);
  }

  public static async saveList(list: any[]) {
    return PostgreEngine.saveList(ColorsDatabase.tableName, list);
  }

  public static async update(item: any) {
    return PostgreEngine.update(ColorsDatabase.tableName, item);
  }

  public static async deleteList(ids: number[]) {
    return PostgreEngine.deleteList(ColorsDatabase.tableName, ids);
  }
}

ColorsDatabase.createTable();

const formidable = require('formidable');
const fs = require('fs');

import { router } from '../../router';
import { ColorsLogic } from './ColorsLogic';

const fileTypes = [
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/vnd.ms-excel'
]; // TODO: Move to config

router.post('/Api/Colors/GetList', async (req, res) => {
  try {
    const list = await ColorsLogic.getList();
    res.json(list);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Colors/Save', async (req, res) => {
  try {
    const {item} = req.body;
    await ColorsLogic.save(item);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Colors/Delete', async (req, res) => {
  try {
    const {ids} = req.body;
    await ColorsLogic.deleteList(ids);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Colors/Upload', async (req, res) => {
  try {
    const form = new formidable.IncomingForm();

    form.parse(req, (err, fields, files) => {
      if (err) {
        res.json({
          url: req.url,
          error: err,
          fields
        });
      } else {
        if (files.file &&
          fileTypes.indexOf(files.file.type) !== -1) {
          fs.readFile(files.file.path, async (err, data) => {
            if (err) {
              res.json({
                url: req.url,
                error: err
              });
            } else {
              await ColorsLogic.upload(data);
              res.json({
                success: true
              });
            }
          });
        } else {
          res.json({
            error: 'Required xlsx file'
          });
        }
      }
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Colors/Check', async (req, res) => {
  try {
    await ColorsLogic.check();
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

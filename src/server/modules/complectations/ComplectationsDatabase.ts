import { PostgreEngine } from '../../database/PostgreEngine';

export class ComplectationsDatabase {
  private static tableName: string = 'complectations';

  public static async createTable() {
    const text = `CREATE TABLE IF NOT EXISTS ${ComplectationsDatabase.tableName} (
                    id SERIAL PRIMARY KEY,
                    code TEXT,
                    mark TEXT,
                    model TEXT,
                    description TEXT,
                    explanation TEXT,
                    UNIQUE (code, mark, model)
                )`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async getList() {
    return PostgreEngine.getList(ComplectationsDatabase.tableName);
  }

  public static async getItem(filter) {
    return PostgreEngine.getItem(ComplectationsDatabase.tableName, filter);
  }

  public static async save(item: any) {
    return PostgreEngine.save(ComplectationsDatabase.tableName, item);
  }

  public static async saveList(list: any[]) {
    return PostgreEngine.saveList(ComplectationsDatabase.tableName, list);
  }

  public static async update(item: any) {
    return PostgreEngine.update(ComplectationsDatabase.tableName, item);
  }

  public static async deleteList(ids: number[]) {
    return PostgreEngine.deleteList(ComplectationsDatabase.tableName, ids);
  }
}

ComplectationsDatabase.createTable();

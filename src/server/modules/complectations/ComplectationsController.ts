const formidable = require('formidable');
const fs = require('fs');

import { router } from '../../router';
import { ComplectationsLogic } from './ComplectationsLogic';

const fileTypes = [
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/vnd.ms-excel'
]; // TODO: Move to config

router.post('/Api/Complectations/GetList', async (req, res) => {
  try {
    const list = await ComplectationsLogic.getList();
    res.json(list);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Complectations/Save', async (req, res) => {
  try {
    const {item} = req.body;
    await ComplectationsLogic.save(item);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Complectations/Delete', async (req, res) => {
  try {
    const {ids} = req.body;
    await ComplectationsLogic.deleteList(ids);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Complectations/Upload', async (req, res) => {
  try {
    const form = new formidable.IncomingForm();

    form.parse(req, (err, fields, files) => {
      if (err) {
        res.json({
          url: req.url,
          error: err,
          fields
        });
      } else {
        if (files.file &&
          fileTypes.indexOf(files.file.type) !== -1) {
          fs.readFile(files.file.path, async (err, data) => {
            if (err) {
              res.json({
                url: req.url,
                error: err
              });
            } else {
              await ComplectationsLogic.upload(data);
              res.json({
                success: true
              });
            }
          });
        } else {
          res.json({
            error: 'Required xlsx file'
          });
        }
      }
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Complectations/Check', async (req, res) => {
  try {
    await ComplectationsLogic.check();
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

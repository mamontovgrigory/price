import { WorksheetsLogic } from '../worksheets/WorksheetsLogic';
import { ComplectationsDatabase } from './ComplectationsDatabase';
import { logLogic } from '../../registration';

export class ComplectationsLogic {
  public static mapping = {
    code: 'Код модификации',
    mark: 'Марка',
    model: 'Модель',
    description: 'Описание',
    explanation: 'Расшифровка'
  };

  public static async getList() {
    return await ComplectationsDatabase.getList();
  }

  public static async save(item: any) {
    delete item.settings;
    if (typeof item.id === 'undefined') {
      await ComplectationsDatabase.save(item);
    } else {
      await ComplectationsDatabase.update(item);
    }
  }

  public static async deleteList(ids: number[]) {
    await ComplectationsDatabase.deleteList(ids);
  }

  public static async upload(file) {
    const list = WorksheetsLogic.getMappedData(ComplectationsLogic.mapping, file);
    const baseList = await ComplectationsDatabase.getList();
    const resultList = await logLogic.prepareUploadList(
      list,
      baseList,
      ComplectationsLogic.mapping,
      ['code', 'mark', 'model']
    );
    await ComplectationsDatabase.saveList(resultList);
  }

  public static async check() {
    const list = await ComplectationsDatabase.getList();
    const log = logLogic.checkSpaces(list, ComplectationsLogic.mapping);
    await logLogic.save({log});
  }
}

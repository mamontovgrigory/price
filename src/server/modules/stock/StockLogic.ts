import { intersection, uniq, isString, isArray, shuffle } from 'lodash';
const TelegramBot = require('node-telegram-bot-api');

import { WorksheetsLogic } from '../worksheets/WorksheetsLogic';
import { ModificationsLogic } from '../modifications/ModificationsLogic';
import { ColorsLogic } from '../colors/ColorsLogic';
import { InteriorsLogic } from '../interiors/InteriorsLogic';
import { OptionsLogic } from '../options/OptionsLogic';
import { ComplectationsLogic } from '../complectations/ComplectationsLogic';
import { InteriorPhotosLogic } from '../interiorsPhotos/InteriorsPhotosLogic';
import { StockDatabase } from './StockDatabase';
import { logLogic, processLogic } from '../../registration';

const token = '5960599930:AAGIPGngHFSo23hw5yYfoXLAkxAj1tSCicg';

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, {polling: true});

bot.on('message', async (msg) => {
  const chatId = msg.chat.id;
  const chats = await StockDatabase.getChats();
  const chatIds = chats.map(({chatId}) => chatId);
  if (chatIds.indexOf(chatId) != -1) {
    bot.sendMessage(chatId, 'Hi! You already been subscribed for converter logs');
  } else {
    StockDatabase.saveChatId(chatId);
    bot.sendMessage(chatId, 'Hello! You subscribed for converter logs');
  }
});

export class StockLogic {
  public static keyFront: string = 'Начало';
  public static keyBack: string = 'Конец';
  public static keyMark: string = 'Марка';
  public static keyModel: string = 'Модель';
  public static keyBody: string = 'Кузов';
  public static keyColor: string = 'Цвет';
  public static keyPhoto: string = 'Фото';
  public static keyCondition: string = 'Состояние';
  public static keyComplectation: string = 'Комплектация';
  public static keyModificationCode: string = 'Код модификации';
  public static keyPlace: string = 'Место';
  public static keyDate: string = 'Дата съемки';
  public static keyVin: string = 'Исходный VIN';
  public static keyClientId: string = 'ID от клиента';
  public static keyMileage: string = 'Пробег';
  public static keyVideos: string = 'Ссылка на видео';
  public static keyPanoramas: string = 'Панорама';
  public static keyInteriorsByVin: string = 'Интерьеры по VIN';
  public static keyOptions: string = 'Коды опций комплектации';
  public static keyDescription: string = 'Описание';
  public static keySalon: string = 'салон';
  public static keyStreet: string = 'улица';
  public static keyHangar: string = 'ангар';
  public static keyFooter: string = 'подвал';
  public static keyInteriorCode: string = 'Код интерьера';
  public static keyClientPhotos: string = 'Фото клиента';
  public static separator: string = ',';
  public static intersectionSeparator: string = '/';
  public static host: string = 'http://ph.onllline.ru';
  public static matrix: Array<{
    mark: string;
    model: string;
    complectation: string;
    body: string;
    color: string;
    vin?: string;
    amgPackage: boolean;
    optics: boolean;
    photos: string[];
    videos: string[];
    panoramas: string[];
  }> = [];
  public static catalogMatrix: Array<{
    mark: string;
    model: string;
    complectation: string;
    body: string;
    color: string;
    amgPackage: boolean;
    optics: boolean;
    photos: string[];
    videos: string[];
    panoramas: string[];
  }> = [];
  public static vinMatrix: {
    [vin: string]: {
      [place: string]: {
        photos: string[];
        videos: string[];
        panoramas: string[];
      }
    };
  } = {};
  public static interiorsMatrix: any[] = [];
  private static log: string = '';

  public static sort = [StockLogic.keySalon, StockLogic.keyStreet, StockLogic.keyHangar, StockLogic.keyFooter];

  public static mapping = {
    front: StockLogic.keyFront,
    back: StockLogic.keyBack,
    mark: StockLogic.keyMark,
    model: StockLogic.keyModel,
    complectation: StockLogic.keyComplectation,
    body: StockLogic.keyBody,
    color: StockLogic.keyColor,
    condition: StockLogic.keyCondition,
    place: StockLogic.keyPlace,
    date: StockLogic.keyDate,
    description: StockLogic.keyDescription,
    options: StockLogic.keyOptions,
    vin: StockLogic.keyVin,
    videos: StockLogic.keyVideos,
    panoramas: StockLogic.keyPanoramas,
    interiorsByVin: StockLogic.keyInteriorsByVin,
  };

  public static catalogConfiguration = [
    {
      file: [
        {
          column: 'mark'
        }
      ],
      base: [
        {
          column: 'mark'
        }
      ]
    },
    {
      file: [
        {
          column: 'model'
        }
      ],
      base: [
        {
          column: 'model'
        }
      ]
    },
    {
      file: [
        {
          column: 'complectation'
        }
      ],
      base: [
        {
          column: 'complectation'
        }
      ],
      intersection: true,
      ifExists: true
    },
    {
      file: [
        {
          column: 'body'
        }
      ],
      base: [
        {
          column: 'body'
        }
      ]
    },
    {
      file: [
        {
          column: 'color'
        }
      ],
      base: [
        {
          column: 'color'
        }
      ]
    }
  ];

  public static configuration = StockLogic.catalogConfiguration;

  public static async getList() {
    return await StockDatabase.getList();
  }

  public static async save(item) {
    if (typeof item.id === 'undefined') {
      await StockDatabase.save(item);
    } else {
      await StockDatabase.update(item);
    }
  }

  public static async deleteList(ids: number[]) {
    await StockDatabase.deleteList(ids);
  }

  public static async getClients() {
    const rows = await StockDatabase.getClients();
    return rows.map((item) => item.client).filter((item) => Boolean(String(item).trim()));
  }

  public static getValues = (rules, object) => {
    let value = true;
    rules.forEach((rule) => {
      let ruleValue: any = isArray(rule.column) ?
        [].concat(rule.column).map((column) => String(object[column]).toLowerCase()).join('') :
        String(object[rule.column]).toLowerCase();
      if (rule.like) {
        const reg = new RegExp(rule.like.map((item) => item.toLowerCase()).join('|'));
        ruleValue = reg.test(ruleValue);
      }
      if (rule.invert) {
        ruleValue = !ruleValue;
      }
      value = value && ruleValue;
    });
    return value;
  }

  public static getItemValues = (configuration, item, rulesKey: 'file' | 'base') => {
    return [].concat(configuration).map((rules) => {
      const fileRules = rules[rulesKey];
      return StockLogic.getValues(fileRules, item);
    });
  }

  public static findItemIndex(array, fileItem, configuration) {
    return array
      .sort((arrayItem) => !arrayItem.comlectation)
      .findIndex((arrayItem) => {
        const result = [].concat(configuration).filter((rules, ruleIndex) => {
          const fileRules = rules.file;
          const fileValues = fileItem.values ?
            fileItem.values[ruleIndex] :
            StockLogic.getValues(fileRules, fileItem);
          const baseRules = rules.base;
          const baseValues = arrayItem.values ?
            arrayItem.values[ruleIndex] :
            StockLogic.getValues(baseRules, arrayItem);
          if (rules.ifExists && (!fileValues || !baseValues)) {
            return true;
          }
          if (rules.intersection && isString(fileValues) && isString(baseValues)) {
            return intersection(String(fileValues).split(StockLogic.intersectionSeparator),
              String(baseValues).split(StockLogic.intersectionSeparator)).length > 0;
          }
          return fileValues === baseValues;
        });
        return result.length === configuration.length;
      });
  }

  // TODO: Matrix should include array of values and photos
  public static async buildMatrix(client?: string, frontLength?: number, backLength?: number, onlySalon?: boolean) {
    let result = [];
    StockLogic.vinMatrix = {};
    if (client) {
      const filter: any = { client };
      if (onlySalon) {
        filter.place = StockLogic.keySalon;
      }
      const rows: any[] = await StockDatabase.getList(filter);
      const data = [
        [
          StockLogic.keyFront,
          StockLogic.keyBack,
          StockLogic.keyMark,
          StockLogic.keyModel,
          StockLogic.keyComplectation,
          StockLogic.keyBody,
          StockLogic.keyColor,
          StockLogic.keyPlace,
          StockLogic.keyDescription,
          StockLogic.keyOptions,
          StockLogic.keyVin,
          StockLogic.keyVideos,
          StockLogic.keyPanoramas,
          StockLogic.keyInteriorsByVin
        ] // TODO: Delete perversions
      ];
      rows.forEach((item) => {
        item.complectation.split(StockLogic.intersectionSeparator).forEach((complectationItem) => {
          data.push([
            item.front,
            item.back,
            item.mark,
            item.model,
            complectationItem,
            item.body,
            item.color,
            item.place,
            item.description,
            item.options,
            item.vin,
            item.videos,
            item.panoramas,
            item.interiorsByVin
          ]);
        });
      });
      const array = StockLogic.parsePhotos(data, frontLength, backLength);
      array.forEach((item) => {
        if (item.photos && item.photos.length) {
          const configuration = [].concat(StockLogic.configuration).map((configurationItem) => {
            return {
              ...configurationItem,
              ifExists: false
            }
          });
          const values = StockLogic.getItemValues(configuration, item, 'base');
          const photos = item.photos ? [item.photos] : [];
          const videos = item.videos ? [item.videos] : [];
          const panoramas = item.panoramas ? [item.panoramas] : [];
          const resultItem = { ...item, photos, videos, panoramas, values };
          const resultIndex = StockLogic.findItemIndex(result, resultItem, configuration);
          if (resultIndex === -1) {
            result.push(resultItem);
          } else {
            result[resultIndex].photos = result[resultIndex].photos.concat(photos);
            result[resultIndex].videos = result[resultIndex].videos.concat(videos);
            result[resultIndex].panoramas = result[resultIndex].panoramas.concat(panoramas);
          }
          if (item.vin && item.photos.length) {
            const interiorsByVin = item.interiorsByVin[0] ?
              shuffle(item.interiorsByVin[0].split(StockLogic.separator)).splice(0, 5).join(StockLogic.separator) : null;
            const vinItem = {
              photos: [interiorsByVin ? [item.photos[0], interiorsByVin].join(StockLogic.separator) : item.photos[0]],
              videos: [item.videos[0]],
              panoramas: [item.panoramas[0]],
            };
            if (StockLogic.vinMatrix[item.vin]) {
              StockLogic.vinMatrix[item.vin][item.place] = vinItem;
            } else {
              StockLogic.vinMatrix[item.vin] = {
                [item.place]: vinItem
              };
            }
          }
        }
      });

      result = result.map((item) => {
        let { photos, videos, panoramas } = item;
        const resultPhotos = [];
        while (photos.length) {
          photos.forEach((row) => {
            if (row && row.length) {
              resultPhotos.push(row.splice(0, 1)[0]);
            }
          });
          photos = photos.filter((row) => row.length !== 0);
        }
        const resultVideos = []; // TODO: Delete code doubling
        while (videos.length) {
          videos.forEach((row) => {
            if (row && row.length) {
              resultVideos.push(row.splice(0, 1)[0]);
            }
          });
          videos = videos.filter((row) => row.length !== 0);
        }
        const resultPanoramas = []; // TODO: Delete code doubling
        while (panoramas.length) {
          panoramas.forEach((row) => {
            if (row && row.length) {
              resultPanoramas.push(row.splice(0, 1)[0]);
            }
          });
          panoramas = panoramas.filter((row) => row.length !== 0);
        }
        return {
          ...item,
          photos: resultPhotos,
          videos: resultVideos,
          panoramas: resultPanoramas
        };
      });

      StockLogic.matrix = result;
    }
    return result;
  }

  public static async buildCatalogMatrix() {
    const rows = await StockDatabase.getList({
      client: ''
    });
    const result = [];
    rows.forEach((item) => {
      const photos = [];
      if (item.front) {
        photos.push(item.front);
      }
      if (item.back) {
        photos.push(item.back);
      }
      result.push({
        mark: item.mark,
        model: item.model,
        complectation: item.complectation,
        body: item.body,
        color: item.color,
        photos,
        videos: item.videos,
        panoramas: item.panoramas
      });
    });
    StockLogic.catalogMatrix = result;
    return result;
  }

  public static async buildInteriorsMatrix(client?: string, interiorsLength: number = 0) {
    let result = [];
    if (interiorsLength > 0) {
      if (client) {
        const clientInteriorsPhotos: any = await InteriorPhotosLogic.getList(client);
        if (clientInteriorsPhotos && clientInteriorsPhotos.length) {
          result = clientInteriorsPhotos;
        }
      }
      const catalogInteriorsPhotos: any = await InteriorPhotosLogic.getList('');
      if (catalogInteriorsPhotos && catalogInteriorsPhotos.length) {
        result = [].concat(result, catalogInteriorsPhotos);
      }
      result = result.map((item) => {
        return {
          ...item,
          photos: item.photo.split(StockLogic.separator).splice(0, interiorsLength)
        }
      });
      result.sort(item => item.color ? -1 : 1);
    }
    return result;
  }

  public static parsePhotos(data: any, frontLength: number = 1, backLength: number = 3) {
    const result = [];
    if (data && data.length) {
      const headerIndex = 0;
      const headers = data[headerIndex];
      const markIndex = headers.indexOf(StockLogic.keyMark);
      const modelIndex = headers.indexOf(StockLogic.keyModel);
      const complectationIndex = headers.indexOf(StockLogic.keyComplectation);
      const bodyIndex = headers.indexOf(StockLogic.keyBody);
      const colorIndex = headers.indexOf(StockLogic.keyColor);
      const frontIndex = headers.indexOf(StockLogic.keyFront);
      const backIndex = headers.indexOf(StockLogic.keyBack);
      const placeIndex = headers.indexOf(StockLogic.keyPlace);
      const descriptionIndex = headers.indexOf(StockLogic.keyDescription);
      const optionsIndex = headers.indexOf(StockLogic.keyOptions);
      const vinIndex = headers.indexOf(StockLogic.keyVin);
      const videosIndex = headers.indexOf(StockLogic.keyVideos);
      const panoramasIndex = headers.indexOf(StockLogic.keyPanoramas);
      const interiorsByVinIndex = headers.indexOf(StockLogic.keyInteriorsByVin);
      data.forEach((item, index) => {
        if (index !== headerIndex) {
          const frontPhotos = item[frontIndex] ?
            item[frontIndex].split(StockLogic.separator) :
            [];
          let backPhotos = item[backIndex] ?
            item[backIndex].split(StockLogic.separator) :
            [];
          frontPhotos.sort(() => {
            return Math.random() - 0.5;
          });
          backPhotos.sort(() => {
            return Math.random() - 0.5;
          });
          const photos = [];
          while (frontPhotos.length > 0) {
            const front = frontPhotos.splice(0, frontLength).sort((a, b) => {
              return Number(a.replace(/[^0-9,]/g, '')) - Number(b.replace(/[^0-9,]/g, ''));
            });
            const back = backPhotos.splice(0, backLength).sort((a, b) => {
              return Number(a.replace(/[^0-9,]/g, '')) - Number(b.replace(/[^0-9,]/g, ''));
            });
            backPhotos = backPhotos.concat(back);
            photos.push(front.concat(back).join(StockLogic.separator));
          }
          const videos = [].concat(photos).map(() => item[videosIndex]); // TODO: Create more beautiful decision
          const panoramas = [].concat(photos).map(() => item[panoramasIndex]); // TODO: Create more beautiful decision
          const interiorsByVin = [].concat(photos).map(() => item[interiorsByVinIndex]); // TODO: Create more beautiful decision
          result.push({
            mark: item[markIndex],
            model: item[modelIndex],
            complectation: item[complectationIndex],
            body: item[bodyIndex],
            color: item[colorIndex],
            place: item[placeIndex],
            description: item[descriptionIndex],
            options: item[optionsIndex],
            vin: item[vinIndex],
            videos,
            panoramas,
            photos,
            interiorsByVin
          });
        }
      });
    }
    result.sort((a, b) => {
      let numberA = StockLogic.sort.indexOf(String(a.place).toLowerCase());
      let numberB = StockLogic.sort.indexOf(String(b.place).toLowerCase());
      if (numberA === numberB) {
        numberA = a.photos && a.photos.length ?
          Number(a.photos[0].substr(0, a.photos[0].indexOf(',')).replace(/[^0-9,]/g, '')) : 0;
        numberB = b.photos && b.photos.length ?
          Number(b.photos[0].substr(0, b.photos[0].indexOf(',')).replace(/[^0-9,]/g, '')) : 0;
      }
      return numberB - numberA;
    });
    return result;
  }

  public static parsePhotoValue(client: string, value: string, name?: string) {
    const path = client ? `${StockLogic.host}/official/${client}` : `${StockLogic.host}/gallery/${name}`;
    const result = [];
    value = String(value);
    if (Boolean(value)) {
      const groups = value.split(';');
      for (const group of groups) {
        if (/http/.test(group)) {
          result.push(group);
        } else if (!/[^0-9-]/.test(group)) {
          const numbers = group.split('-');
          const start = Number(numbers[0]);
          const end = Number(numbers[numbers.length - 1]);
          for (let photoNumber = start; photoNumber <= end; photoNumber++) {
            let photo = String(photoNumber);
            while (photo.length < 4) {
              photo = '0' + photo;
            }
            result.push(`${path}/IMG_${photo}.jpg`);
          }
        }
      }
    }
    return result;
  }

  public static compareValues(first: string, second: string, multiple: boolean = false) {
    let result = false;
    const reg = /\s*/g;
    first = String(first).trim().toLowerCase().replace(reg, '');
    second = String(second).trim().toLowerCase().replace(reg, '');
    if (first && second) {
      if (multiple) {
        const separator = '/';
        result = intersection(first.split(separator), second.split(separator)).length > 0;
      } else {
        result = first === second;
      }
    }
    return result;
  }

  public static getPhotosByVin(vin?: string) {
    const itemsByVin = vin && StockLogic.vinMatrix.hasOwnProperty(vin) ? StockLogic.vinMatrix[vin] : null;
    if (itemsByVin) {
      const places = Object.keys(itemsByVin);
      const place = StockLogic.sort.find((sortKey) =>
        places.some((place) => String(place).toLowerCase() === String(sortKey).toLowerCase())
      );
      return place ? itemsByVin[place] : itemsByVin[places[0]];
    }
    return null;
  }

  public static getLinks(data: {
    client?: string;
    mark: string;
    model: string;
    complectation: string;
    modificationCode?: string;
    body: string;
    color: string;
    description: string;
    options: string;
    vin?: string;
    interiorCode?: string;
    clientPhotos?: string;
  }) {
    const result = {
      photos: [],
      videos: [],
      panoramas: []
    };
    const { client, interiorCode, mark, model, color, body, vin } = data;
    const itemByVin = this.getPhotosByVin(vin);
    if (itemByVin) {
      const interiorsPhotos = StockLogic.getInteriorsPhotos({
        interiorCode,
        client,
        mark,
        model,
        color,
        body,
      });
      result.photos = [].concat(itemByVin.photos, interiorsPhotos);
      result.videos = itemByVin.videos;
      result.panoramas = itemByVin.panoramas;
    }
    if (result.photos.length === 0) {
      const resultIndex = StockLogic.findItemIndex(StockLogic.matrix, data, StockLogic.configuration);
      const item = StockLogic.matrix[resultIndex];
      if (item) {
        const photos = item.photos.splice(0, 1);
        item.photos = item.photos.concat(photos);
        const interiorsPhotos = StockLogic.getInteriorsPhotos({
          interiorCode,
          client,
          mark,
          model,
          color,
          body,
        });
        result.photos = [].concat(photos, interiorsPhotos);
        const videos = item.videos.splice(0, 1);
        item.videos = item.videos.concat(videos);
        result.videos = videos;
        const panoramas = item.panoramas.splice(0, 1);
        item.panoramas = item.panoramas.concat(panoramas);
        result.panoramas = panoramas;
      } else {
        const resultIndex = StockLogic.findItemIndex(StockLogic.catalogMatrix, data, StockLogic.catalogConfiguration);
        const item = StockLogic.catalogMatrix[resultIndex];
        const clientPhotos = data.clientPhotos ? data.clientPhotos.split(',') : [];
        if (clientPhotos.length > 0) {
          result.photos = clientPhotos;
        }
        if (item) {
          const interiorsPhotos = StockLogic.getInteriorsPhotos({
            interiorCode,
            mark,
            model,
            color,
            body,
          });
          if (clientPhotos.length === 0) {
            result.photos = [].concat(item.photos, interiorsPhotos);
          }
          result.videos = item.videos;
          result.panoramas = item.panoramas;
        }
      }
    }
    return result;
  }

  public static getInteriorsPhotos(data: {
    interiorCode: string;
    client?: string;
    mark: string;
    model: string;
    color: string;
    body: string;
  }) {
    const { interiorCode, client, mark, model, color, body } = data;
    const interiorPhoto = StockLogic.interiorsMatrix.find((item) => {
      return interiorCode && mark &&
        StockLogic.compareValues(interiorCode, item.code, true) &&
        ((!client && !item.client) || StockLogic.compareValues(client, item.client)) &&
        StockLogic.compareValues(mark, item.mark) &&
        StockLogic.compareValues(model, item.model) &&
        (!item.color || StockLogic.compareValues(color, item.color)) &&
        StockLogic.compareValues(body, item.body);
    });
    if (interiorPhoto && interiorPhoto.photos) {
      return interiorPhoto.photos;
    } else if (StockLogic.interiorsMatrix.length) {
      StockLogic.log += `Не найдено совпадений в базе "Фото интерьеров"` +
        ` по значениям "Код интерьера": ${interiorCode}, "Модель": ${model};\r\n`;
      return [];
    }
  }

  public static async fillData(client: string, data: any[]) {
    if (data && data.length) {
      const headerIndex = 0;
      const headers = data[headerIndex];
      const markIndex = headers.indexOf(StockLogic.keyMark);
      const modelIndex = headers.indexOf(StockLogic.keyModel);
      const complectationIndex = headers.indexOf(StockLogic.keyComplectation);
      const modificationCodeIndex = headers.indexOf(StockLogic.keyModificationCode);
      const bodyIndex = headers.indexOf(StockLogic.keyBody);
      const colorIndex = headers.indexOf(StockLogic.keyColor);
      const photoIndex = headers.indexOf(StockLogic.keyPhoto);
      const vinIndex = headers.indexOf(StockLogic.keyVin);
      const videosIndex = headers.indexOf(StockLogic.keyVideos);
      const panoramasIndex = headers.indexOf(StockLogic.keyPanoramas);
      const descriptionIndex = headers.indexOf(StockLogic.keyDescription);
      const optionsIndex = headers.indexOf(StockLogic.keyOptions);
      const interiorCodeIndex = headers.indexOf(StockLogic.keyInteriorCode);
      const clientPhotosIndex = headers.indexOf(StockLogic.keyClientPhotos);
      return data.map((item, index) => {
        if (index !== headerIndex && !item[photoIndex]) {
          const mark = item[markIndex];
          const model = item[modelIndex];
          const complectation = item[complectationIndex];
          const modificationCode = item[modificationCodeIndex];
          const body = item[bodyIndex];
          const color = item[colorIndex];
          const description = item[descriptionIndex];
          const options = item[optionsIndex];
          const vin = item[vinIndex];
          const interiorCode = item[interiorCodeIndex];
          const clientPhotos = item[clientPhotosIndex];
          let { photos, videos, panoramas } =
            (mark && model && body && color) ||
            item[vinIndex] ?
              StockLogic.getLinks({
                client,
                mark,
                model,
                complectation,
                modificationCode,
                body,
                color,
                description,
                options,
                interiorCode,
                vin,
                clientPhotos
              }) : { photos: [], videos: [], panoramas: [] };
          if (photos) {
            item[photoIndex] = photos.join(StockLogic.separator);
          }
          if (videos) {
            item[videosIndex] = videos.join(StockLogic.separator);
          }
          if (panoramas) {
            item[panoramasIndex] = panoramas.join(StockLogic.separator);
          }
        }
        return item;
      });
    } else {
      return [];
    }
  }

  public static async saveData(client, file) {
    const worksheets = WorksheetsLogic.parse(file); // TODO: Delete double parsing
    const name = worksheets && worksheets.length ? worksheets[0].name : null;
    const list = WorksheetsLogic.getMappedData(StockLogic.mapping, file);
    for (const item of list) {
      const { front, back, interiorsByVin } = item;
      item.client = client;
      item.front = StockLogic.parsePhotoValue(client, front, name);
      item.back = StockLogic.parsePhotoValue(client, back, name);
      item.interiorsByVin = StockLogic.parsePhotoValue(client, interiorsByVin, name);
    }
    let log = '';
    if (worksheets && worksheets.length > 1) {
      log += 'Учитывается только первая страница загружаемого файла;\r\n';
    }
    log += logLogic.checkSpaces(list, StockLogic.mapping);
    log += logLogic.countRows(list);
    await logLogic.save({ log });
    await StockDatabase.saveList(list);
  }

  public static async check() {
    const list = await StockDatabase.getList();
    const log = logLogic.checkSpaces(list, StockLogic.mapping);
    await logLogic.save({ log });
  }

  public static async generate(file, client, stream, filename) {
    const { data } = WorksheetsLogic.parse(file)[0];
    if (data && data.length &&
      data[0].includes('Марка') &&
      data[0].includes('Модель') &&
      data[0].includes('Комплектация')) {
      await logLogic.save({ log: '' });
      return data;
    }
    const mapping = {
      modificationCode: 'Код модификации',
      complectationCode: 'Код комплектации',
      colorCode: 'Код цвета',
      interiorCode: 'Код интерьера',
      options: 'Опции и пакеты',
      descriptionHeader: 'Шапка описания',
      price: 'Цена',
      price1: 'Цена с НДС',
      year: 'Год',
      vin: StockLogic.keyVin,
      clientId: StockLogic.keyClientId,
      mileage: StockLogic.keyMileage,
      tradeIn: 'Трейд-ин',
      credit: 'Кредит',
      insurance: 'Страховка',
      max: 'Максималка',
      clientPhotos: 'Фото клиента'
    };
    const list = WorksheetsLogic.getMappedData(mapping, file);
    const result = [ // TODO: Create method for mapping in WorksheetsLogic
      [
        'Марка',
        'Модель',
        'Комплектация',
        'Авто.ру Комплектация',
        'Цена',
        'Цена с НДС',
        'Кузов',
        'Объем двигателя',
        'Мощность',
        'Коробка передач',
        'Тип двигателя',
        'Привод',
        'Год выпуска',
        'Цвет',
        'Описание',
        'VIN',
        'Исходный VIN',
        'Трейд-ин',
        'Кредит',
        'Страховка',
        'Максималка',
        'Состояние',
        'Пробег',
        'Код модификации',
        'Код комплектации',
        'Код цвета',
        'Код интерьера',
        'Коды опций комплектации',
        'ID в базе',
        'Фото',
        'Источник фото',
        'Стикеры (авто.ру)',
        'Премиум (авто.ру)',
        'Промо-текст',
        'Город',
        'Место осмотра',
        StockLogic.keyVideos,
        'Блокировано',
        'Блокировка на авто.ру',
        'Блокировка на ам.ру',
        'Цена для Авто.ру',
        'Цена для Каркопи',
        'Цена для ИРР',
        'Цена для Карсгуру',
        'Цена для Автонавигатора',
        'Цена для Дрома',
        'Наличие',
        'ID модели авто.ру',
        'Модель авто.ру',
        'Поколение авто.ру',
        'ID модификации авто.ру',
        'Модификация авто.ру',
        'Доп. опции',
        'Грузоподъемность',
        'Панорама',
        'Статус продажи',
        StockLogic.keyClientId,
        StockLogic.keyClientPhotos
      ]
    ];
    const modifications: any = await ModificationsLogic.getList();
    const colors: any = await ColorsLogic.getList();
    const interiors: any = await InteriorsLogic.getList();
    const options: any = await OptionsLogic.getList();
    const complectations: any = await ComplectationsLogic.getList();
    const notFound = {
      modificationCodes: [],
      colorCodes: [],
      interiorCodes: [],
      optionCodes: [],
      complectationCodes: []
    };
    for (const item of list) {
      let modification = modifications.find((modificationItem) => {
        return Boolean(item.modificationCode) && Boolean(modificationItem) && Boolean(modificationItem.code) &&
          String(item.modificationCode).trim().toLowerCase() === String(modificationItem.code).trim().toLowerCase();
      });
      let color = colors.find((colorItem) => {
        return Boolean(item.colorCode) && Boolean(modification) && Boolean(modification.mark) &&
          String(item.colorCode).trim().toLowerCase() === String(colorItem.code).trim().toLowerCase() &&
          String(modification.mark).trim().toLowerCase() === String(colorItem.mark).trim().toLowerCase();
      });
      const interior = interiors.find((interiorItem) => {
        return Boolean(item.interiorCode) && Boolean(modification) && Boolean(modification.mark) &&
          String(item.interiorCode).trim().toLowerCase() === String(interiorItem.code).trim().toLowerCase() &&
          String(modification.mark).trim().toLowerCase() === String(interiorItem.mark).trim().toLowerCase();
      });
      const optionCodes = String(item.options).split(' ');
      const itemOptions = [];
      for (const optionCode of optionCodes) {
        const filteredOptions = options.filter((optionItem) => {
          return Boolean(optionCode) && Boolean(modification) && Boolean(modification.mark) &&
            String(optionCode).trim().toLowerCase() === String(optionItem.code).trim().toLowerCase() &&
            String(modification.mark).trim().toLowerCase() === String(optionItem.mark).trim().toLowerCase() &&
            String(modification.mark).trim().toLowerCase() === String(optionItem.mark).trim().toLowerCase();
        });
        const option = filteredOptions.length === 1 ? filteredOptions[0] :
          filteredOptions.sort((optionItem) => optionItem.model ? -1 : 1)
            .find((optionItem) => !optionItem.model ||
              optionItem.model.split('/').map((model) => String(model).trim().toLowerCase())
                .includes(String(modification.model).trim().toLowerCase()));
        if (option) {
          if (!option.disabled) {
            itemOptions.push(option);
          }
        } else if (modification) {
          notFound.optionCodes.push(String(optionCode).trim());
        }
      }
      const complectation = complectations.find((complectationItem) => {
        return Boolean(item.modificationCode) &&
          Boolean(modification) && Boolean(modification.mark) && Boolean(modification.model) &&
          StockLogic.compareValues(item.modificationCode, complectationItem.code, true) &&
          String(modification.mark).trim().toLowerCase() === String(complectationItem.mark).trim().toLowerCase() &&
          String(modification.model).trim().toLowerCase() === String(complectationItem.model).trim().toLowerCase();
      });

      let description = '';
      if (item.descriptionHeader) {
        description += `${item.descriptionHeader}\r\n`;
      }
      if (modification && modification.complectationAutoRu) {
        description += `Комплектация: ${modification.complectationAutoRu}\r\n`;
      }
      if (color) {
        description += `Цвет кузова: ${color.fullName}\r\n`;
      }
      if (interior) {
        description += `Интерьер: ${interior.explanation}\r\n`;
      }
      if (itemOptions.length) {
        for (const option of itemOptions) {
          description += `${option.explanation}\r\n`;
        }
      }
      if (complectation) {
        description += `${complectation.explanation}\r\n`;
      }

      if (!modification) {
        modification = {};
        notFound.modificationCodes.push(String(item.modificationCode).trim());
      }
      if (!color) {
        color = {};
        if (modification) {
          notFound.colorCodes.push(String(item.colorCode).trim());
        }
      }
      if (!interior && modification && item.interiorCode) {
        notFound.interiorCodes.push(String(item.interiorCode).trim());
      }
      if (!complectation && modification && item.modificationCode) {
        notFound.complectationCodes.push(String(item.modificationCode).trim());
      }

      result.push([
        modification.mark,
        modification.model,
        modification.complectation,
        modification.complectationAutoRu,
        item.price,
        item.price1,
        modification.body,
        modification.engineCapacity,
        modification.power,
        modification.transmission,
        modification.engineType,
        modification.driveUnit,
        item.year,
        color.name,
        description,
        '',
        item.vin,
        item.tradeIn,
        item.credit,
        item.insurance,
        item.max,
        'новая',
        item.mileage,
        item.modificationCode,
        item.complectationCode,
        item.colorCode,
        item.interiorCode,
        item.options,
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        modification.loadCapacity,
        '',
        'активно',
        item.clientId,
        item.clientPhotos
      ]);
    }
    let log = '';
    Object.keys(notFound).forEach((key) => {
      const notFoundList = notFound[key];
      if (notFoundList.length) {
        const notFoundString = uniq(notFoundList).join(', ');
        switch (key) {
          case 'modificationCodes':
            log += `Не найдено совпадений в базе "Модификации" по значениям "${mapping.complectationCode}": ${notFoundString};\r\n`;
            break;
          case 'colorCodes':
            log += `Не найдено совпадений в базе "Цвета" по значениям "${mapping.colorCode}": ${notFoundString};\r\n`;
            break;
          case 'interiorCodes':
            log += `Не найдено совпадений в базе "Интерьеры" по значениям "${mapping.interiorCode}": ${notFoundString};\r\n`;
            break;
          case 'optionCodes':
            log += `Не найдено совпадений в базе "Опции" по значениям "${mapping.options}": ${notFoundString};\r\n`;
            break;
          case 'complectationCodes':
            log += `Не найдено совпадений в базе "Комплектации" по значениям "${mapping.complectationCode}": ${notFoundString};\r\n`;
            break;
          default:
        }
      }
    });
    StockLogic.log = log;
    const chats = await StockDatabase.getChats();
    chats.forEach(({chatId}) => {
      bot.sendMessage(chatId, `Клиент: ${client}`);
      bot.sendDocument(chatId, stream, {}, {
        filename,
        contentType: 'application/vnd.ms-excel',
      });
    });
    return result;
  }

  public static getTemplate() {
    const data = [
      [
        'Код модификации',
        'Код комплектации',
        'Код цвета',
        'Код интерьера',
        'Опции и пакеты',
        'Цена',
        'Цена с НДС',
        'Цена по акции 2',
        'Год',
        'Исходный VIN',
        'ID от клиента'
      ],
      [
        '44781313-RB7 ZG2',
        '',
        '7831',
        'V4T',
        '',
        '7683345',
        '',
        '',
        '2019',
        'WDF44781313613093',
        '555'
      ]
    ];
    return WorksheetsLogic.getBuffer('template', data);
  }

  public static async initProcess(data: {
    client?: string;
    configuration: any;
    file: any;
    frontLength?: number;
    backLength?: number;
    interiorsLength?: number;
    onlySalon?: boolean;
  }, stream, fileName) {
    const { client, configuration, file, frontLength, backLength, interiorsLength, onlySalon } = data;
    StockLogic.configuration = configuration;
    const list = await StockLogic.generate(file, client, stream, fileName);
    StockLogic.matrix = await StockLogic.buildMatrix(client, frontLength, backLength, onlySalon);
    StockLogic.catalogMatrix = await StockLogic.buildCatalogMatrix();
    StockLogic.interiorsMatrix = await StockLogic.buildInteriorsMatrix(client, interiorsLength);
    const headers = list.splice(0, 1)[0];
    const processId = await processLogic.insertData(list.map((row) => JSON.stringify(row)));
    await StockDatabase.saveMatrix({
      processId,
      client,
      headers: JSON.stringify(headers),
      configuration: JSON.stringify(configuration),
      log: StockLogic.log
    });
    return processId;
  }

  public static async setProcessPortion(processId: number) {
    const list = await processLogic.getPortion(processId);
    const matrixData = await StockDatabase.getMatrix(processId);
    const data = [
      JSON.parse(matrixData.headers),
      ...list.map((item) => JSON.parse(item.data))
    ];
    StockLogic.configuration = JSON.parse(matrixData.configuration);
    StockLogic.log = matrixData.log;
    const result = await StockLogic.fillData(matrixData.client, data);
    result.splice(0, 1);
    await StockDatabase.updateMatrix({
      ...matrixData,
      log: StockLogic.log
    });
    StockLogic.log = '';
    return await processLogic.updatePortion((list as any).map((item, index) => {
      return {
        ...item,
        result: JSON.stringify(result[index])
      };
    }));
  }

  public static async getProcessResult(processId: number) {
    const matrixData = await StockDatabase.getMatrix(processId);
    const list = await processLogic.getData(processId);
    await StockDatabase.deleteMatrix(processId);
    const headers = JSON.parse(matrixData.headers);
    const listItems = list.map((item) => JSON.parse(item)).map((item) => item.slice(0, item.length - 1));
    const result = [
      headers.slice(0, headers.length - 1),
      ...listItems
    ];
    let log = matrixData.log;
    const frontIndex = headers.indexOf(StockLogic.keyPhoto);
    const emptyPhotosLength = listItems.filter((item) => !item[frontIndex]).length;
    log += `Количество строк с незаполненным столбцом "Фото": ${emptyPhotosLength};\r\n`;
    await logLogic.save({ log, processId });
    const chats = await StockDatabase.getChats();
    chats.forEach(({chatId}) => {
      bot.sendMessage(chatId, log);
    });
    return {
      buffer: WorksheetsLogic.getBuffer(matrixData.name, result)
    };
  }
}

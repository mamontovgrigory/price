import { PostgreEngine } from '../../database/PostgreEngine';

export class StockDatabase {
  public static table = 'stock';
  public static matrixTable = 'stock_matrix';
  public static chatsTable = 'chats';

  public static async createMatrixTable() {
    const text = `CREATE TABLE IF NOT EXISTS ${StockDatabase.table} (
                    id integer NOT NULL,
                    front text,
                    back text,
                    mark text,
                    model text,
                    color text,
                    body text,
                    condition text,
                    place text,
                    date text,
                    vin text,
                    client text NOT NULL,
                    complectation text,
                    description text,
                    options text
                );
                ALTER TABLE ${StockDatabase.table}
                ADD COLUMN IF NOT EXISTS videos TEXT;
                ALTER TABLE ${StockDatabase.table}
                ADD COLUMN IF NOT EXISTS panoramas TEXT;
                ALTER TABLE ${StockDatabase.table}
                ADD COLUMN IF NOT EXISTS interiors_by_vin TEXT;
                CREATE TABLE IF NOT EXISTS ${StockDatabase.matrixTable} (
                    id SERIAL PRIMARY KEY,
                    process_id SERIAL,
                    name TEXT,
                    client TEXT,
                    matrix TEXT,
                    catalog_matrix TEXT
                );
                ALTER TABLE ${StockDatabase.matrixTable}
                ADD COLUMN IF NOT EXISTS headers TEXT;
                ALTER TABLE ${StockDatabase.matrixTable}
                ADD COLUMN IF NOT EXISTS configuration TEXT;
                ALTER TABLE ${StockDatabase.matrixTable}
                ADD COLUMN IF NOT EXISTS vin_matrix TEXT;
                ALTER TABLE ${StockDatabase.matrixTable}
                ADD COLUMN IF NOT EXISTS interiors_matrix TEXT;
                ALTER TABLE ${StockDatabase.matrixTable}
                ADD COLUMN IF NOT EXISTS log TEXT;
                CREATE TABLE IF NOT EXISTS ${StockDatabase.chatsTable} (
                  id SERIAL PRIMARY KEY,
                  chat_id INTEGER,
                  UNIQUE (chat_id)
                );`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async getList(filter?: {
    client?: string;
    place?: string;
  }) {
    return await PostgreEngine.getList(StockDatabase.table, filter);
  }

  public static async save(item: any) {
    await PostgreEngine.save(StockDatabase.table, item);
  }

  public static async update(item: any) {
    await PostgreEngine.update(StockDatabase.table, item);
  }

  public static async saveList(list: any[]) {
    await PostgreEngine.saveList(StockDatabase.table, list);
  }

  public static async deleteList(ids: number[]) {
    await PostgreEngine.executeQuery({
      text: `DELETE FROM ${StockDatabase.table} WHERE id IN (${ids.join(',')})`
    });
  }

  public static async getClients() {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT client FROM ${StockDatabase.table} GROUP BY client`
    });
    return result.rows;
  }

  public static async saveMatrix(item) {
    await PostgreEngine.save(StockDatabase.matrixTable, item);
  }

  public static async getMatrix(processId: number) {
    return await PostgreEngine.getItem(StockDatabase.matrixTable, {processId});
  }

  public static async updateMatrix(item: any) {
    return await PostgreEngine.update(StockDatabase.matrixTable, item);
  }

  public static async deleteMatrix(processId: number) {
    await PostgreEngine.executeQuery({
      text: `DELETE FROM ${StockDatabase.matrixTable} WHERE process_id = ${processId};`
    });
  }

  public static async saveChatId(chatId: number) {
    await PostgreEngine.save(StockDatabase.chatsTable, {chatId});
  }

  public static async getChats() {
    return await PostgreEngine.getList(StockDatabase.chatsTable);
  }
}

StockDatabase.createMatrixTable();

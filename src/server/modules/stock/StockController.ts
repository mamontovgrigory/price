const formidable = require('formidable');
const fs = require('fs');

import { router } from '../../router';
import { StockLogic } from './StockLogic';
const fileTypes = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel'];

router.post('/Api/Stock/GetList', async (req, res) => {
  try {
    const list = await StockLogic.getList();
    res.json(list);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Stock/Save', async (req, res) => {
  try {
    const {item} = req.body;
    await StockLogic.save(item);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Stock/Delete', async (req, res) => {
  try {
    const {ids} = req.body;
    await StockLogic.deleteList(ids);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Stock/GetClients', async (req, res) => {
  try {
    const list = await StockLogic.getClients();
    res.json(list);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Stock/Upload', async (req, res) => {
  try {
    const form = new formidable.IncomingForm();

    form.parse(req, (err, fields, files) => {
      if (err) {
        res.json({
          url: req.url,
          error: err,
          fields
        });
      } else {
        if (files.file &&
          fileTypes.indexOf(files.file.type) !== -1) {
          fs.readFile(files.file.path, async (err, data) => {
            if (err) {
              res.json({
                url: req.url,
                error: err
              });
            } else {
              const name = fields && fields.client && fields.client !== 'undefined' // TODO: Fix sending data
                ? fields.client : '';
              await StockLogic.saveData(name, data);
              res.json({
                success: true
              });
            }
          });
        } else {
          res.json({
            error: 'Required xlsx file'
          });
        }
      }
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Stock/Check', async (req, res) => {
  try {
    await StockLogic.check();
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Stock/GetTemplate', async (req, res) => {
  try {
    const {buffer} = StockLogic.getTemplate();
    res.setHeader('Content-disposition', `attachment; filename=template.xlsx`);
    res.setHeader('Content-type', 'text/plain');
    res.charset = 'windows-1251';
    res.write(buffer);
    res.end();
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Stock/StartProcess', async (req, res) => {
  try {
    const form = new formidable.IncomingForm();

    form.parse(req, (err, fields, files) => {
      if (err) {
        res.json({
          url: req.url,
          error: err,
          fields
        });
      } else {
        if (files.file &&
          fileTypes.indexOf(files.file.type) !== -1) {
          fs.readFile(files.file.path, async (err, data: any) => {
            if (err) {
              res.json({
                url: req.url,
                error: err
              });
            } else {
              const client = fields && fields.client ? fields.client : null;
              // TODO: parse json by middleware
              const configuration = fields && fields.configuration ? JSON.parse(fields.configuration) : null;
              const frontLength = fields && fields.frontLength ? Number(fields.frontLength) : null;
              const backLength = fields && fields.backLength ? Number(fields.backLength) : null;
              const interiorsLength = fields && fields.interiorsLength ? Number(fields.interiorsLength) : null;
              const onlySalon = fields && fields.onlySalon === 'true';
              const processId = await StockLogic.initProcess({
                client,
                configuration,
                file: data,
                frontLength,
                backLength,
                interiorsLength,
                onlySalon
              }, fs.createReadStream(files.file.path), files.file.name);
              res.json({
                processId
              });
            }
          });
        } else {
          res.json({
            error: 'Required xlsx file'
          });
        }
      }
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Stock/GetProcessStep', async (req, res) => {
  try {
    const {processId} = req.body;
    const progress = await StockLogic.setProcessPortion(processId);
    res.json({
      progress
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Stock/GetProcessResult', async (req, res) => {
  try {
    const form = new formidable.IncomingForm();

    form.parse(req, async (err, fields) => {
      if (err) {
        res.json({
          url: req.url,
          error: err,
          fields
        });
      } else {
        const processId = fields && fields.processId ? parseInt(fields.processId, 10) : null;
        // TODO: parse json by middleware
        const {buffer} = await StockLogic.getProcessResult(processId);
        res.setHeader('Content-disposition', `attachment; filename=price.xlsx`);
        res.setHeader('Content-type', 'text/plain');
        res.charset = 'windows-1251';
        res.write(buffer);
        res.end();
      }
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

import { router } from '../../router';
import { logLogic } from '../../registration';

router.post('/Api/Log/GetList', async (req, res) => {
  try {
    const list = await logLogic.getList();
    res.json(list);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Log/GetByProcessId', async (req, res) => {
  try {
    const {processId} = req.body;
    const log = await logLogic.getByProcessId(processId);
    res.json(log);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Log/Save', async (req, res) => {
  try {
    const {item} = req.body;
    await logLogic.save(item);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Log/Delete', async (req, res) => {
  try {
    const {ids} = req.body;
    await logLogic.deleteList(ids);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

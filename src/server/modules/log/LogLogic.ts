import { ILogDto, ILogLogic } from '../../models/log';
import {logDatabase, logLogic} from '../../registration';

export class LogLogic implements ILogLogic {
  public async getList() {
    return logDatabase.getList();
  }

  public getByProcessId = async (processId: number) => logDatabase.getByProcessId(processId);

  public async save(item: ILogDto) {
    if (typeof item.id === 'undefined') {
      await logDatabase.save(item);
    } else {
      await logDatabase.update(item);
    }
  }

  public async saveList(list: ILogDto[]) {
    await logDatabase.saveList(list);
  }

  public async deleteList(ids: number[]) {
    return logDatabase.deleteList(ids);
  }

  public checkSpaces(list: any[], mapping: object) {
    let log = '';
    for (const item of list) {
      const rowNumber = item.hasOwnProperty('id') ? ` в строке ${item.id}` : '';
      for (const key of Object.keys(mapping)) {
        const value = String(item[key]);
        if (value && /^\s/.test(value)) {
          log += `Значение "${value}" поля "${mapping[key]}" содержит пробельные символы в начале${rowNumber};\r\n`;
        }
        if (value && /\s$/.test(value)) {
          log += `Значение "${value}" поля "${mapping[key]}" содержит пробельные символы в конце${rowNumber};\r\n`;
        }
        if (value && /\s\s+/.test(String(value).trim())) {
          log += `Значение "${value}" поля "${mapping[key]}" содержит более одного пробельного символа между словами${rowNumber};\r\n`;
        }
        if (value && /	/.test(String(value).trim())) {
          log += `Значение "${value}" поля "${mapping[key]}" содержит символы табуляции между словами${rowNumber};\r\n`;
        }
      }
    }
    return log;
  }

  public countRows(list: any[]) {
    return `Добавлено ${list.length} строк;\r\n`;
  }

  public async prepareUploadList(list: any[], baseList: any[], mapping: object, uniqueColumns: string[]) {
    let log = '';
    const resultList = list.filter((item) =>  {
      const notUnique = baseList.some((baseItem) =>
        uniqueColumns.every((column) =>
          String(baseItem[column]).trim() === String(item[column]).trim()
        )
      );
      if (notUnique) {
        const columnsString = uniqueColumns.map((column) => `${mapping[column]} ${item[column]}`).join(', ');
        log += `Не уникальные значения: ${columnsString};\r\n`;
      }
      return !notUnique;
    });
    log += logLogic.checkSpaces(resultList, mapping);
    log += logLogic.countRows(resultList);
    await this.save({log});
    return resultList;
  }
}

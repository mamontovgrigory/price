import { ILogDto, ILogDatabase } from '../../models/log';
import { PostgreEngine } from '../../database/PostgreEngine';

export class LogDatabase implements ILogDatabase {
  constructor() {
    this.createTable();
  }

  private tableName: string = 'log';

  private async createTable() {
    const text = `CREATE TABLE IF NOT EXISTS ${this.tableName} (
                    id SERIAL PRIMARY KEY,
                    log TEXT
                );
                ALTER TABLE ${this.tableName}
                ADD COLUMN IF NOT EXISTS process_id INTEGER;`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public async getList() {
    return PostgreEngine.getList(this.tableName);
  }

  public getByProcessId = async (processId: number) =>
    await PostgreEngine.getItem(this.tableName, { processId });

  public async save(item: ILogDto) {
    await PostgreEngine.save(this.tableName, item);
  }

  public async saveList(list: ILogDto[]) {
    await PostgreEngine.saveList(this.tableName, list);
  }

  public async update(item: ILogDto) {
    await PostgreEngine.update(this.tableName, item);
  }

  public async deleteList(ids: number[]) {
    await PostgreEngine.deleteList(this.tableName, ids);
  }
}

import { PostgreEngine } from '../../database/PostgreEngine';
import { IProgressDto, IProgressDatabase } from '../../models/progress';

export class ProgressDatabase implements IProgressDatabase {
  constructor() {
    this.createTable();
  }

  public tableName: string = 'progress';

  public async createTable() { // TODO: Make method private
    const text = `CREATE TABLE IF NOT EXISTS ${this.tableName} (
                    id SERIAL PRIMARY KEY,
                    process_id SERIAL,
                    data TEXT,
                    result TEXT
                )`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public async getLastProcessId(): Promise<number> {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT MAX(process_id) as id
        FROM ${this.tableName} 
        ORDER BY id DESC 
        LIMIT 1`
    });
    return result.rows.length ? result.rows[0].id : null;
  }

  public async saveList(list: IProgressDto[]) {
    await PostgreEngine.saveList(this.tableName, list);
  }

  public async getPortion(processId: number, limit: number): Promise<IProgressDto[]> {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT * FROM ${this.tableName} WHERE process_id = ${processId} AND result IS NULL LIMIT ${limit};`
    });
    return result.rows.length ? result.rows : [];
  }

  public async updateList(list: IProgressDto[]) {
    await PostgreEngine.updateList(this.tableName, list);
  }

  public async getData(processId: number): Promise<IProgressDto[]> {
    return await PostgreEngine.getList(this.tableName, {processId});
  }

  public async deleteData(processId: number) {
    await PostgreEngine.executeQuery({
      text: `DELETE FROM ${this.tableName} WHERE process_id = ${processId};`
    });
  }

  public async getProgressCount(processId: number): Promise<number> {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT COUNT(*) as count FROM ${this.tableName} WHERE process_id = ${processId} AND result IS NOT NULL;`
    });
    return result.rows.length ? result.rows[0].count : null;
  }

  public async getTotalCount(processId: number): Promise<number> {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT COUNT(*) as count FROM ${this.tableName} WHERE process_id = ${processId};`
    });
    return result.rows.length ? result.rows[0].count : null;
  }
}

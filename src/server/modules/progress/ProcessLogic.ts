import { IProgressDto, IProgressLogic } from '../../models/progress';
import { progressDatabase } from '../../registration';

export class ProcessLogic implements IProgressLogic {
  public limit: number = 30;

  public async insertData(list: any[]): Promise<number> {
    let processId = 0;
    const lastProcessId = await progressDatabase.getLastProcessId();
    if (Number.isInteger(lastProcessId)) {
      processId = lastProcessId + 1;
    }
    await progressDatabase.saveList(list.map((item) => {
      return {
        data: String(item),
        processId
      };
    }));
    return processId;
  }

  public async getPortion(processId: number): Promise<IProgressDto[]> {
    return await progressDatabase.getPortion(processId, this.limit);
  }

  public async updatePortion(list: IProgressDto[]): Promise<number> {
    const maxPercents = 100;
    if (list && list.length) {
      await progressDatabase.updateList(list);
      const processId = (list[0] as any).process_id; // TODO: use processId variable name in logic
      const totalCount = await progressDatabase.getTotalCount(processId);
      const progressCount = await progressDatabase.getProgressCount(processId);
      return Math.floor(progressCount / totalCount * 100);
    }
    return maxPercents;
  }

  public async getData(processId: number): Promise<string[]> {
    const result = await progressDatabase.getData(processId);
    await progressDatabase.deleteData(processId);
    return result.map((item) => {
      return item.result;
    });
  }
}

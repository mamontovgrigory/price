import { WorksheetsLogic } from '../worksheets/WorksheetsLogic';
import { ModificationsDatabase } from './ModificationsDatabase';
import { logLogic } from '../../registration';

export class ModificationsLogic {
  public static mapping = {
    code: 'Код модификации',
    complectationCode: 'Код комплектации',
    complectationBase: 'Базовая комплектация',
    complectationAutoRu: 'Авто.ру Комплектация',
    mark: 'Марка',
    model: 'Модель',
    complectation: 'Комплектация',
    body: 'Кузов',
    engineCapacity: 'Объем двигателя',
    power: 'Мощность',
    transmission: 'Коробка передач',
    engineType: 'Тип двигателя',
    driveUnit: 'Привод'
  };

  public static async getList() {
    return await ModificationsDatabase.getList();
  }

  public static async save(item: any) {
    delete item.settings;
    if (typeof item.id === 'undefined') {
      await ModificationsDatabase.save(item);
    } else {
      await ModificationsDatabase.update(item);
    }
  }

  public static async deleteList(ids: number[]) {
    await ModificationsDatabase.deleteList(ids);
  }

  public static async upload(file) {
    const list = WorksheetsLogic.getMappedData(ModificationsLogic.mapping, file);
    const baseList = await ModificationsDatabase.getList();
    const resultList = await logLogic.prepareUploadList(
      list,
      baseList,
      ModificationsLogic.mapping,
      ['code', 'mark', 'model']
    );
    await ModificationsDatabase.saveList(resultList);
  }

  public static async check() {
    const list = await ModificationsDatabase.getList();
    const log = logLogic.checkSpaces(list, ModificationsLogic.mapping);
    await logLogic.save({log});
  }
}

import { PostgreEngine } from '../../database/PostgreEngine';

export class ModificationsDatabase {
  private static tableName: string = 'modifications';

  public static async createTable() {
    const text = `CREATE TABLE IF NOT EXISTS ${ModificationsDatabase.tableName} (
                    id SERIAL PRIMARY KEY,
                    code TEXT,
                    complectation_code TEXT,
                    complectation_base TEXT,
                    complectation_auto_ru TEXT,
                    mark TEXT,
                    model TEXT,
                    complectation TEXT,
                    body TEXT,
                    engine_capacity TEXT,
                    power TEXT,
                    transmission TEXT,
                    engine_type TEXT,
                    drive_unit TEXT,
                    UNIQUE (code, mark, model)
                  );
                  ALTER TABLE ${ModificationsDatabase.tableName}
                  ADD COLUMN IF NOT EXISTS load_capacity TEXT;`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async getList() {
    return PostgreEngine.getList(ModificationsDatabase.tableName);
  }

  public static async getItem(filter?: object) {
    return PostgreEngine.getItem(ModificationsDatabase.tableName, filter);
  }

  public static async save(item: any) {
    return PostgreEngine.save(ModificationsDatabase.tableName, item);
  }

  public static async saveList(list: any[]) {
    return PostgreEngine.saveList(ModificationsDatabase.tableName, list);
  }

  public static async update(item: any) {
    return PostgreEngine.update(ModificationsDatabase.tableName, item);
  }

  public static async deleteList(ids: number[]) {
    return PostgreEngine.deleteList(ModificationsDatabase.tableName, ids);
  }
}

ModificationsDatabase.createTable();

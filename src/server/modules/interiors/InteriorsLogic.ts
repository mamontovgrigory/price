import { WorksheetsLogic } from '../worksheets/WorksheetsLogic';
import { InteriorsDatabase } from './InteriorsDatabase';
import { logLogic } from '../../registration';

export class InteriorsLogic {
  public static mapping = {
    code: 'Код интерьера',
    mark: 'Марка',
    explanation: 'Расшифровка'
  };

  public static async getList() {
    return await InteriorsDatabase.getList();
  }

  public static async save(item: any) {
    delete item.settings;
    if (typeof item.id === 'undefined') {
      await InteriorsDatabase.save(item);
    } else {
      await InteriorsDatabase.update(item);
    }
  }

  public static async deleteList(ids: number[]) {
    await InteriorsDatabase.deleteList(ids);
  }

  public static async upload(file) {
    const list = WorksheetsLogic.getMappedData(InteriorsLogic.mapping, file);
    const baseList = await InteriorsDatabase.getList();
    const resultList = await logLogic.prepareUploadList(
      list,
      baseList,
      InteriorsLogic.mapping,
      ['code', 'mark']
    );
    await InteriorsDatabase.saveList(resultList);
  }

  public static async check() {
    const list = await InteriorsDatabase.getList();
    const log = logLogic.checkSpaces(list, InteriorsLogic.mapping);
    await logLogic.save({log});
  }
}

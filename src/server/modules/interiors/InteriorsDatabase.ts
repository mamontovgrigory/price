import { PostgreEngine } from '../../database/PostgreEngine';

export class InteriorsDatabase {
  private static tableName: string = 'interiors';

  public static async createTable() {
    const text = `CREATE TABLE IF NOT EXISTS ${InteriorsDatabase.tableName} (
                    id SERIAL PRIMARY KEY,
                    code TEXT,
                    mark TEXT,
                    explanation TEXT,
                    UNIQUE (code, mark)
                )`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async getList() {
    return PostgreEngine.getList(InteriorsDatabase.tableName);
  }

  public static async getItem(filter) {
    return PostgreEngine.getItem(InteriorsDatabase.tableName, filter);
  }

  public static async save(item: any) {
    return PostgreEngine.save(InteriorsDatabase.tableName, item);
  }

  public static async saveList(list: any[]) {
    return PostgreEngine.saveList(InteriorsDatabase.tableName, list);
  }

  public static async update(item: any) {
    return PostgreEngine.update(InteriorsDatabase.tableName, item);
  }

  public static async deleteList(ids: number[]) {
    return PostgreEngine.deleteList(InteriorsDatabase.tableName, ids);
  }
}

InteriorsDatabase.createTable();

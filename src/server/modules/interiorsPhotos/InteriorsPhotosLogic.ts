import { WorksheetsLogic } from '../worksheets/WorksheetsLogic';
import { StockLogic } from '../stock/StockLogic';
import { InteriorPhotosDatabase } from './InteriorsPhotosDatabase';
import { logLogic } from '../../registration';

export class InteriorPhotosLogic {
  public static mapping = {
    code: 'Код интерьера',
    mark: 'Марка',
    model: 'Модель',
    client: 'Клиент',
    body: 'Кузов',
    color: 'Цвет',
    photo: 'Фото интерьера',
    explanation: 'Расшифровка'
  };

  public static async getList(client?: string) {
    return await InteriorPhotosDatabase.getList(client);
  }

  public static async save(item: any) {
    delete item.settings;
    if (typeof item.id === 'undefined') {
      await InteriorPhotosDatabase.save(item);
    } else {
      await InteriorPhotosDatabase.update(item);
    }
  }

  public static async deleteList(ids: number[]) {
    await InteriorPhotosDatabase.deleteList(ids);
  }

  public static async upload(file) {
    const worksheets = WorksheetsLogic.parse(file); // TODO: Delete double parsing
    const name = worksheets && worksheets.length ? worksheets[0].name : null;
    const list = WorksheetsLogic.getMappedData(InteriorPhotosLogic.mapping, file);
    let log = '';
    for (const item of list) {
      const {client, photo} = item;
      const photos = StockLogic.parsePhotoValue(client, photo, name);
      item.photo = photos.join(StockLogic.separator);
    }
    log += logLogic.checkSpaces(list, InteriorPhotosLogic.mapping);
    log += logLogic.countRows(list);
    await logLogic.save({log});
    await InteriorPhotosDatabase.saveList(list);
  }

  public static async check() {
    const list = await InteriorPhotosDatabase.getList();
    const log = logLogic.checkSpaces(list, InteriorPhotosLogic.mapping);
    await logLogic.save({log});
  }
}

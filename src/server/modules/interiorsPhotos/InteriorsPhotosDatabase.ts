import { PostgreEngine } from '../../database/PostgreEngine';

export class InteriorPhotosDatabase {
  private static tableName: string = 'interior_photos';

  public static async createTable() {
    const text = `CREATE TABLE IF NOT EXISTS ${InteriorPhotosDatabase.tableName} (
                    id SERIAL PRIMARY KEY,
                    code TEXT,
                    client TEXT,
                    mark TEXT,
                    model TEXT,
                    color TEXT,
                    photo TEXT,
                    explanation TEXT
                  );
                  ALTER TABLE ${InteriorPhotosDatabase.tableName}
                  ADD COLUMN IF NOT EXISTS body TEXT;`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async getList(client?: string) {
    let text = `SELECT * FROM ${InteriorPhotosDatabase.tableName}`;
    if (typeof (client) !== 'undefined') {
      text += ` WHERE client='${client}'`;
    }
    const result = await PostgreEngine.executeQuery({
      text
    });
    return result.rows;
  }

  public static async getItem(filter) {
    return PostgreEngine.getItem(InteriorPhotosDatabase.tableName, filter);
  }

  public static async save(item: any) {
    return PostgreEngine.save(InteriorPhotosDatabase.tableName, item);
  }

  public static async saveList(list: any[]) {
    return PostgreEngine.saveList(InteriorPhotosDatabase.tableName, list);
  }

  public static async update(item: any) {
    return PostgreEngine.update(InteriorPhotosDatabase.tableName, item);
  }

  public static async deleteList(ids: number[]) {
    return PostgreEngine.deleteList(InteriorPhotosDatabase.tableName, ids);
  }
}

InteriorPhotosDatabase.createTable();

import { PostgreEngine } from '../../database/PostgreEngine';

const usersTable = 'users';
const groupsTable = 'groups';

export class UsersDatabase {
  public static async login(login: string, password: string) {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT id, login FROM ${usersTable}
      WHERE login = '${login}' AND password = '${password}'`
    });
    return result.rows.length ? result.rows[0] : null;
  }

  public static async setToken(id: number, token: string) {
    await PostgreEngine.executeQuery({
      text: `UPDATE ${usersTable} SET token='${token}'
             WHERE id = ${id}`
    });
  }

  public static async getItem(id) {
    const result = await PostgreEngine.executeQuery({
      text: `SELECT id, login, token FROM ${usersTable}
             WHERE id = ${id}`
    });
    return result.rows.length ? result.rows[0] : null;
  }

  public static async getList() {
    const result = await
      PostgreEngine.executeQuery({
        text: `SELECT u.id, u.login, u.group_id, g.name as group_name
             FROM ${usersTable} u
             LEFT JOIN ${groupsTable} g on u.group_id = g.id`
      });
    return result.rows;
  }

  public static async save(item: any) {
    const keys = Object.keys(item).filter((key) => key !== 'group_id');
    const values = keys.map((key) => item[key]);
    const hasGroupId = item.hasOwnProperty('group_id'); // TODO: Create universal query constructor
    await PostgreEngine.executeQuery({
      text: `INSERT INTO ${usersTable} (${keys.join(`,`)}${hasGroupId ? ',group_id' : ''})
             VALUES ('${values.join(`','`)}'${hasGroupId ? `,${item.group_id}` : ''})`
    });
  }

  public static async update(item: any) {
    let text;
    const passwordString = item.password === 'undefined' ? `` : `,password='${item.password}'`;
    text = `UPDATE ${usersTable}
            SET login='${item.login}',group_id=${item.group_id}${passwordString}
            WHERE id = ${item.id}`;
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async deleteList(ids: number[]) {
    await PostgreEngine.executeQuery({
      text: `DELETE FROM ${usersTable} WHERE id IN (${ids.join(',')})`
    });
  }
}

import { router } from '../../router';

import { authenticationMiddleware } from '../../registration';
import { UsersLogic } from './UsersLogic';

router.post('/Api/Users/Login', async (req, res, next) => {
  try {
    authenticationMiddleware.login(req, res, next);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Users/Logout', (req, res) => {
  try {
    authenticationMiddleware.logout(req, res);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Users/CheckSession', async (req, res) => {
  try {
    if (req.user && req.user.id && req.user.token) {
      const {id, token} = req.user;
      const user = await UsersLogic.checkToken(id, token);
      res.json(user);
    } else {
      res.json(null);
    }
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Users/GetAccount', async (req, res) => {
  try {
    if (req.user && req.user.id) {
      const {id} = req.user;
      const result = await UsersLogic.getAccount(id);
      res.json(result);
    }
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Users/GetList', async (req, res) => {
  try {
    const list = await UsersLogic.getList();
    res.json(list);
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Users/Save', async (req, res) => {
  try {
    const {item} = req.body;
    await UsersLogic.save(item);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

router.post('/Api/Users/Delete', async (req, res) => {
  try {
    const {ids} = req.body;
    await UsersLogic.deleteList(ids);
    res.json({
      success: true
    });
  } catch (error) {
    res.json({
      url: req.url,
      error: error.errorType,
      message: error.message
    });
  }
});

const md5 = require('md5');

import { PermissionsLogic } from '../permissions/PermissionsLogic';
import { UsersDatabase } from './UsersDatabase';

export class UsersLogic {
  public static generateCode(length: number = 10) {
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789';
    let code = '';
    const min = 0;
    const max = chars.length;
    while (code.length < length) {
      code += chars[Math.floor(Math.random() * (max - min + 1)) + min];
    }
    return code;
  }

  public static getPasswordHash(password: string): string {
    return md5(password);
  }

  public static async login(login: string, password: string) {
    password = UsersLogic.getPasswordHash(password);
    const user = await UsersDatabase.login(login, password);
    if (user) {
      const token = UsersLogic.generateCode();
      await UsersDatabase.setToken(Number(user.id), token);
      return {user, token};
    } else {
      return {user, token: null};
    }
  }

  public async checkLogin(login: string,
                          password: string,
                          callback: (user, token) => void) {
    const {user, token} = await UsersLogic.login(login, password);
    return callback(user, token);
  }

  public static async checkToken(id: number, token: string) {
    const item = await UsersDatabase.getItem(id);
    return item.token === token ? item : null;
  }

  public static async getAccount(id?: number) {
    return await PermissionsLogic.getUserPermissions(id);
  }

  public static async getList() {
    const list = await UsersDatabase.getList();
    return list ? list.map((item) => { // TODO: delete mapping
      item.groupId = item.group_id;
      item.groupName = item.group_name;
      delete item.group_id;
      delete item.group_name;
      return item;
    }) : [];
  }

  public static async save(item) {
    if (item.password) {
      item.password = UsersLogic.getPasswordHash(item.password);
    }
    if (typeof (item.groupId) !== 'undefined') {
      item.group_id = item.groupId; // TODO: delete mapping
      delete item.groupId;
    }
    if (typeof item.id === 'undefined') {
      await UsersDatabase.save(item);
    } else {
      await UsersDatabase.update(item);
    }
  }

  public static async deleteList(ids: number[]) {
    await UsersDatabase.deleteList(ids);
  }
}

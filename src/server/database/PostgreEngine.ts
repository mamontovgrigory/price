const snake = require('to-snake-case');
const camel = require('to-camel-case');

import {sequelize} from './Sequelize';

export class PostgreEngine {
  public static async executeQuery(query) {
    try {
      const result = await sequelize.query(query.text, { raw: true });
      const rows = result[0];
      return {rows};
    } catch (error) {
      console.log(query.text, error);
    }
  }

  public static getFilterQuery(filter?: object) {
    let where = '';
    if (filter) {
      where = ' WHERE 1 = 1';
      for (const key of Object.keys(filter)) {
        where += ` AND ${snake(key)} = $$${filter[key]}$$`;
      }
    }
    return where;
  }

  public static getCamelCaseItem(item: any) {
    const object = {};
    const keys = Object.keys(item);
    keys.forEach((key) => {
      object[camel(key)] = item[key];
    });
    return object;
  }

  public static async getList(table: string, filter?: object, limit?: number): Promise<any[]> {
    const where = PostgreEngine.getFilterQuery(filter);
    const result = await PostgreEngine.executeQuery({
      text: `SELECT * FROM ${table}${where} ORDER BY id${limit ? ` LIMIT ${limit}` : ''};`
    });
    const rows: any[] = result.rows.length ? result.rows : [];
    return rows.map((item) => {
      return PostgreEngine.getCamelCaseItem(item);
    });
  }

  public static async getItem(table: string, filter?: object): Promise<any> {
    const where = PostgreEngine.getFilterQuery(filter);
    const result = await PostgreEngine.executeQuery({
      text: `SELECT * FROM ${table}${where} ORDER BY id DESC LIMIT 1;`
    });
    const item = result.rows.length ? result.rows[0] : null;
    return PostgreEngine.getCamelCaseItem(item);
  }

  private static getInsertQuery(table: string, item: any) {
    const keys = Object.keys(item).filter((key) => key !== 'id');
    const values = keys.map((key) => item[key]);
    return `INSERT INTO ${table} (${keys.map((key) => snake(key)).join(`,`)})
            VALUES ($$${values.join(`$$,$$`)}$$)
            ON CONFLICT DO NOTHING;`;
  }

  private static getUpdateQuery(table, item: any) {
    let text = `UPDATE ${table} SET `;
    const keys = Object.keys(item).filter((key) => key !== 'id');
    text += keys.map((key) => {
      return `${snake(key)} = $$${item[key]}$$`;
    }).join(`,`);
    text += ` WHERE id = ${item.id};`;
    return text;
  }

  public static async save(table: string, item: any) {
    await PostgreEngine.executeQuery({
      text: PostgreEngine.getInsertQuery(table, item)
    });
  }

  public static async saveList(table: string, list: any[]) {
    let text = '';
    for (const item of list) {
      text += PostgreEngine.getInsertQuery(table, item);
    }
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async update(table: string, item: any) {
    await PostgreEngine.executeQuery({
      text: PostgreEngine.getUpdateQuery(table, item)
    });
  }

  public static async updateList(table: string, list: any[]) {
    let text = '';
    for (const item of list) {
      text += PostgreEngine.getUpdateQuery(table, item);
    }
    await PostgreEngine.executeQuery({
      text
    });
  }

  public static async deleteList(table: string, ids: number[]) {
    await PostgreEngine.executeQuery({
      text: `DELETE FROM ${table} WHERE id IN (${ids.join(',')});`
    });
  }
}

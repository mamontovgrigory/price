export class Bind {
  public identifiers: any[];
  public classObj: any;
  public instanceObj: any;
  public isSingletone: boolean;
  public paramsCreate: boolean;
}

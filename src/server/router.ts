import * as express from 'express';

import { ExpressSessionTuner } from './middlewares/common/ExpressSessionTuner';
import { ExpressCommonTuner } from './middlewares/common/ExpressCommonTuner';
import { PassportLocalStrategyTuner } from './middlewares/authenticationPassport/PassportLocalStrategyTuner';

const app = express();
ExpressCommonTuner.Setup(app);
ExpressSessionTuner.Setup(app);
PassportLocalStrategyTuner.Setup(app);
export const apiRouter = express.Router();
app.use('/Api', apiRouter);

export const router = app;

import * as passport from 'passport';
import HTTP_STATUS_CODES from 'http-status-enum';

import { IAuthenticationMiddleware } from '../../models/IAuthenticationMiddleware';

export class PassportLocalStrategyMiddlewareFunctions implements IAuthenticationMiddleware {
  public login(req, res, next) {
    passport.authenticate('local',
      (err/*: IAjaxResponse<SessionDto>*/,
       sessionResponse/*: IAjaxResponse<SessionDto>*/) => {
        if (err) {
          return res.status(HTTP_STATUS_CODES.OK).json(null);
        }

        req.logIn(sessionResponse, (error) => {
          if (error) {
            return next(error);
          }
          return res.json(sessionResponse);
        });

      })(req, res, next);
  }

  public logout(req, res) {
    if (req.isAuthenticated()) {
      req.logout();
    }
    return res.json({errorCode: 403/*ErrorCodeEnum.NoErrors*/});
  }

  public mustAuthenticate(req, res, next) {
    req.isAuthenticated() ? next() : res.status(HTTP_STATUS_CODES.UNAUTHORIZED).json({
      errorCode: 403/*ErrorCodeEnum.AuthorizationRequiredError*/,
      errorMessage: 'Authorization required'
    });
  }
}

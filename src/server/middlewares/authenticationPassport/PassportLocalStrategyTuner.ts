import { Strategy, IVerifyOptions } from 'passport-local';
import * as passport from 'passport';
import { Express } from 'express-serve-static-core';

import { usersLogic } from '../../registration';

export class PassportLocalStrategyTuner {
  public static Setup(app: Express) {
    PassportLocalStrategyTuner.InitializePassport();
    app.use(passport.initialize());
    app.use(passport.session());
  }

  private static InitializePassport() {
    passport.use(new Strategy({
        usernameField: 'login',
        passwordField: 'password'
      }, PassportLocalStrategyTuner.verifyFunction
    ));
    passport.serializeUser/*<SessionDto, SessionDto>*/(PassportLocalStrategyTuner.serializeUser);
    passport.deserializeUser/*<SessionDto, SessionDto>*/(PassportLocalStrategyTuner.deserializeUser);
  }

  private static verifyFunction(username: string,
                                password: string,
                                done: (error: any, user?: any, options?: IVerifyOptions) => void) {
    usersLogic.checkLogin(username, password, (user, token) => {
      if (user) {
        return done(null, {...user, token});
      }
      return done({error: 'test'}, false);
    });
  }

  private static serializeUser(session/*: SessionDto*/, done: (err, session) => void) {
    done(null, session);
  }

  private static deserializeUser(session/*: SessionDto*/, done: (err, session?) => void) {
    done(null, session);
  }
}

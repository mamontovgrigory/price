import { IAuthenticationMiddleware } from './models/IAuthenticationMiddleware';
import { Ioc } from './ioc';
import {
  PassportLocalStrategyMiddlewareFunctions
} from './middlewares/authenticationPassport/PassportLocalStrategyMiddlewareFunctions';
import { IUsersLogic } from './models/IUsersLogic';
import { IConfigurationsDatabase, IConfigurationsLogic } from './models/configurations';
import { ILogDatabase, ILogLogic } from './models/log';
import { UsersLogic } from './modules/users/UsersLogic';
import { ConfigurationsLogic } from './modules/configurations/ConfigurationsLogic';
import { ConfigurationsDatabase } from './modules/configurations/ConfigurationsDatabase';
import { LogLogic } from './modules/log/LogLogic';
import { LogDatabase } from './modules/log/LogDatabase';
import { ProcessLogic } from './modules/progress/ProcessLogic';
import { ProgressDatabase } from './modules/progress/ProgressDatabase';

export const authenticationMiddleware: IAuthenticationMiddleware =
  Ioc.register<IAuthenticationMiddleware>('IAuthenticationMiddleware',
    true,
    new PassportLocalStrategyMiddlewareFunctions());
export const usersLogic: IUsersLogic = Ioc.register<IUsersLogic>('IUsersLogic', true, new UsersLogic());

export const configurationsDatabase: IConfigurationsDatabase =
  Ioc.register<IConfigurationsDatabase>('IConfigurationsDatabase', true, new ConfigurationsDatabase());
export const configurationsLogic: IConfigurationsLogic =
  Ioc.register<IConfigurationsLogic>('IConfigurationsLogic', true, new ConfigurationsLogic());

export const logDatabase: ILogDatabase =
  Ioc.register<ILogDatabase>('ILogDatabase', true, new LogDatabase());
export const logLogic: ILogLogic =
  Ioc.register<ILogLogic>('ILogLogic', true, new LogLogic());

export const processLogic: ProcessLogic =
  Ioc.register<ProcessLogic>('ProcessLogic', true, new ProcessLogic());
export const progressDatabase: ProgressDatabase =
  Ioc.register<ProgressDatabase>('ProgressDatabase', true, new ProgressDatabase());

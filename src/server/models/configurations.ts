export interface IConfigurationDto {
  id?: number;
  name: string;
  configuration: string;
}

export interface IConfigurationsDatabase {
  getList: () => Promise<IConfigurationDto[]>;
  save: (item: IConfigurationDto) => void;
  saveList: (list: IConfigurationDto[]) => void;
  update: (item: IConfigurationDto) => void;
  deleteList: (ids: number[]) => void;
}

export interface IConfigurationsLogic {
  getList: () => Promise<IConfigurationDto[]>;
  save: (item: IConfigurationDto) => void;
  saveList: (list: IConfigurationDto[]) => void;
  deleteList: (ids: number[]) => void;
}

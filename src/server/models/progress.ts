export interface IProgressDto {
  id?: number;
  processId: number;
  data?: string;
  result?: string;
}

export interface IProgressDatabase {
  tableName: string;
  createTable(): void;
  getLastProcessId(): Promise<number>;
  saveList(list: IProgressDto[]): void;
  getPortion(processId: number, limit: number): Promise<IProgressDto[]>;
  updateList(list: IProgressDto[]): void;
  getData(processId: number): Promise<IProgressDto[]>;
  deleteData(processId: number): void;
  getProgressCount(processId: number): Promise<number>;
  getTotalCount(processId: number): Promise<number>;
}

export interface IProgressLogic {
  limit: number;
  insertData(list: string[]): Promise<number>;
  getPortion(processId: number): Promise<IProgressDto[]>;
  updatePortion(list: IProgressDto[]): Promise<number>;
  getData(processId: number): Promise<string[]>;
}

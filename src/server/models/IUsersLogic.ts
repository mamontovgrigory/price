export interface IUsersLogic {
  checkLogin(login: string, password: string, callback: (user, token) => void): void;
}

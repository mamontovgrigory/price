export interface ILogDto {
  id?: number;
  log: string;
  processId?: number;
}

export interface ILogDatabase {
  getList: () => Promise<ILogDto[]>;
  getByProcessId: (processId: number) => Promise<ILogDto>;
  save: (item: ILogDto) => void;
  saveList: (list: ILogDto[]) => void;
  update: (item: ILogDto) => void;
  deleteList: (ids: number[]) => void;
}

export interface ILogLogic {
  getList: () => Promise<ILogDto[]>;
  getByProcessId: (processId: number) => Promise<ILogDto>;
  save: (item: ILogDto) => void;
  saveList: (list: ILogDto[]) => void;
  deleteList: (ids: number[]) => void;
  checkSpaces: (list: any[], mapping: object) => string;
  countRows: (list: any[]) => string;
  prepareUploadList: (list: any[], baseList: any[], mapping: object, uniqueColumns: string[]) => Promise<any[]>;
}
